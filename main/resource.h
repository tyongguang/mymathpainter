//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by MyMathPainter.rc
//
#define IDD_ABOUTBOX                    100
#define IDR_MAINFRAME                   128
#define IDS_MOUSE_POS                   129
#define IDS_LEVEL                       130
#define IDS_NEWLAYER                    131
#define IDS_DELLAYER                    132
#define IDS_UP_LAYER                    133
#define IDS_DOWN_LAYER                  134
#define IDS_LAYER_RENAME                135
#define IDS_BOOKMARK_ADD                136
#define IDS_BOOKMARK_REMOVE             137
#define IDS_BOOKMARK_RENAME             138
#define IDD_DIALOG_SETNAME              202
#define IDD_DIALOG_INPUT_GEOM           203
#define IDD_DIALOG_INPUT_EXPRESSION     204
#define IDB_BITMAP_FX                   205
#define IDD_DIALOG_SETCENTER            205
#define IDC_CURSOR_DISABLE              206
#define IDC_CURSOR_MOVING               207
#define IDC_CURSOR_HORZ                 208
#define IDC_CURSOR_VERT                 209
#define IDC_CURSOR_TAB                  210
#define IDB_BITMAP_BOTTOM               211
#define IDB_BITMAP_LEFT                 212
#define IDB_BITMAP_RIGHT                213
#define IDB_BITMAP_SYS                  214
#define IDB_BITMAP_TAB                  215
#define IDB_BITMAP_TABS                 215
#define IDB_BITMAP_TOP                  216
#define IDB_NEWLAYER                    217
#define IDB_NEW_LAYER                   217
#define IDB_REMOVE_LAYER                218
#define IDI_LAYER_ACTIVE                219
#define IDI_LAYER_HIDE                  220
#define IDI_LAYER_SELECT                221
#define IDI_LAYER_SHOW                  222
#define IDI_LAYER_UNSELECT              223
#define IDB_DOWN                        224
#define IDB_UP                          225
#define IDI_LOCK                        227
#define IDI_UNLOCK                      228
#define IDB_RENAME                      229
#define IDB_BOOKMARK_ADD                232
#define IDB_BOOKMARK_REMOVE             233
#define IDC_LIST1                       1000
#define IDC_EDIT1                       1001
#define IDC_EDIT_X                      1001
#define IDC_EDIT_INPUT                  1001
#define IDC_EDIT2                       1002
#define IDC_EDIT_Y                      1002
#define IDC_STATIC_TIPS                 1002
#define IDC_LIST_GEOM_LIST              1003
#define IDC_CHECK1                      1004
#define IDC_CHECK_AUTO_ZOOM             1004
#define IDC_EDIT_NAME                   1005
#define IDC_EDIT_r                      1006
#define IDC_EDIT_README                 1006

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        234
#define _APS_NEXT_COMMAND_VALUE         32783
#define _APS_NEXT_CONTROL_VALUE         1007
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
