#include "stdafx.h"
#include "layer.h"
#include "geom.h"
#include "Wm5IntrSegment2Segment2.h"
#include "Wm5IntrLine2Segment2.h"

void CoordSys::getBox(Wm5::Box2d & box)
{
    box.Center = base_conv->center();
    box.Extent[0] = base_conv->extent().X();
    box.Extent[1] = base_conv->extent().Y();
    box.Axis[0].X() = 1;
    box.Axis[0].Y() = 0;

    box.Axis[1].X() = 0;
    box.Axis[1].Y() = 1;
}

void CoordSys::getRtreeBound(RtreeBound & bound)
{
    gu::CreateBound(bound,
        vPoint(base_conv->vleft(), base_conv->vtop()),
        vPoint(base_conv->vright(), base_conv->vbottom())
        );
}

bool CacheDrawlayer::needRedraw(int reason)
{
    return reason > draw_reson::draw_reson_Force_begin;
}

void CacheDrawlayer::DoDraw(Gdiplus::Graphics &g, CoordSys * pConv, int reason)
{
    assert(cache_bmp_.get() != nullptr);
    if (true == needRedraw(reason)) {
        ClearBitmap(*cache_bmp_.get());
        gp::Graphics gCache(cache_bmp_.get());

//        gCache.SetSmoothingMode(gp::SmoothingModeAntiAlias);
//        gCache.SetSmoothingMode(gp::SmoothingModeAntiAlias);
        draw_elements(gCache, pConv);
    }
    g.DrawImage(cache_bmp_.get(), 0, 0,
        cache_bmp_->GetWidth(), 
        cache_bmp_->GetHeight()
        );
}

void CacheDrawlayer::setSize(int width, int height)
{
    cache_bmp_.reset(new gp::Bitmap(width, height, PixelFormat32bppPARGB));
}

void CacheDrawlayer::ClearBitmap(gp::Bitmap &bmp)
{
    gp::Rect rc(0, 0, bmp.GetWidth(), bmp.GetHeight());
    gp::BitmapData bmpData;
    bmp.LockBits(&rc, gp::ImageLockMode::ImageLockModeWrite, bmp.GetPixelFormat(), &bmpData);
    memset(bmpData.Scan0, 0, abs(bmpData.Stride) *  bmp.GetHeight());
    bmp.UnlockBits(&bmpData);
}

bool DrawableBase::IntersectWith(IDrawable * other, RtreeBound & bound, vPoint * pt)
{
    std::vector<Wm5::Segment2d> your_segments;
    if (false == other->getSegment(bound, your_segments))
        return false;

    std::vector<Wm5::Segment2d> my_segments;
    if (false == this->getSegment(bound, my_segments))
        return false;

    for (auto & my_seg : my_segments) {
        for (auto & your_seg : your_segments) {
            Wm5::IntrSegment2Segment2d intr(my_seg, your_seg);
            if (intr.Find() == true) {
                *pt = intr.GetPoint(0);
                return true;
            }
        }
    }

    return false;
}

bool DrawableBase::IntersectWithAxis(RtreeBound & bound, vPoint * pt)
{
    std::vector<Wm5::Segment2d> my_segments;
    if (false == this->getSegment(bound, my_segments))
        return false;

    Wm5::Line2d axisX(vPoint(0, 0), vPoint(1, 0));
    Wm5::Line2d axisY(vPoint(0, 0), vPoint(0, 1));
    for (auto & my_seg : my_segments) {
        Wm5::IntrLine2Segment2d intrX(axisX, my_seg);
        if (intrX.Find() == true) {
            *pt = intrX.GetPoint();
            return true;
        }

        Wm5::IntrLine2Segment2d intrY(axisY, my_seg);
        if (intrY.Find() == true) {
            *pt = intrY.GetPoint();
            return true;
        }

    }
    return false;

}
