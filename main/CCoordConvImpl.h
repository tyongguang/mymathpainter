
#pragma once
#include "layer.h"

#define WM_INIT_PARAM WM_USER + 73
template<class T>
class CCoordConvImpl : public ICoordConv
{
public:
    CCoordConvImpl() :zoom_level_(23), leve_base_(18), is_ready_(false){
        Init_kCache();
    }
    BEGIN_MSG_MAP(CCoordConvImpl)
        MESSAGE_HANDLER(WM_SIZE, OnSize)
        MESSAGE_HANDLER(WM_CREATE, OnCreate)
        MESSAGE_HANDLER(WM_INIT_PARAM, OnInit)
    END_MSG_MAP()

    LRESULT OnCreate(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled)
    {
        bHandled = false;
        T * pThis = static_cast<T *>(this);
        pThis->PostMessage(WM_INIT_PARAM, WM_INIT_PARAM, WM_INIT_PARAM);
        return 0;
    }

    LRESULT OnInit(UINT /*uMsg*/, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
    {
        if (wParam == WM_INIT_PARAM && lParam == WM_INIT_PARAM)
        {
            bHandled = true;
            ptOffset.X() = double(cur_width_) / 2;
            ptOffset.Y() = double(cur_height_) / 2;
            is_ready_ = true;
            return 0;
        }
        bHandled = false;
        return 0;
    }
    
    LRESULT OnSize(UINT /*uMsg*/, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
    {
        bHandled = false;
        cur_height_ = HIWORD(lParam);
        cur_width_ = LOWORD(lParam);
        return 0;
    }

    LRESULT OnMouseWheelZoom(UINT /*uMsg*/, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
    {
        int fwKeys = GET_KEYSTATE_WPARAM(wParam);
        int zDelta = GET_WHEEL_DELTA_WPARAM(wParam) / WHEEL_DELTA;
        int xPos = GET_X_LPARAM(lParam);
        int yPos = GET_Y_LPARAM(lParam);
        POINT ptCurrnet = { xPos, yPos };
        T * pThis = static_cast<T *>(this);
        ScreenToClient(pThis->m_hWnd, &ptCurrnet);
        bHandled = false;

        Gdiplus::PointF cur_pt((float)ptCurrnet.x, (float)ptCurrnet.y);
        auto cur_view_pt = Screen2View(cur_pt);
        if (false == Zoom_inner(zDelta)) {
            return 0;
        }

        //a = x0 * k - x1
        ptOffset.X() = cur_pt.X - k() * cur_view_pt.X();
        ptOffset.Y() = cur_pt.Y + k() * cur_view_pt.Y();
        return 0;
    }

    virtual vPoint Screen2View(const Gdiplus::PointF & pt)
    {
        /*  View坐标：（x0, y0), Screen坐标: (x1, y1)
            x0 = (x1 - a )/ k
            y0 = -(y1 - b )/ k
        */
        return vPoint(
            (pt.X - a()) / k(),
            -(pt.Y - b()) / k()
            );
    }

    virtual Gdiplus::PointF View2Screen(const vPoint & vpt)
    {
        /*  View坐标：（x0, y0), Screen坐标: (x1, y1)
            x1 = k * x0 + a
            y1 = -k * y0 + b
            这里y 使用-k， 是因为windows的y是向下为正方向，一般是坐标是向上为正方向
        */
        return Gdiplus::PointF(
            float(k() * vpt.X() + a()),
            float(-k() * vpt.Y() + b())
            );
    }
    virtual vPoint center() {
        return CenterPoint();
    }
    virtual vPoint extent() {
        return vPoint(
            fabs(cur_width_ * k() / 2 - a()),
            fabs(cur_height_ * k() / 2 - b())
            );
    }
    virtual double vleft()
    {
        // x0 = (x1 - a )/ k
        return (0 - a()) / k() ;
    }

    virtual double vtop()
    {
        return -(0 - b()) / k();
    }

    virtual double vright()
    {
        return (cur_width_ - a()) / k();
    }

    virtual double vbottom()
    {
        return -(cur_height_ - b()) / k();
    }

    virtual float swidth()
    {
        return float(cur_width_);
    }

    virtual float sheight()
    {
        return float(cur_height_);
    }


    bool Zoom(int Delta)
    {
        Gdiplus::PointF cur_pt(float(cur_width_) / 2, float(cur_height_) / 2);
        auto cur_view_pt = Screen2View(cur_pt);
        if (false == Zoom_inner(Delta))
            return false;
        view_pt_SetCenter(cur_view_pt);
        return true;
    }


    bool ZoomAbs(int absValue)
    {
        return Zoom(absValue - zoom_level_);
    }

    void FitRectZoom(const vPoint & pt1, const vPoint & pt2) {
        vPoint pt_left_top;
        vPoint pt_right_bottom;
        pt_left_top.X() = MIN(pt1.X(), pt2.X());
        pt_left_top.Y() = MIN(pt1.Y(), pt2.Y());

        pt_right_bottom.X() = MAX(pt1.X(), pt2.X());
        pt_right_bottom.Y() = MAX(pt1.Y(), pt2.Y());


        vPoint mid((pt_left_top.X() + pt_right_bottom.X()) / 2,
            (pt_left_top.Y() + pt_right_bottom.Y()) / 2);

        double vHeight = pt_right_bottom.Y() - pt_left_top.Y();
        double vWidth = pt_right_bottom.X() - pt_left_top.X();

        double expect_k1 = cur_height_ / vHeight;
        double expect_k2 = cur_width_ / vWidth;
        if (expect_k1 == DBL_MAX && expect_k2 == DBL_MAX)
            return;

        if (isinf(expect_k1) && isinf(expect_k2)) {
            return;
        }

        double log2 = log(2);
        double log_k1 = log(expect_k1) / log2;
        double log_k2 = log(expect_k2) / log2;
        int before = zoom_level_;
        int target_level = int(MIN(log_k1, log_k2));
        if (false == ZoomCheck(target_level + leve_base_ - zoom_level_))
            return;

        zoom_level_ = target_level + leve_base_;
        view_pt_SetCenter(mid);

        T * pThis = static_cast<T *>(this);
        pThis->OnLevelChanged(before, zoom_level_);
    }

    void OnLevelChanged(int before, int current) {
    }

    void view_pt_offset(double offset_x, double offset_y) {
        /*
        输出入是view的坐标差 offset_x = x0m - x0n, offset_y = y0m - y0n, 
        这里要转成screen 的从标差
        由于：
        view点1 转成screen  view点2 转成screen
        x1m = k * x0m + a   x1n = k * x0n + a

           x1m - x1n
        = (k * x0m + a ) - (k * x0n + a)
        =  k * (x0m - x0n)
        =  k * offset_x
        */
        ptOffset.X() += double(k() * offset_x);
        ptOffset.Y() += -double(k() * offset_y);
    }

    inline void view_pt_SetCenter(const vPoint & vpt)
    {
        /*  View坐标：（x0, y0), Screen坐标: (x1, y1)
               x1 = k  * x0 + a
            =>  a = x1 - k * x0
        */
        Gdiplus::PointF screen_center(float(cur_width_) / 2, float(cur_height_) / 2);
        ptOffset.X() = screen_center.X - k() * vpt.X();
        ptOffset.Y() = screen_center.Y + k() * vpt.Y();
    }
    
    vPoint CenterPoint() {
        Gdiplus::PointF screen_center(float(cur_width_) / 2, float(cur_height_) / 2);
        return Screen2View(screen_center);
    }
    virtual double ScreenSize2ViewSize(double sSize) {
        return double(sSize) / k();
    }

    virtual double ViewSize2ScreenSize(double vSize) {
        return double(vSize) * k();
    }

    int getLevel() {
        return zoom_level_;
    }
    
protected:
    int cur_height_;
    int cur_width_;
    int zoom_level_;
    int leve_base_;
    double k_cache[62];
    bool is_ready_;
    vPoint ptOffset;
private:
    void Init_kCache() {

        for (int i = -31; i < 32 ; ++i) {
            k_cache[31 + i] = i > 0 ? 1 << i : 1.0f / (1 << -i);
        }
    }
    inline double fast_pow_cache(int level) {
        return k_cache[31 + level];
    }
    inline bool ZoomCheck(int Delta)
    {
        if (abs(leve_base_ - (zoom_level_ + Delta)) >= 27) {
            return false;
        }
        return true;
    }
    bool Zoom_inner(int Delta)
    {
        if (false == ZoomCheck(Delta))
            return false;

        int before = zoom_level_;
        zoom_level_ += Delta;
        T * pThis = static_cast<T *>(this);
        pThis->OnLevelChanged(before, zoom_level_);
        return true;
    }

    inline double fast_pow2(int level) {
        if (level >= 0 )
            return 1 << level;
        return 1.0f / (1 << (-level));
    }

    inline double k() {
        return fast_pow_cache(zoom_level_ - leve_base_);
        //return fast_pow2(zoom_level_ - leve_base_);
    }
    inline double a() {
        return  ptOffset.X();
    }
    inline double b() {
        return  ptOffset.Y();
    }
};
