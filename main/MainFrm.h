// MainFrm.h : interface of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////

#pragma once
#include "command.h"
#include "tp_controls/MultiPaneCtrl/MultiPaneCtrl.h"
#include "OutputEditWnd.h"
#include "layer_mgr/layerView.h"
#include "selection_mgr/selectionView.h"
#include "bookmark/bookmarkView.h"

template <class T , UINT t_ID>
class CRibbonSpinnerCtrlEx : public RibbonUI::CRibbonImpl<T>::CRibbonFloatSpinnerCtrl<t_ID>
{
public:
    CRibbonSpinnerCtrlEx()
    {
        /*
        // k_DecimalValue = 201, k_MaxValue = 203, k_MinValue, k_Increment, k_DecimalPlaces
        */
        m_Values[0] = 9527;//初始值
        //
        m_Values[1] = 1000000.0;//最大值
        m_Values[2] = -1000000.0;  //最小值
        m_Values[3] = 0.1;          //step
        m_Values[4] = 6;//小数位
        //m_FormatString = "dddd.ff";
        m_RepresentativeString = "12345.1234567";
    }
};

class CMainFrame : 
	public CRibbonFrameWindowImpl<CMainFrame>, 
	public CMessageFilter, public CIdleHandler
    , public MultiPaneCtrlUserAbility
    , public MultiPaneCtrlNotify
{
public:
	DECLARE_FRAME_WND_CLASS(L"MathPainter", IDR_MAINFRAME)

	CMyMathPainterView m_view;
	CCommandBarCtrl m_CmdBar;

	//TODO: Declare ribbon controls

	// Ribbon control map
	BEGIN_RIBBON_CONTROL_MAP(CMainFrame)
        RIBBON_CONTROL(m_centerX)
        RIBBON_CONTROL(m_centerY)
	END_RIBBON_CONTROL_MAP()

	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL OnIdle();

	BEGIN_UPDATE_UI_MAP(CMainFrame)
		UPDATE_ELEMENT(ID_VIEW_TOOLBAR, UPDUI_MENUPOPUP)
		UPDATE_ELEMENT(ID_VIEW_STATUS_BAR, UPDUI_MENUPOPUP)
	END_UPDATE_UI_MAP()

	BEGIN_MSG_MAP(CMainFrame)
        ADD_EDIT_CMD(m_OutputCommandView)
		MESSAGE_HANDLER(WM_CREATE, OnCreate)
		MESSAGE_HANDLER(WM_DESTROY, OnDestroy)
		COMMAND_ID_HANDLER(ID_APP_EXIT, OnFileExit)
		//COMMAND_ID_HANDLER(ID_FILE_NEW, OnFileNew)
		COMMAND_ID_HANDLER(ID_VIEW_TOOLBAR, OnViewToolBar)
		COMMAND_ID_HANDLER(ID_VIEW_STATUS_BAR, OnViewStatusBar)
		COMMAND_ID_HANDLER(ID_VIEW_RIBBON, OnViewRibbon)
		COMMAND_ID_HANDLER(ID_APP_ABOUT, OnAppAbout)
        RIBBON_FLOATSPINNER_CONTROL_HANDLER(ID_CENTER_X, CenterChanged)
        MESSAGE_HANDLER(WM_COMMAND, OnUICommand)
        MESSAGE_HANDLER(WM_OUTPUT, OnOutputMessage)
		CHAIN_MSG_MAP(CRibbonFrameWindowImpl<CMainFrame>)
	END_MSG_MAP()

    void InitCreateCommand();
     virtual HRESULT DoUpdateProperty(UINT nCmdID, REFPROPERTYKEY key,
         const PROPVARIANT* /*ppropvarCurrentValue*/, PROPVARIANT* ppropvarNewValue);

// Handler prototypes (uncomment arguments if needed):
//	LRESULT MessageHandler(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
//	LRESULT CommandHandler(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
//	LRESULT NotifyHandler(int /*idCtrl*/, LPNMHDR /*pnmh*/, BOOL& /*bHandled*/)

    LRESULT OnUICommand(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT OnCreate(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT OnDestroy(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled);
	LRESULT OnFileExit(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	LRESULT OnFileNew(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	LRESULT OnViewToolBar(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	LRESULT OnViewStatusBar(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	LRESULT OnViewRibbon(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	LRESULT OnAppAbout(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);

    LRESULT CenterChanged(WORD /*wID*/, DOUBLE * lVal, BOOL& /*bHandled*/);
    LRESULT OnOutputMessage(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& /*bHandled*/);

    auto_signal<std::function<void(double, double, UINT) > >::slot mosue_move_slot;
    auto_signal<std::function<void(int, int) > >::slot level_changed_slot;

    CRibbonSpinnerCtrlEx<CMainFrame, ID_CENTER_X> m_centerX;
    CRibbonFloatSpinnerCtrl<ID_CENTER_Y> m_centerY;

    CMultiPaneStatusBarCtrl  m_wndStatusBar;
    CommandRoot m_cmdRoot;
    
    bool InitControls();
    MultiPaneCtrlEx < MultiPaneCtrlStyle_base > m_MultiPaneCtrl;
    layerView m_LayerView;
    selectionView m_selectView;
    
    bookmarkView m_bookmarkView;
    OutputEditWnd m_OutputCommandView;
    
    CListBox m_elementPropView;

    void LayoutWindows();
};
