#include "stdafx.h"
#include "command.h"
#include "Ribbon.h"
#include "misc.h"
#include "geom.h"


class SingleHotKeyCommand : public ICommand
{
public:
    SingleHotKeyCommand()
    {
    }
    virtual unsigned int getID()
    {
        return 0;
    }

    int getGroupID()
    {
        return BASE_RADIO_GROUP;
    }

    virtual bool isEnable()
    {
        return true;
    }

    virtual Command_exe_type getCommandType()
    {
        return  Command_exe_type::LikeButton;
    }
    virtual void OnConnected() {
        key_slot_ = map_->mouse_keyup_event.connect(std::bind(&SingleHotKeyCommand::OnKeyBoradEvent, this,
            holder::_1, holder::_2, holder::_3));

    }

    void OnKeyBoradEvent(TCHAR vk, UINT cRepeat, UINT flags)
    {
        short key_state = GetKeyState(VK_CONTROL);
        if (vk == VK_OEM_PLUS) { //+
            map_->ZoomDelta(1);
        }
        else if (vk == VK_OEM_MINUS) {
            map_->ZoomDelta(-1);
        }
    }
    virtual void OnDisConnected() {
        key_slot_.disconnect();
    }

    virtual void OnExecute(Command_value_type type, long value) {
    }
private:
    KeyBoardEventType::slot key_slot_;

};
COMMAND_FACTORY_REGISTE(SingleHotKeyCommand);

