#include "stdafx.h"
#include "command.h"
#include "Ribbon.h"
#include "Dialogs/SetCenterDlg.h"

class SetCenterCommand : public ICommand
{
public:
    virtual unsigned int getID()
    {
        return ID_SETCENTER;
    }

    virtual bool isEnable()
    {
        return true;
    }

    virtual Command_exe_type getCommandType()
    {
        return  Command_exe_type::LikeButton;
    }

    virtual void OnExecute(Command_value_type type, long value)
    {
        SetCenterDlg dlg;
        if (IDOK != dlg.DoModal())
            return;
        map_->SetCenter(vPoint(dlg.m_fX, dlg.m_fY));
    }

};
COMMAND_FACTORY_REGISTE(SetCenterCommand);


class ResetCenterCommand : public ICommand
{
public:
    virtual unsigned int getID()
    {
        return ID_CENTER_RESET;
    }

    virtual bool isEnable()
    {
        return true;
    }

    virtual Command_exe_type getCommandType()
    {
        return  Command_exe_type::LikeButton;
    }

    virtual void OnExecute(Command_value_type type, long value)
    {
        map_->SetCenter(vPoint(0, 0));
    }

};
COMMAND_FACTORY_REGISTE(ResetCenterCommand);