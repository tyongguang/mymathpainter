#include "stdafx.h"
#include "command.h"
#include "Ribbon.h"
#include "misc.h"
#include "geom.h"
#include "guimisc.h"


class Capture_Base
    : public CheckBoxCommand
    , public IMouseCaptureFilter
{
public:
    Capture_Base()
    {
    }
   
    virtual int getGroupID();
    virtual bool isEnable();
    virtual Command_exe_type getCommandType();
    virtual void OnChecked();
    virtual void OnUnChecked();
    virtual void OnPaint(Gdiplus::Graphics * pg, CoordSys * pConv);

    void ActionPaint(Gdiplus::Graphics * pg, CoordSys * pConv);
    void UpdateStatus(bool is_intr, vPoint * pt= nullptr);

    inline vPoint & getIntrPt() {
        return intr_pt_;
    }
private:
    vPoint intr_pt_;
    bool is_intr_;
    ActionDrawEventType::slot action_slot_;
    
};

template<class T>
class Capture_BaseEx 
    : public Capture_Base
    , public std::enable_shared_from_this<T>
{
public:
    virtual void OnChecked()
    {
        Capture_Base::OnChecked();
        map_->AddMouseCaptureFilter(shared_from_this());
    }

    virtual void OnUnChecked()
    {
        Capture_Base::OnUnChecked();
        map_->RemoveMouseCaptureFilter(shared_from_this());
    }
};
    
