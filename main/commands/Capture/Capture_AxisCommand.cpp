#include "stdafx.h"
#include "command.h"
#include "Ribbon.h"
#include "misc.h"
#include "geom.h"
#include "guimisc.h"
#include "Capture_Base.h"


class Capture_AxisCommand : 
    public Capture_BaseEx<Capture_AxisCommand>
{
public:
    Capture_AxisCommand()
    {
    }

    virtual unsigned int getID()
    {
        return ID_CAPTURE_AXIS;
    }
   
    virtual void getName(CString & name)
    {
        name = L"���ཻ";
    }

    virtual bool Filter(vPoint & vpt)
    {
        double range = map_->ScreenSize2ViewSize(8);

        gu::Bound2d bound(vpt, range);
        std::vector<IDrawable *> selected;

        for (auto & layer : map_->layers) {
            if (layer.second->visiable == true)
                layer.second->Query(bound, selected);
        }
        size_t size = selected.size();
        if (size == 0) {
            UpdateStatus(false);
            return false;
        }

        double min_dist = DBL_MAX;
        vPoint target_pt;

        for (auto & cur_sel : selected) {
            vPoint pending_pt;
            
            if (true == cur_sel->IntersectWithAxis(bound, &pending_pt)) {
                double len = (vpt - pending_pt).SquaredLength();
                if (len < min_dist) {
                    min_dist = len;
                    target_pt = pending_pt;
                }   
            }
        }
        if (min_dist == DBL_MAX || sqrt(min_dist) > range) {
            UpdateStatus(false);
            return false;
        }

        vpt = target_pt;
        UpdateStatus(true, &vpt);
        return true;
    }

 
    virtual void OnChecked()
    {
        __super::OnChecked();
        pen_.reset(new gp::Pen(gp::Color::Red, 2.0f));
    }

    virtual void OnUnChecked()
    {
        __super::OnUnChecked();
        pen_.release();
    }

    virtual void OnPaint(Gdiplus::Graphics * pg, CoordSys * pConv) {
        auto rc = gm::CenterPointSquare(pConv->conv(getIntrPt()), 10);
        pg->DrawLine(pen_.get(), gp::PointF(rc.GetLeft(), rc.GetTop() + rc.Height / 2), gp::PointF(rc.GetRight(), rc.GetTop() + rc.Height / 2));
        pg->DrawLine(pen_.get(), gp::PointF(rc.GetLeft() + rc.Width / 2, rc.GetTop()), gp::PointF(rc.GetLeft() + rc.Width / 2, rc.GetBottom()));
       
    }
private:
    std::unique_ptr<gp::Pen> pen_;

};
COMMAND_FACTORY_REGISTE(Capture_AxisCommand);

