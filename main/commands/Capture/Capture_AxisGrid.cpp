#include "stdafx.h"
#include "command.h"
#include "Ribbon.h"
#include "misc.h"
#include "geom.h"
#include "guimisc.h"
#include "Capture_Base.h"


class Capture_AxisGrid : 
    public Capture_BaseEx<Capture_AxisGrid>
{
public:
    Capture_AxisGrid()
    {
    }

    virtual unsigned int getID()
    {
        return ID_CAPTURE_AXIS_GRID;
    }
   
    virtual void getName(CString & name)
    {
        name = L"�����";
    }

    bool getGridPoint(const vPoint & pt, vPoint * output_pt, float pixel_range /*= 8.0F*/)
    {
        IAxisControl * pAxis = map_->getAxisControl();
        if (pAxis->rw_visiable_ == false || pAxis->rw_showGrid_ == false || pAxis->r_scale_valid_ == false)
            return false;

        double range = map_->ScreenSize2ViewSize(pixel_range);

        int base_x = int(pt.X() / pAxis->r_scale_);
        int base_y = int(pt.Y() / pAxis->r_scale_);


        double x1 = base_x * pAxis->r_scale_;
        double x2 = base_x > 0 ? (base_x + 1)* pAxis->r_scale_ : (base_x - 1)* pAxis->r_scale_;

        double y1 = base_y * pAxis->r_scale_;
        double y2 = base_y > 0 ? (base_y + 1)* pAxis->r_scale_ : (base_y - 1)* pAxis->r_scale_;

        double min_dist = DBL_MAX;
        for (auto x : { x1, x2 }) {
            for (auto y : { y1, y2 }) {
                if (x > pt.X() - range && x < pt.X() + range &&
                    y > pt.Y() - range && y < pt.Y() + range)
                {
                    double len = (vPoint(x, y) - pt).SquaredLength();
                    if (len < min_dist) {
                        output_pt->X() = x;
                        output_pt->Y() = y;
                        min_dist = len;
                    }

                }
            }
        }

        if (min_dist != DBL_MAX)
            return true;

        return false;
    }


    virtual bool Filter(vPoint & vpt)
    {
        vPoint result_pt;
        if (true == getGridPoint(vpt, &result_pt, 6)) {
            vpt = result_pt;
            UpdateStatus(true, &vpt);
            return true;
        }
        UpdateStatus(false);

        return false;
    }

 
    virtual void OnChecked()
    {
        __super::OnChecked();
        pen_.reset(new gp::Pen(gp::Color::Red, 2.0f));
    }

    virtual void OnUnChecked()
    {
        __super::OnUnChecked();
        pen_.release();
    }

    virtual void OnPaint(Gdiplus::Graphics * pg, CoordSys * pConv) {
        auto rc = gm::CenterPointSquare(pConv->conv(getIntrPt()), 6);
        pg->DrawRectangle(pen_.get(), rc);
       
    }
private:
    std::unique_ptr<gp::Pen> pen_;

};
COMMAND_FACTORY_REGISTE(Capture_AxisGrid);

