#include "stdafx.h"
#include "command.h"
#include "Ribbon.h"
#include "misc.h"
#include "geom.h"
#include "guimisc.h"
#include "Capture_Base.h"
#include "Wm5DistPoint2Segment2.h"


class Capture_IntersectCommand : 
    public Capture_BaseEx<Capture_IntersectCommand>
{
public:
    Capture_IntersectCommand()
    {
    }

    virtual unsigned int getID()
    {
        return ID_CAPTURE_INTERSECTION;
    }
   
    virtual void getName(CString & name)
    {
        name = L"�ཻ";
    }

    virtual bool Filter(vPoint & vpt)
    {
        double range = map_->ScreenSize2ViewSize(8);

        gu::Bound2d bound(vpt, range);
        std::vector<IDrawable *> selected;

        for (auto & layer : map_->layers) {
            if (layer.second->visiable == true)
                layer.second->Query(bound, selected);
        }
        size_t size = selected.size();
        if (size == 0) {
            UpdateStatus(false);
            return false;
        }

        for (size_t i = 0; i < size - 1; ++i) {
            for (size_t j = i + 1; j < size; ++j) {
                if (true == selected[i]->IntersectWith(selected[j], bound, &vpt)) {
                    UpdateStatus(true, &vpt);
                    return true;
                }
            }
        }
        UpdateStatus(false);
        return false;
    }

 
    virtual void OnChecked()
    {
        __super::OnChecked();
        pen_.reset(new gp::Pen(gp::Color::Red, 2.0f));
    }

    virtual void OnUnChecked()
    {
        __super::OnUnChecked();
        pen_.release();
    }

    virtual void OnPaint(Gdiplus::Graphics * pg, CoordSys * pConv) {
        auto rc = gm::CenterPointSquare(pConv->conv(getIntrPt()), 10);
        pg->DrawLine(pen_.get(), gp::PointF(rc.GetLeft(), rc.GetTop()), gp::PointF(rc.GetRight(), rc.GetBottom()));
        pg->DrawLine(pen_.get(), gp::PointF(rc.GetLeft(), rc.GetBottom()), gp::PointF(rc.GetRight(), rc.GetTop()));
    }
private:
    std::unique_ptr<gp::Pen> pen_;

};
COMMAND_FACTORY_REGISTE(Capture_IntersectCommand);

