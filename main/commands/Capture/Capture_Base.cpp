#include "stdafx.h"
#include "Capture_Base.h"

int Capture_Base::getGroupID()
{
    return BASE_RADIO_GROUP;
}

bool Capture_Base::isEnable()
{
    return true;
}

Command_exe_type Capture_Base::getCommandType()
{
    return  Command_exe_type::LikeCheckBox;
}

void Capture_Base::OnChecked()
{
    action_slot_ = map_->action_draw_event.connect(std::bind(
        &Capture_Base::ActionPaint, this, holder::_1, holder::_2));

    is_intr_ = false;
}

void Capture_Base::OnUnChecked()
{
    if (action_slot_.isConnected() == true) {
        action_slot_.disconnect();
        map_->refresh();
    }
}

void Capture_Base::OnPaint(Gdiplus::Graphics * pg, CoordSys * pConv)
{

}

void Capture_Base::ActionPaint(Gdiplus::Graphics * pg, CoordSys * pConv)
{
    if (is_intr_ == true) {
        OnPaint(pg, pConv);
    }
}

void Capture_Base::UpdateStatus(bool is_intr, vPoint * pt/*= nullptr*/)
{
    if (is_intr == true) {
        ATLASSERT(pt != nullptr);
        intr_pt_ = *pt;
    }
    is_intr_ = is_intr;
}

