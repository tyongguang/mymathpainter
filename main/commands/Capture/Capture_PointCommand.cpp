#include "stdafx.h"
#include "command.h"
#include "Ribbon.h"
#include "misc.h"
#include "geom.h"
#include "guimisc.h"
#include "Capture_Base.h"


class Capture_PointCommand :
    public Capture_BaseEx<Capture_PointCommand>
{
public:
    Capture_PointCommand()
    {
    }

    virtual unsigned int getID()
    {
        return ID_CAPTURE_POINT;
    }
   
    virtual void getName(CString & name)
    {
        name = L"ʵ��";
    }

    virtual bool Filter(vPoint & vpt)
    {
        bool is_found = map_->getRealPoint(vpt);
        if (false == is_found) {
            UpdateStatus(false);
            return false;
        }
        UpdateStatus(true, &vpt);
        return true;
    }

 
    virtual void OnChecked()
    {
        __super::OnChecked();
        pen_.reset(new gp::Pen(gp::Color::Red, 2.0f));
    }

    virtual void OnUnChecked()
    {
        __super::OnUnChecked();
        pen_.release();
    }

    virtual void OnPaint(Gdiplus::Graphics * pg, CoordSys * pConv) {
        auto rc = gm::CenterPointSquare(pConv->conv(getIntrPt()), 8);
        pg->DrawEllipse(pen_.get(), rc);
       
    }
private:
    std::unique_ptr<gp::Pen> pen_;

};
COMMAND_FACTORY_REGISTE(Capture_PointCommand);

