#include "stdafx.h"
#include "command.h"
#include "Ribbon.h"
#include "misc.h"
#include "geom.h"
#include "guimisc.h"
#include "Capture_Base.h"


class Capture_ShapeCommand : 
    public Capture_BaseEx<Capture_ShapeCommand>
{
public:
    Capture_ShapeCommand()
    {
    }

    virtual unsigned int getID()
    {
        return ID_CAPTURE_SHAPE;
    }
   
    virtual void getName(CString & name)
    {
        name = L"��״��";
    }

    virtual bool Filter(vPoint & vpt)
    {
        double range = map_->ScreenSize2ViewSize(8);

        std::vector<Wm5::Segment2d> segments;
        bool is_found_ = map_->getSegments(vpt, segments, 6);
        if (false == is_found_) {
            UpdateStatus(false);
            return false;
        }
        double min_dist = DBL_MAX;
        vPoint target_pt;
        for (auto & seg : segments) {

            double cur_dist0 = (seg.P0 - vpt).SquaredLength();
            if (cur_dist0 < min_dist) {
                target_pt = seg.P0;
                min_dist = cur_dist0;
            }

            double cur_dist1 = (seg.P1 - vpt).SquaredLength();
            if (cur_dist1 < min_dist) {
                target_pt = seg.P1;
                min_dist = cur_dist1;
            }

        }
        if (min_dist == DBL_MAX || sqrt(min_dist) > range) {
            UpdateStatus(false);
            return false;
        }

        vpt = target_pt;


        UpdateStatus(true, &target_pt);
        return true;
    }

 
    virtual void OnChecked()
    {
        __super::OnChecked();
        pen_.reset(new gp::Pen(gp::Color::Red, 2.0f));
    }

    virtual void OnUnChecked()
    {
        __super::OnUnChecked();
        pen_.release();
    }

    virtual void OnPaint(Gdiplus::Graphics * pg, CoordSys * pConv) {
        auto rc = gm::CenterPointSquare(pConv->conv(getIntrPt()), 6);
        pg->DrawEllipse(pen_.get(), rc);
       
    }
private:
    std::unique_ptr<gp::Pen> pen_;

};
COMMAND_FACTORY_REGISTE(Capture_ShapeCommand);

