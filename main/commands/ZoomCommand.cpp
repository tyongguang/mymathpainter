#include "stdafx.h"
#include "command.h"
#include "Ribbon.h"
#include "misc.h"
#include "geom.h"


class ZoomCommand : public RadionCommand
{
private:
    enum cmd_state {

        eBoundSelect = 0,
        ePointSelect,
        eInvalid
    };
public:
    ZoomCommand() :is_ldown(false)
    {
        bound_pen_.reset(new gp::Pen(gp::Color(51, 153, 255), 1.0));
        bound_brush_.reset(new gp::SolidBrush(gp::Color(150, 155, 192, 231)));
    }
    virtual unsigned int getID()
    {
        return ID_ZOOM;
    }

    int getGroupID()
    {
        return BASE_RADIO_GROUP;
    }

    virtual bool isEnable()
    {
        return true;
    }

    virtual Command_exe_type getCommandType()
    {
        return  Command_exe_type::LikeRadioButton;
    }

    virtual void OnChecked()
    {
        action_slot_ = map_->action_draw_event.connect(std::bind(
            &ZoomCommand::ActionPaint, this, holder::_1, holder::_2));

        mouse_ldown_slot_ = map_->mouse_ldown_event.connect(
            std::bind(&ZoomCommand::mouse_ldown, this, holder::_1, holder::_2, holder::_3));

        mouse_lup_slot_ = map_->mouse_lup_event.connect(
            std::bind(&ZoomCommand::mouse_lup, this, holder::_1, holder::_2, holder::_3));

        mouse_move_slot_ = map_->mouse_move_event.connect(
            std::bind(&ZoomCommand::mouse_move, this, holder::_1, holder::_2, holder::_3));

      
    }

    virtual void OnUnChecked()
    {
        if (action_slot_.isConnected() == true) {
            action_slot_.disconnect();
            map_->refresh();
        }
        mouse_ldown_slot_.disconnect();
        mouse_move_slot_.disconnect();
        mouse_lup_slot_.disconnect();


    }

    void ActionPaint(Gdiplus::Graphics * pg, CoordSys * pConv) {
        if (is_ldown == true) {
            auto spt = pConv->conv(pt_ldown);
            auto spt_end = pConv->conv(pt_moving);

            gp::PointF pt_left_top;
            gp::PointF pt_right_bottom;
            pt_left_top.X = MIN(spt.X, spt_end.X);
            pt_left_top.Y = MIN(spt.Y, spt_end.Y);

            pt_right_bottom.X = MAX(spt.X, spt_end.X);
            pt_right_bottom.Y = MAX(spt.Y, spt_end.Y);


            gp::RectF rc(pt_left_top.X, pt_left_top.Y,
                pt_right_bottom.X - pt_left_top.X,
                pt_right_bottom.Y - pt_left_top.Y);

            pg->DrawRectangle(bound_pen_.get(), rc);
            pg->FillRectangle(bound_brush_.get(), rc);
        }
   
    }
    void mouse_ldown(double x, double y, UINT status) {
        pt_ldown.X() = x;
        pt_ldown.Y() = y;
        SetCapture(map_->getHWND());
        is_ldown = true;

    }
    void mouse_lup(double x, double y, UINT status) {
        is_ldown = false;
        if (GetCapture() == map_->getHWND()) {
            ReleaseCapture();
        }
        else{
            return;
        }

       

        //���Է�Χ̫С��
        double min_len = map_->ScreenSize2ViewSize(5);
        if ((pt_ldown - pt_moving).Length() < min_len)
            return;

        map_->refresh();
        map_->FitRectZoom(pt_ldown, pt_moving);

    }
    void mouse_move(double x, double y, UINT status) {
        vPoint cur_pt(x, y);
        pt_moving = cur_pt;
        if (status & MK_LBUTTON) {
            if (GetCapture() != map_->getHWND())
                return;
            map_->refresh();
            return;
        }
    }
private:
    std::unique_ptr<gp::Pen> bound_pen_;
    std::unique_ptr<gp::SolidBrush> bound_brush_;


    ActionDrawEventType::slot action_slot_;
    MouseEventType::slot mouse_ldown_slot_;
    MouseEventType::slot mouse_lup_slot_;
    MouseEventType::slot mouse_move_slot_;
    vPoint pt_moving;
    vPoint pt_ldown;

    bool is_ldown;

};
COMMAND_FACTORY_REGISTE(ZoomCommand);

