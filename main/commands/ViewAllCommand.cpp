#include "stdafx.h"
#include "command.h"
#include "Ribbon.h"



class ViewAllCommand : public ICommand
{
private:

public:
    ViewAllCommand()  {

    }
    virtual unsigned int getID()
    {
        return ID_VIEW_ALL;
    }

    int getGroupID()
    {
        return BASE_RADIO_GROUP;
    }

    virtual bool isEnable()
    {
        return true;
    }

    virtual Command_exe_type getCommandType()
    {
        return  Command_exe_type::LikeButton;
    }

    virtual void OnExecute(Command_value_type type, long value)
    {
        auto layer = map_->getActiveLayer();
        RtreeBound bb;
        if (layer->getBound(bb) == true) {
            map_->FitRectZoom(vPoint(bb.boundMin[0], bb.boundMin[1]), vPoint(bb.boundMax[0], bb.boundMax[1]));
        }
    }
private:


};
COMMAND_FACTORY_REGISTE(ViewAllCommand);

