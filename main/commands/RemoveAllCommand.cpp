#include "stdafx.h"
#include "command.h"
#include "Ribbon.h"



class RemoveAllCommand : public ICommand
{
private:

public:
    RemoveAllCommand()  {

    }
    virtual unsigned int getID()
    {
        return ID_REMOVE_ALL;
    }

    int getGroupID()
    {
        return BASE_RADIO_GROUP;
    }

    virtual bool isEnable()
    {
        return true;
    }

    virtual Command_exe_type getCommandType()
    {
        return  Command_exe_type::LikeButton;
    }

    virtual void OnExecute(Command_value_type type, long value)
    {
        if (IDYES != MessageBox(map_->getHWND(), L"��ȫ��ɾ����", L"����", MB_YESNO |MB_ICONWARNING))
            return;

        for (auto & layer : map_->layers) {
            layer.second->RemoveAll();
        }
        map_->UnSelect();
        map_->refresh(draw_reson::draw_reson_Delete);

    }
private:


};
COMMAND_FACTORY_REGISTE(RemoveAllCommand);

