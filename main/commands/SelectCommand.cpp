#include "stdafx.h"
#include "command.h"
#include "Ribbon.h"
#include "misc.h"
#include "geom.h"


class SelectCommand : public RadionCommand
{
private:
    enum cmd_state {
        
        eBoundSelect = 0,
        eInvalid
    };
public:
    SelectCommand() : state_(cmd_state::eInvalid) {
        bound_pen_.reset(new gp::Pen(gp::Color(51, 153, 255), 1.0));
        bound_brush_.reset(new gp::SolidBrush(gp::Color(150, 255, 184, 149)));
    }
    virtual bool getDefaultCheckedValue() {
        return true;
    }
    virtual unsigned int getID()
    {
        return ID_SELECT;
    }

    int getGroupID()
    {
        return BASE_RADIO_GROUP;
    }

    virtual bool isEnable()
    {
        return true;
    }

    virtual Command_exe_type getCommandType()
    {
        return  Command_exe_type::LikeRadioButton;
    }

    virtual void OnChecked()
    {
        action_slot_ = map_->action_draw_event.connect(std::bind(
            &SelectCommand::ActionPaint, this, holder::_1, holder::_2));

        mouse_ldown_slot_ = map_->mouse_ldown_event.connect(
            std::bind(&SelectCommand::mouse_ldown, this, holder::_1, holder::_2, holder::_3));

        mouse_lup_slot_ = map_->mouse_lup_event.connect(
            std::bind(&SelectCommand::mouse_lup, this, holder::_1, holder::_2, holder::_3));

        mouse_move_slot_ = map_->mouse_move_event.connect(
            std::bind(&SelectCommand::mouse_move, this, holder::_1, holder::_2, holder::_3));

        state_ = cmd_state::eInvalid;
        pen_.reset(new gp::Pen(gp::Color(100, 125, 0, 251), 2.0f));
        pen_->SetDashStyle(gp::DashStyle::DashStyleDot);

        font_.reset(new gp::Font(L"Arial", 16));
        brush_.reset(new gp::SolidBrush(gp::Color(180, 255, 0, 0)));
    }

    virtual void OnUnChecked()
    {
        if (action_slot_.isConnected() == true) {
            action_slot_.disconnect();
            map_->refresh();
        }
        mouse_ldown_slot_.disconnect();
        mouse_move_slot_.disconnect();
        mouse_lup_slot_.disconnect();

        pen_.release();
        font_.release();
        brush_.release();
    }

    void ActionPaint(Gdiplus::Graphics * pg, CoordSys * pConv) {
        if (state_ == eInvalid)
            return;

        auto spt = pConv->conv(pt_ldown);
        auto spt_end = pConv->conv(pt_moving);

        gp::PointF pt_left_top;
        gp::PointF pt_right_bottom;
        pt_left_top.X = min(spt.X, spt_end.X);
        pt_left_top.Y = min(spt.Y, spt_end.Y);

        pt_right_bottom.X = max(spt.X, spt_end.X);
        pt_right_bottom.Y = max(spt.Y, spt_end.Y);


        gp::RectF rc(pt_left_top.X, pt_left_top.Y,
            pt_right_bottom.X - pt_left_top.X,
            pt_right_bottom.Y - pt_left_top.Y);

        pg->DrawRectangle(bound_pen_.get(), rc);
        pg->FillRectangle(bound_brush_.get(), rc);
    }

    void mouse_ldown(double x, double y, UINT status) {
        pt_ldown.X() = x;
        pt_ldown.Y() = y;
        state_ = cmd_state::eBoundSelect;
        SetCapture(map_->getHWND());
    }

    void mouse_lup(double x, double y, UINT status) {
        state_ = cmd_state::eInvalid;
        if (GetCapture() == map_->getHWND()) {
            ReleaseCapture();
        }
        else{
            return;
        }
       
        double range = map_->ScreenSize2ViewSize(8);
        bool select_point_only = false;
        if (MK_CONTROL & status) {
            select_point_only = true;
        }

        vPoint cur_pt(x, y);
        std::vector<IDrawable *> selected;
        bool isSingleSelect = (cur_pt - pt_ldown).Length() < range;
        if (isSingleSelect) {
            //单选
            gu::Bound2d bound_single(cur_pt, range);
            
            for (auto & layer : map_->layers) {
                if (layer.second->visiable == true && layer.second->selectable == true)
                    layer.second->Query(bound_single, selected);
            }
            IDrawable * min_dist_element = nullptr;
            double min_dist = DBL_MAX;
            for (auto & sel_element : selected) {
                if (select_point_only == true && sel_element->getType() != GeomType::Point) {
                    continue;
                }
                auto sel_pt = sel_element->nearestPoint(cur_pt, range);
               
                double cur_dist = (sel_pt - cur_pt).Length();
                if (cur_dist < min_dist) {
                    min_dist_element = sel_element;
                    min_dist = cur_dist;
                }
            }
            if (nullptr == min_dist_element) {
                //这个要素实在不支持单选，那就多选
                if (selected.size() != 0)
                    map_->SelectMany(selected);
                else
                    map_->Select(min_dist_element);
            }
            else{
                map_->Select(min_dist_element);
            }
        }
        else{
            selected.clear();
            gu::Bound2d bound(cur_pt, pt_ldown);
            for (auto & layer : map_->layers) {
                if (layer.second->visiable == true && layer.second->selectable == true)
                    layer.second->Query(bound, selected);
            }
            map_->SelectMany(selected);
        }
        map_->refresh(draw_reson::draw_reson_highlight);
    }

    void mouse_move(double x, double y, UINT status) {
        vPoint cur_pt(x, y);
        pt_moving = cur_pt;
        if (status & MK_LBUTTON) {
            if (GetCapture() != map_->getHWND())
                return;
            state_ = cmd_state::eBoundSelect;
            map_->refresh();
            return;
        }

    }
private:
    std::unique_ptr<gp::Pen> pen_;
    std::unique_ptr<gp::Font> font_;
    std::unique_ptr<gp::SolidBrush> brush_;

    std::unique_ptr<gp::Pen> bound_pen_;
    std::unique_ptr<gp::SolidBrush> bound_brush_;


    ActionDrawEventType::slot action_slot_;
    MouseEventType::slot mouse_ldown_slot_;
    MouseEventType::slot mouse_lup_slot_;
    MouseEventType::slot mouse_move_slot_;
    vPoint pt_moving;
    vPoint pt_ldown;

    //std::vector<IDrawable *> selected_elements;
    //IDrawable * min_dist_element;
    cmd_state state_;
    
};
COMMAND_FACTORY_REGISTE(SelectCommand);

