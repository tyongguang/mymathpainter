#include "stdafx.h"
#include "command.h"
#include "Ribbon.h"
#include "layers/Layer_zindex_def.h"
class AxisShowCommand : public CheckBoxCommand
{
public:
    virtual bool getDefaultCheckedValue() {
        return map_->getAxisControl()->rw_visiable_;
    }
    virtual unsigned int getID()
    {
        return ID_SHOW_AXIS;
    }

    virtual bool isEnable()
    {
        return true;
    }

    virtual Command_exe_type getCommandType()
    {
        return  Command_exe_type::LikeCheckBox;
    }

    virtual void OnChecked()
    {
        map_->getAxisControl()->rw_visiable_ = true;
        map_->refresh();
    }

    virtual void OnUnChecked()
    {
        map_->getAxisControl()->rw_visiable_ = false;
        map_->refresh();
    }

};
COMMAND_FACTORY_REGISTE(AxisShowCommand);


class AxisShowMeasureScaleCommand : public CheckBoxCommand
{
public:
    virtual bool getDefaultCheckedValue() {
        return map_->getAxisControl()->rw_showMeasureScale_;
    }
    virtual unsigned int getID()
    {
        return ID_AXIS_SHOW_MEASURESCALE;
    }

    virtual bool isEnable()
    {
        return true;
    }

    virtual Command_exe_type getCommandType()
    {
        return  Command_exe_type::LikeCheckBox;
    }

    virtual void OnChecked()
    {
        map_->getAxisControl()->rw_showMeasureScale_ = true;
        map_->refresh();
    }

    virtual void OnUnChecked()
    {
        map_->getAxisControl()->rw_showMeasureScale_ = false;
        map_->refresh();
    }

};
COMMAND_FACTORY_REGISTE(AxisShowMeasureScaleCommand);




class AxisShowGridCommand : public CheckBoxCommand
{
public:
    virtual bool getDefaultCheckedValue() {
        return map_->getAxisControl()->rw_showGrid_;
    }
    virtual unsigned int getID()
    {
        return ID_AXIS_SHOW_GRID;
    }

    virtual bool isEnable()
    {
        return true;
    }

    virtual Command_exe_type getCommandType()
    {
        return  Command_exe_type::LikeCheckBox;
    }

    virtual void OnChecked()
    {
        map_->getAxisControl()->rw_showGrid_ = true;
        map_->refresh();
    }

    virtual void OnUnChecked()
    {
        map_->getAxisControl()->rw_showGrid_ = false;
        map_->refresh();
    }

};
COMMAND_FACTORY_REGISTE(AxisShowGridCommand);


class AxisShowGridTextCommand : public CheckBoxCommand
{
public:
    virtual bool getDefaultCheckedValue() {
        return map_->getAxisControl()->rw_showGridText_;
    }
    virtual unsigned int getID()
    {
        return ID_AXIS_SHOW_GRIDTEXT;
    }

    virtual bool isEnable()
    {
        return true;
    }

    virtual Command_exe_type getCommandType()
    {
        return  Command_exe_type::LikeCheckBox;
    }

    virtual void OnChecked()
    {
        map_->getAxisControl()->rw_showGridText_ = true;
        map_->refresh();
    }

    virtual void OnUnChecked()
    {
        map_->getAxisControl()->rw_showGridText_ = false;
        map_->refresh();
    }

};
COMMAND_FACTORY_REGISTE(AxisShowGridTextCommand);