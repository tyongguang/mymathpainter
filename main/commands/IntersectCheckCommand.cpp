#include "stdafx.h"
#include "command.h"
#include "Ribbon.h"
#include "misc.h"
#include "guimisc.h"

class IntersectCheckCommand : public CheckBoxCommand
{
public:
    virtual unsigned int getID()
    {
        return ID_INTERSECT;
    }
    
    int getGroupID()
    {
        return BASE_RADIO_GROUP;
    }

    virtual bool isEnable()
    {
        return true;
    }

    virtual Command_exe_type getCommandType()
    {
        return  Command_exe_type::LikeCheckBox;
    }

    virtual void OnChecked()
    {
        action_slot_ = map_->action_draw_event.connect(std::bind(
            &IntersectCheckCommand::ActionPaint, this, holder::_1, holder::_2));

//         mouse_ldown_slot_ = map_->mouse_ldown_event.connect(
//             std::bind(&IntersectCheckCommand::mouse_ldown, this, holder::_1, holder::_2, holder::_3));
        
        
        mouse_move_slot_ = map_->mouse_move_event.connect(
            std::bind(&IntersectCheckCommand::mouse_move, this, holder::_1, holder::_2, holder::_3));

        pen_.reset(new gp::Pen(gp::Color(100, 125, 0, 251), 2.0f) );
        pen_->SetDashStyle(gp::DashStyle::DashStyleDot);

        font_.reset(new gp::Font(L"Arial", 16));
        brush_.reset(new gp::SolidBrush(gp::Color::Blue));
        brush_intr_.reset(new gp::SolidBrush(gp::Color::Red));

        is_intr_ = false;

    }

    virtual void OnUnChecked()
    {
        if (action_slot_.isConnected() == true) {
            action_slot_.disconnect();
            map_->refresh();
        }
        mouse_ldown_slot_.disconnect();
        mouse_move_slot_.disconnect();

        pen_.release();
        font_.release();
        brush_.release();
        brush_intr_.release();
    }

    void ActionPaint(Gdiplus::Graphics * pg, CoordSys * pConv) {


        Gdiplus::Graphics & g = *pg;

        if (true == is_intr_) {
            auto smooth_mode = g.GetSmoothingMode();
            g.SetSmoothingMode(gp::SmoothingMode::SmoothingModeAntiAlias);
            auto sintr_pt = pConv->conv(intr_pt_);
            

            g.FillEllipse(brush_intr_.get(), gm::CenterPointSquare(sintr_pt, 6));
            sintr_pt.Y += -16;
            sintr_pt.X += 8;
            g.DrawString(L"(" + misc::format_point(intr_pt_.X(), intr_pt_.Y()) + L")", -1, font_.get(), sintr_pt, brush_.get());
            g.SetSmoothingMode(smooth_mode);
        }
        
    }
    void mouse_ldown(double x, double y, UINT status) {

            
    }
    void mouse_move(double x, double y, UINT status) {

        is_intr_ = false;
        vPoint pt_moving(x, y);
        vPoint cur_intr;
        
        is_intr_ = map_->getIntersectPoint(pt_moving, &cur_intr, 10);
        if (is_intr_ && intr_pt_ != cur_intr) {
            map_->Output(IMap::output_to::output_window, L"�ཻ��(" + misc::format_point(cur_intr.X(), cur_intr.Y()) + L")");
        }
        intr_pt_ = cur_intr;
        map_->refresh();
    }
private:

    std::unique_ptr<gp::Pen> pen_;
    std::unique_ptr<gp::Font> font_;
    std::unique_ptr<gp::SolidBrush> brush_;
    std::unique_ptr<gp::SolidBrush> brush_intr_;
    
    bool is_intr_;
    ActionDrawEventType::slot action_slot_;
    MouseEventType::slot mouse_ldown_slot_;
    MouseEventType::slot mouse_move_slot_;

    vPoint intr_pt_;
    
};
COMMAND_FACTORY_REGISTE(IntersectCheckCommand);

