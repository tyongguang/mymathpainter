#include "stdafx.h"
#include "command.h"
#include "Ribbon.h"
#include "misc.h"
#include "geom/geomLine.h"


class CreateLineCommand : public RadionCommand
{
private:
    enum Step {
        eRequireFirstPt = 0,
        eRequireSecondPt 
    };
public:
    virtual unsigned int getID()
    {
        return ID_CREATE_LINE;
    }
    
    int getGroupID()
    {
        return BASE_RADIO_GROUP;
    }

    virtual bool isEnable()
    {
        return true;
    }

    virtual Command_exe_type getCommandType()
    {
        return  Command_exe_type::LikeRadioButton;
    }

    virtual void OnChecked()
    {
        action_slot_ = map_->action_draw_event.connect(std::bind(
            &CreateLineCommand::ActionPaint, this, holder::_1, holder::_2));

        mouse_ldown_slot_ = map_->mouse_ldown_event.connect(
            std::bind(&CreateLineCommand::mouse_ldown, this, holder::_1, holder::_2, holder::_3));

        mouse_move_slot_ = map_->mouse_move_event.connect(
            std::bind(&CreateLineCommand::mouse_move, this, holder::_1, holder::_2, holder::_3));

        key_slot_ = map_->mouse_keyup_event.connect(std::bind(&CreateLineCommand::OnKeyBoradEvent, this,
            holder::_1, holder::_2, holder::_3));

        step_ = eRequireFirstPt;
        pen_.reset(new gp::Pen(gp::Color(100, 125, 0, 251), 2.0f) );
        pen_->SetDashStyle(gp::DashStyle::DashStyleSolid);

    }

    virtual void OnUnChecked()
    {
        if (action_slot_.isConnected() == true) {
            action_slot_.disconnect();
            map_->refresh();
        }
        mouse_ldown_slot_.disconnect();
        mouse_move_slot_.disconnect();
        key_slot_.disconnect();
        pen_.release();
    }


    void OnKeyBoradEvent(TCHAR vk, UINT cRepeat, UINT flags)
    {
        if (vk == VK_ESCAPE) {
            step_ = eRequireFirstPt;
            map_->refresh();
        }
    }
    void ActionPaint(Gdiplus::Graphics * pg, CoordSys * pConv) {
        if (step_ == eRequireFirstPt)
            return;

        Gdiplus::Graphics & g = *pg;
        
        if (step_ == eRequireSecondPt) {
            auto smooth_mode = g.GetSmoothingMode();
            g.SetSmoothingMode(gp::SmoothingMode::SmoothingModeAntiAlias);
            auto last_pt = pConv->conv(moving_point_);
            g.DrawLine(pen_.get(), pConv->conv(pt1_), last_pt);
            g.SetSmoothingMode(smooth_mode);
        }
        
    }
    void mouse_ldown(double x, double y, UINT status) {
        if (step_ == eRequireFirstPt) {
            pt1_.X() = x;
            pt1_.Y() = y;
            moving_point_.X() = x;
            moving_point_.Y() = y;
            step_ = eRequireSecondPt;
        }
        else if (step_ == eRequireSecondPt) {
            step_ = eRequireFirstPt;
            vPoint dir = vPoint(x, y) - pt1_;
            dir.Normalize();
            
            map_->getActiveLayer()->AddElement(std::shared_ptr<IDrawable>(new geomLine(pt1_, dir)));


            map_->refresh(draw_reson::draw_reson_Force);
        }
            
    }
    void mouse_move(double x, double y, UINT status) {
        if (step_ == eRequireFirstPt)
            return;
        moving_point_.X() = x;
        moving_point_.Y() = y;
        map_->refresh();
    }
private:
    std::unique_ptr<gp::Pen> pen_;


    ActionDrawEventType::slot action_slot_;
    MouseEventType::slot mouse_ldown_slot_;
    MouseEventType::slot mouse_move_slot_;
    Step step_;
    vPoint pt1_;
    vPoint moving_point_;
    KeyBoardEventType::slot key_slot_;
};
COMMAND_FACTORY_REGISTE(CreateLineCommand);

