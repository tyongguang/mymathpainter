#include "stdafx.h"
#include "command.h"
#include "Ribbon.h"
#include "misc.h"
#include "geom.h"
#include "geom/geomPoint.h"


class CreatePointCommand : public RadionCommand
{
public:
    CreatePointCommand() {
    }

    virtual unsigned int getID()
    {
        return ID_CREATE_POINT;
    }

    int getGroupID()
    {
        return BASE_RADIO_GROUP;
    }

    virtual bool isEnable()
    {
        return true;
    }

    virtual Command_exe_type getCommandType()
    {
        return  Command_exe_type::LikeRadioButton;
    }

    virtual void OnChecked()
    {
        mouse_ldown_slot_ = map_->mouse_ldown_event.connect(
            std::bind(&CreatePointCommand::mouse_ldown, this, holder::_1, holder::_2, holder::_3));


    }

    virtual void OnUnChecked()
    {
        mouse_ldown_slot_.disconnect();
    }

    void mouse_ldown(double x, double y, UINT status) {
        vPoint vpt(x, y);
        auto cur_layer = map_->getActiveLayer();
        cur_layer->AddElement(std::shared_ptr<IDrawable>(new geomPoint(vpt)));
        map_->refresh(draw_reson::draw_reson_Force);
    }

private:
    MouseEventType::slot mouse_ldown_slot_;

    
};
COMMAND_FACTORY_REGISTE(CreatePointCommand);

