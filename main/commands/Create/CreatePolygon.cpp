#include "stdafx.h"
#include "command.h"
#include "Ribbon.h"
#include "misc.h"
#include "geom.h"
#include "geom/geomPolygon.h"
#include "guimisc.h"
//#include "layers/MinDistPoint.h"


class CreatePolygon : public RadionCommand
{
private:
    enum cmd_state {
        eRequireFirstPt = 0,
        eRequireNext,
        eInvalid
    };
public:
    CreatePolygon() : state_(cmd_state::eInvalid) {
        capture_end_ = false;
    }
    virtual unsigned int getID()
    {
        return ID_CREATE_POLYGON;
    }

    int getGroupID()
    {
        return BASE_RADIO_GROUP;
    }

    virtual bool isEnable()
    {
        return true;
    }

    virtual Command_exe_type getCommandType()
    {
        return  Command_exe_type::LikeRadioButton;
    }

    virtual void OnChecked()
    {
        action_slot_ = map_->action_draw_event.connect(std::bind(
            &CreatePolygon::ActionPaint, this, holder::_1, holder::_2));

        mouse_ldown_slot_ = map_->mouse_ldown_event.connect(
            std::bind(&CreatePolygon::mouse_ldown, this, holder::_1, holder::_2, holder::_3));


        mouse_move_slot_ = map_->mouse_move_event.connect(
            std::bind(&CreatePolygon::mouse_move, this, holder::_1, holder::_2, holder::_3));

        key_slot_ = map_->mouse_keyup_event.connect(std::bind(&CreatePolygon::OnKeyBoradEvent, this,
            holder::_1, holder::_2, holder::_3));

        state_ = cmd_state::eRequireFirstPt;
        pen_.reset(new gp::Pen(gp::Color::Blue, 2.0f));
        pen_->SetDashStyle(gp::DashStyle::DashStyleSolid);
        brush_.reset(new gp::SolidBrush(gp::Color::SkyBlue));
        brush_capture_.reset(new gp::SolidBrush(gp::Color::Red));
        capture_end_ = false;
        
    }

    virtual void OnUnChecked()
    {
        if (action_slot_.isConnected() == true) {
            action_slot_.disconnect();
            map_->refresh();
        }
        mouse_ldown_slot_.disconnect();
        mouse_move_slot_.disconnect();

        pen_.release();
        brush_.release();
    }

    void OnKeyBoradEvent(TCHAR vk, UINT cRepeat, UINT flags)
    {
        if (vk == VK_ESCAPE) {
            state_ = eRequireFirstPt;
            map_->refresh();
        }
    }

    void ActionPaint(Gdiplus::Graphics * pg, CoordSys * pConv) {
        if (state_ != cmd_state::eRequireNext)
            return;

        if (vpts_.size() >= 2) {
            std::vector<gp::PointF> drawed_points;
            pConv->conv(vpts_.begin(), vpts_.end(), std::back_inserter(drawed_points));
            pg->DrawLines(pen_.get(), &drawed_points.front(), drawed_points.size());
            for (auto & spt : drawed_points) {
                pg->FillEllipse(brush_.get(), gm::CenterPointSquare(spt, (float)point_width));
            }
            if (capture_end_ == true) {
                pg->FillEllipse(brush_capture_.get(), gm::CenterPointSquare(drawed_points.front(), (float)point_width + 2));
            }
        }

        auto spt = pConv->conv(pre_point_);
        auto spt_end = pConv->conv(moving_point_);
        pg->DrawLine(pen_.get(), spt, spt_end);
        pg->FillEllipse(brush_.get(), gm::CenterPointSquare(spt_end, (float)point_width));
    }

    void mouse_ldown(double x, double y, UINT status) {

        if (capture_end_ == true) {
            capture_end_ = false;
            vpts_.push_back(vpts_.front());
            map_->getActiveLayer()->AddElement(std::shared_ptr<IDrawable>(new geomPolygon(vpts_, true)));
            map_->refresh(draw_reson::draw_reson_Force);
            state_ = cmd_state::eRequireFirstPt;
            map_->refresh();
            return;
        }

        if (cmd_state::eRequireFirstPt == state_) {
            vpts_.clear();
            pre_point_.X() = x;
            pre_point_.Y() = y;
            moving_point_ = pre_point_;
            vpts_.push_back(pre_point_);
            state_ = cmd_state::eRequireNext;
        }
        else if (cmd_state::eRequireNext == state_) {
            pre_point_.X() = x;
            pre_point_.Y() = y;
            moving_point_ = pre_point_;
            vpts_.push_back(pre_point_);
        }

    }

    void mouse_move(double x, double y, UINT status) {
        if (state_ != cmd_state::eRequireNext)
            return;

        vPoint cur_pt(x, y);
        moving_point_ = cur_pt;
      
        capture_end_ = false;
        if (vpts_.size() > 3) {
            double range = map_->ScreenSize2ViewSize(8);
            if ((cur_pt - vpts_.front()).SquaredLength() < range *range) {
                capture_end_ = true;
            }
        }
        map_->refresh();

    }

private:
    std::unique_ptr<gp::Pen> pen_;
    std::unique_ptr<gp::SolidBrush> brush_;
    std::unique_ptr<gp::SolidBrush> brush_capture_;


    ActionDrawEventType::slot action_slot_;
    MouseEventType::slot mouse_move_slot_;
    MouseEventType::slot mouse_ldown_slot_;

    vPoint moving_point_;
    vPoint pre_point_;
    bool capture_end_;
    

    std::vector<vPoint> vpts_;
    cmd_state state_;
    KeyBoardEventType::slot key_slot_;
    const static int point_width = 6;
};
COMMAND_FACTORY_REGISTE(CreatePolygon);

