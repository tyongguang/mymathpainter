#include "stdafx.h"
#include "command.h"
#include "Ribbon.h"
#include "Dialogs/CreateExpressionDlg.h"
#include "parser/ExpressionParser.h"
#include "geom/geomExpression.h"


class CreateExpressionCommand : public ICommand
{
private:

public:
    CreateExpressionCommand()  {

    }
    virtual unsigned int getID()
    {
        return ID_CREATE_EXPRESSION;
    }

    int getGroupID()
    {
        return BASE_RADIO_GROUP;
    }

    virtual bool isEnable()
    {
        return true;
    }

    virtual Command_exe_type getCommandType()
    {
        return  Command_exe_type::LikeButton;
    }

    virtual void OnExecute(Command_value_type type, long value)
    {
        while (1) {
            CreateExpressionDlg dlg;
            if (IDOK != dlg.DoModal())
                return;


            CW2A buffer(dlg.m_strCreatedText);
            ExpressionParser parser(buffer);

            if (true == parser.isOK()) {

                std::shared_ptr<IExpressionNode > expression;
                expression = parser.getResult();
                auto layer = map_->getActiveLayer();
                dlg.m_strCreatedText.Replace(L"\r\n", L" ");
                layer->AddElement(std::shared_ptr<IDrawable>(new geomExpression(
                    std::bind(&IExpressionNode::getValue, expression, holder::_1, holder::_2), dlg.m_strCreatedText)
                    ));
                map_->refresh(draw_reson::draw_reson_Force);
                return;

            }
            else {
                dlg.m_strCreatedText.Replace(L"\r\n", L" ");
                MessageBox(map_->getHWND(), 
                    L"表达式：" + dlg.m_strCreatedText + L"\r\n" + CA2W(parser.getMsg().c_str()),
                    L"发现错误", MB_OK);
            }
        }

    }
private:


};
COMMAND_FACTORY_REGISTE(CreateExpressionCommand);

