#include "stdafx.h"
#include "command.h"
#include "Ribbon.h"
#include "misc.h"
#include "guimisc.h"
#include "geom/geomLine.h"
#include "Wm5DistPoint2Segment2.h"
#include "Wm5Line2.h"
#include "Wm5IntrLine2Box2.h"
#include "geom.h"

class CreateTanVerCommand : public RadionCommand
{
public:
    virtual unsigned int getID()
    {
        return ID_TANGENT;
    }
    
    int getGroupID()
    {
        return BASE_RADIO_GROUP;
    }

    virtual bool isEnable()
    {
        return true;
    }

    virtual Command_exe_type getCommandType()
    {
        return  Command_exe_type::LikeRadioButton;
    }

    virtual void OnChecked()
    {
        action_slot_ = map_->action_draw_event.connect(std::bind(
            &CreateTanVerCommand::ActionPaint, this, holder::_1, holder::_2));

        mouse_ldown_slot_ = map_->mouse_ldown_event.connect(
            std::bind(&CreateTanVerCommand::mouse_ldown, this, holder::_1, holder::_2, holder::_3));

        mouse_move_slot_ = map_->mouse_move_event.connect(
            std::bind(&CreateTanVerCommand::mouse_move, this, holder::_1, holder::_2, holder::_3));

//         key_slot_ = map_->mouse_keyup_event.connect(std::bind(&CreateTanVerCommand::OnKeyBoradEvent, this,
//             holder::_1, holder::_2, holder::_3));


        tan_pen_.reset(new gp::Pen(gp::Color(180, 125, 0, 251), 2.0f));
        vetial_pen_.reset(new gp::Pen(gp::Color::OrangeRed, 2.0f));
        vetial_pen_->SetDashStyle(gp::DashStyle::DashStyleDot);


        is_found_ = false;

    }

    virtual void OnUnChecked()
    {
        if (action_slot_.isConnected() == true) {
            action_slot_.disconnect();
            map_->refresh();
        }
        mouse_ldown_slot_.disconnect();
        mouse_move_slot_.disconnect();
        key_slot_.disconnect();

        tan_pen_.release();
        vetial_pen_.release();

    }

    void DrawLine(gp::Pen * pen, Wm5::Line2d & line, Wm5::Box2d & bound_box, Gdiplus::Graphics & g, CoordSys * pConv) {
        Wm5::IntrLine2Box2d intr(line, bound_box);
        if (intr.Find() == false)
            return;

        if (Wm5::Intersector<double, Wm5::Vector2d>::IT_SEGMENT != intr.GetIntersectionType()) {
            return;
        }

        g.DrawLine(pen, pConv->conv(intr.GetPoint(0)), pConv->conv(intr.GetPoint(1)));
    }

    vPoint getVertical(Wm5::Segment2d & mSegment, vPoint & pt) {
        using namespace Wm5;
        vPoint diff = pt - mSegment.Center;
        auto param = mSegment.Direction.Dot(diff);

        if (-mSegment.Extent < param)
        {
            if (param < mSegment.Extent)
            {
                return mSegment.Center + param*mSegment.Direction;
            }
            else
            {
               mSegment.P1;
            }
        }
        return mSegment.P0;
    }

    void ActionPaint(Gdiplus::Graphics * pg, CoordSys * pConv) {
        Gdiplus::Graphics & g = *pg;
        if (true == is_found_) {

            RtreeBound bound;
            bound.boundMin[0] = pConv->base_conv->vleft();
            bound.boundMin[1] = pConv->base_conv->vbottom();

            bound.boundMax[0] = pConv->base_conv->vright();
            bound.boundMax[1] = pConv->base_conv->vtop();

            Wm5::Box2d bound_box;
            gu::CreateBoxFromBound(bound_box, bound);

            auto smooth_mode = g.GetSmoothingMode();
            g.SetSmoothingMode(gp::SmoothingMode::SmoothingModeAntiAlias);

            DrawLine(tan_pen_.get(), tan_line_, bound_box, g, pConv);
            DrawLine(vetial_pen_.get(), vetial_line_, bound_box, g, pConv);
 
            g.SetSmoothingMode(smooth_mode);
        }
    }

    void mouse_ldown(double x, double y, UINT status) {
        vPoint cur_pt(x, y);
        if (is_found_) {
            map_->getActiveLayer()->AddElement(std::shared_ptr<IDrawable>(new geomLine(tan_line_.Origin, tan_line_.Direction)));
            map_->getActiveLayer()->AddElement(std::shared_ptr<IDrawable>(new geomLine(vetial_line_.Origin, vetial_line_.Direction)));
            is_found_ = false;
            map_->refresh(draw_reson::draw_reson_Force);
        }
            
    }
    void mouse_move(double x, double y, UINT status) {
        pt_moving_.X() = x;
        pt_moving_.Y() = y;

        std::vector<Wm5::Segment2d> segments;
        is_found_ = map_->getSegments(pt_moving_, segments, 6);
        if (false == is_found_) {
            is_found_ = false;
            map_->refresh();
        }

        auto & iter = std::min_element(segments.begin(), segments.end(), [=](Wm5::Segment2d & seg1, Wm5::Segment2d & seg2) {
            Wm5::DistPoint2Segment2d dist_seg1(pt_moving_, seg1);
            Wm5::DistPoint2Segment2d dist_seg2(pt_moving_, seg2);
            return dist_seg1.GetSquared() < dist_seg2.GetSquared();
        });

        if (iter == segments.end()) {
            is_found_ = false;
            map_->refresh();
            return;
        }

        Wm5::Segment2d & seg = *iter;

        //tangent
        tan_line_.Origin = seg.Center;
        tan_line_.Direction = seg.Direction;

        //vertial
        auto ptIntr = getVertical(seg, pt_moving_);
        vetial_line_.Origin = ptIntr;
        vetial_line_.Direction = seg.Direction.Perp();


        map_->refresh();
    }

    void OnKeyBoradEvent(TCHAR vk, UINT cRepeat, UINT flags)
    {
    }

private:

    std::unique_ptr<gp::Pen> tan_pen_;
    std::unique_ptr<gp::Pen> vetial_pen_;

    ActionDrawEventType::slot action_slot_;
    MouseEventType::slot mouse_ldown_slot_;
    MouseEventType::slot mouse_move_slot_;
    KeyBoardEventType::slot key_slot_;

    Wm5::Line2d tan_line_;
    Wm5::Line2d vetial_line_;

    vPoint pt_moving_;
    bool is_found_;

};
COMMAND_FACTORY_REGISTE(CreateTanVerCommand);

