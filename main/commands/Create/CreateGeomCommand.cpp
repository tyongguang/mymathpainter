#include "stdafx.h"
#include "command.h"
#include "Ribbon.h"
#include "Dialogs/CreateGeomDlg.h"
#include "parser/GeomParser.h"


class CreateGeomCommand : public ICommand
{
private:

public:
    CreateGeomCommand()  {

    }
    virtual unsigned int getID()
    {
        return ID_FILE_NEW;
    }

    int getGroupID()
    {
        return BASE_RADIO_GROUP;
    }

    virtual bool isEnable()
    {
        return true;
    }

    virtual Command_exe_type getCommandType()
    {
        return  Command_exe_type::LikeButton;
    }

    virtual void OnExecute(Command_value_type type, long value)
    {
        CreateGeomDlg dlg;
        if (IDOK != dlg.DoModal())
            return;


        CW2A buffer(dlg.m_strCreatedText);
        GeomParser parser(buffer);
        if (true == parser.isOK()) {
            std::vector<std::shared_ptr<IDrawable> > vec;
            parser.getResult(vec);
            auto  layer = map_->getActiveLayer();
            for (auto & shared_draw : vec) {
                //暂时往最后面的Layer上插
                layer->AddElement(shared_draw);
            }

            if (dlg.m_isAutoZoom == TRUE) {
                RtreeBound bb;
                if (layer->getBound(bb) == true) {
                    map_->FitRectZoom(vPoint(bb.boundMin[0], bb.boundMin[1]), vPoint(bb.boundMax[0], bb.boundMax[1]));
                }
                else {
                    map_->refresh(draw_reson::draw_reson_Force);
                }
            }
            else {
                map_->refresh(draw_reson::draw_reson_Force);
            }
        }
        else {
            MessageBox(map_->getHWND(), CA2W(parser.getMsg().c_str()), L"发现错误", MB_OK);
        }

    }
private:


};
COMMAND_FACTORY_REGISTE(CreateGeomCommand);

