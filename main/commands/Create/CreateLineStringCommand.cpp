#include "stdafx.h"
#include "command.h"
#include "Ribbon.h"
#include "misc.h"
#include "geom.h"
#include "geom/geomLineString.h"
#include "guimisc.h"


class CreateLineStringCommand : public RadionCommand
{
private:
    enum cmd_state {
        eRequireFirstPt = 0,
        eRequireNext,
        eInvalid
    };
public:
    CreateLineStringCommand() : state_(cmd_state::eInvalid) {

    }
    virtual unsigned int getID()
    {
        return ID_CREATE_LINESTRING;
    }

    int getGroupID()
    {
        return BASE_RADIO_GROUP;
    }

    virtual bool isEnable()
    {
        return true;
    }

    virtual Command_exe_type getCommandType()
    {
        return  Command_exe_type::LikeRadioButton;
    }

    virtual void OnChecked()
    {
        action_slot_ = map_->action_draw_event.connect(std::bind(
            &CreateLineStringCommand::ActionPaint, this, holder::_1, holder::_2));

        mouse_ldown_slot_ = map_->mouse_ldown_event.connect(
            std::bind(&CreateLineStringCommand::mouse_ldown, this, holder::_1, holder::_2, holder::_3));

  
        mouse_move_slot_ = map_->mouse_move_event.connect(
            std::bind(&CreateLineStringCommand::mouse_move, this, holder::_1, holder::_2, holder::_3));

        mouse_ldbclick_slot_ = map_->mouse_ldbclick_event.connect(
            std::bind(&CreateLineStringCommand::mouse_ldbclick, this, holder::_1, holder::_2, holder::_3));

        key_slot_ = map_->mouse_keyup_event.connect(std::bind(&CreateLineStringCommand::OnKeyBoradEvent, this,
            holder::_1, holder::_2, holder::_3));

        state_ = cmd_state::eRequireFirstPt;
        pen_.reset(new gp::Pen(gp::Color::BlueViolet, 2.0f));
        pen_->SetDashStyle(gp::DashStyle::DashStyleSolid);
        brush_.reset(new gp::SolidBrush(gp::Color::Chocolate));

    }

    virtual void OnUnChecked()
    {
        if (action_slot_.isConnected() == true) {
            action_slot_.disconnect();
            map_->refresh();
        }
        mouse_ldown_slot_.disconnect();
        mouse_move_slot_.disconnect();
        mouse_ldbclick_slot_.disconnect();
        key_slot_.disconnect();

        pen_.release();
        brush_.release();
    }

    void OnKeyBoradEvent(TCHAR vk, UINT cRepeat, UINT flags)
    {
        if (vk == VK_ESCAPE) {
            state_ = cmd_state::eRequireFirstPt;
            map_->refresh();
        }
        else if (vk == VK_RETURN || vk == VK_SPACE) {
            map_->getActiveLayer()->AddElement(std::shared_ptr<IDrawable>(new geomLineString(vpts_, true)));
            map_->refresh(draw_reson::draw_reson_Force);
            state_ = cmd_state::eRequireFirstPt;
        }
    }

    void ActionPaint(Gdiplus::Graphics * pg, CoordSys * pConv) {
        if (state_ != cmd_state::eRequireNext)
            return;


        if (vpts_.size() >= 2) {
            std::vector<gp::PointF> drawed_points;
            pConv->conv(vpts_.begin(), vpts_.end(), std::back_inserter(drawed_points));
            pg->DrawLines(pen_.get(), &drawed_points.front(), drawed_points.size());
            for (auto & spt : drawed_points) {
                pg->FillEllipse(brush_.get(), gm::CenterPointSquare(spt, (float)point_width));
            }
        }

        auto spt = pConv->conv(pre_point_);
        auto spt_end = pConv->conv(moving_point_);
        pg->DrawLine(pen_.get(), spt, spt_end);
        pg->FillEllipse(brush_.get(), gm::CenterPointSquare(spt_end, (float)point_width));

    }

    void mouse_ldown(double x, double y, UINT status) {
        if (cmd_state::eRequireFirstPt == state_) {
            vpts_.clear();
            pre_point_.X() = x;
            pre_point_.Y() = y;
            moving_point_ = pre_point_;
            vpts_.push_back(pre_point_);
            state_ = cmd_state::eRequireNext;
        }
        else if (cmd_state::eRequireNext == state_) {
            pre_point_.X() = x;
            pre_point_.Y() = y;
            moving_point_ = pre_point_;
            vpts_.push_back(pre_point_);
        }
    }

    void mouse_move(double x, double y, UINT status) {
        if (state_ != cmd_state::eRequireNext)
            return;

        vPoint cur_pt(x, y);
        moving_point_ = cur_pt;
        map_->refresh();
    }

    void mouse_ldbclick(double x, double y, UINT status) {
        if (state_ != cmd_state::eRequireNext)
            return;

        map_->getActiveLayer()->AddElement(std::shared_ptr<IDrawable>(new geomLineString(vpts_, true)));
        map_->refresh(draw_reson::draw_reson_Force);
        state_ = cmd_state::eRequireFirstPt;
    }
    
private:
    std::unique_ptr<gp::Pen> pen_;
    std::unique_ptr<gp::Font> font_;
    std::unique_ptr<gp::SolidBrush> brush_;


    ActionDrawEventType::slot action_slot_;
    MouseEventType::slot mouse_move_slot_;
    MouseEventType::slot mouse_ldown_slot_;
    MouseEventType::slot mouse_ldbclick_slot_;
    KeyBoardEventType::slot key_slot_;

    vPoint moving_point_;
    vPoint pre_point_;

    std::vector<vPoint> vpts_;
    cmd_state state_;
    
    const static int point_width = 6;
};
COMMAND_FACTORY_REGISTE(CreateLineStringCommand);

