#include "stdafx.h"
#include "command.h"
#include "Ribbon.h"
#include "Dialogs/CreateGeomDlg.h"
#include "parser/GeomParser.h"


class OpenGeomCommand : public ICommand
{
private:

public:
    OpenGeomCommand()  {

    }
    virtual unsigned int getID()
    {
        return ID_FILE_OPEN;
    }

    int getGroupID()
    {
        return BASE_RADIO_GROUP;
    }

    virtual bool isEnable()
    {
        return true;
    }

    virtual Command_exe_type getCommandType()
    {
        return  Command_exe_type::LikeButton;
    }

    virtual void OnExecute(Command_value_type type, long value)
    {
        WTL::CFileDialog dlg(TRUE, L"txt", NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
            _T("文本文件\0*.txt\0所有文件\0*.*\0"));
        if (IDOK != dlg.DoModal())
            return ;

        std::fstream filereader;
        filereader.open(dlg.m_szFileName, std::ios_base::in | std::ios_base::binary);
        if (false == filereader.is_open())
        {
            MessageBox(map_->getHWND(), L"文件打开失败!", L"文件打开失败", MB_OK);
            return ;
        }

      
        filereader.seekg(0, std::ios::end);
        size_t ps = (size_t)filereader.tellg();  // 读取文件指针的位置
        filereader.seekg(0, std::ios::beg);
        std::unique_ptr<char[]> buffer(new char[ps + 1]);
        buffer[0] = '\0';
        filereader.read(buffer.get(), ps);
        filereader.close();
        buffer[ps] = '\0';

    
        GeomParser parser(buffer.get());
        if (true == parser.isOK()) {
            std::vector<std::shared_ptr<IDrawable> > vec;
            parser.getResult(vec);
			auto  layer = map_->getActiveLayer();
            for (auto & shared_draw : vec) {
                //暂时往最后面的Layer上插
                layer->AddElement(shared_draw);
            }
            RtreeBound bb;
            if (layer->getBound(bb) == true) {
                map_->FitRectZoom(vPoint(bb.boundMin[0], bb.boundMin[1]), vPoint(bb.boundMax[0], bb.boundMax[1]));
            }
        }
        else {
            MessageBox(map_->getHWND(), CA2W(parser.getMsg().c_str()), L"发现错误", MB_OK);
        }

    }
private:


};
COMMAND_FACTORY_REGISTE(OpenGeomCommand);

