#pragma once
#include "command.h"

//�������
class RoamCommand : public RadionCommand
{
public:
    RoamCommand();
    virtual void OnChecked();
    virtual void OnUnChecked();
    virtual unsigned int getID();
    virtual bool isEnable();
    virtual Command_exe_type getCommandType();
    virtual int getGroupID();
private:
    MouseEventType::slot mouse_move_slot;
    MouseEventType::slot mouse_ldown_slot;
    MouseEventType::slot mouse_lup_slot;

    void mouse_move(double x, double y, UINT status);
    void mouse_ldown(double x, double y, UINT status);
    void mouse_lup(double x, double y, UINT status);

    vPoint pt_down;
    CCursor cursor_;
};
COMMAND_FACTORY_REGISTE(RoamCommand);

//�Ҽ�����
class RightRoamCommand : public CheckBoxCommand
{
public:
    RightRoamCommand();
    virtual bool getDefaultCheckedValue();
    virtual void OnChecked();
    virtual void OnUnChecked();
    virtual unsigned int getID();
    virtual bool isEnable();
    virtual Command_exe_type getCommandType();
private:
    MouseEventType::slot mouse_move_slot;
    MouseEventType::slot mouse_rdown_slot;
    MouseEventType::slot mouse_rup_slot;

    void mouse_move(double x, double y, UINT status);
    void mouse_rdown(double x, double y, UINT status);
    void mouse_rup(double x, double y, UINT status);

    vPoint pt_down;
};
COMMAND_FACTORY_REGISTE(RightRoamCommand);