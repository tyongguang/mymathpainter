#include "stdafx.h"
#include "command.h"
#include "Ribbon.h"
#include "misc.h"
#include "geom.h"
#include "stl_util.h"


class AngleMeasureCommand : public RadionCommand
{
private:
    enum Step {
        eRequireFirstPt = 0,
        eRequireSecondPt,
        eRequireLastPt
    };
public:
    virtual unsigned int getID()
    {
        return ID_ANGLE_MEASURE;
    }

    int getGroupID()
    {
        return BASE_RADIO_GROUP;
    }

    virtual bool isEnable()
    {
        return true;
    }

    virtual Command_exe_type getCommandType()
    {
        return  Command_exe_type::LikeRadioButton;
    }

    virtual void OnChecked()
    {
        action_slot_ = map_->action_draw_event.connect(std::bind(
            &AngleMeasureCommand::ActionPaint, this, holder::_1, holder::_2));

        mouse_ldown_slot_ = map_->mouse_ldown_event.connect(
            std::bind(&AngleMeasureCommand::mouse_ldown, this, holder::_1, holder::_2, holder::_3));

        mouse_move_slot_ = map_->mouse_move_event.connect(
            std::bind(&AngleMeasureCommand::mouse_move, this, holder::_1, holder::_2, holder::_3));

        step_ = eRequireFirstPt;
        pen_.reset(new gp::Pen(gp::Color(100, 125, 0, 251), 2.0f));
        pen_->SetDashStyle(gp::DashStyle::DashStyleDot);

        font_.reset(new gp::Font(L"Arial", 16));
        brush_.reset(new gp::SolidBrush(gp::Color(150, 255, 0, 0)));


    }

    virtual void OnUnChecked()
    {
        if (action_slot_.isConnected() == true) {
            action_slot_.disconnect();
            map_->refresh();
        }
        mouse_ldown_slot_.disconnect();
        mouse_move_slot_.disconnect();
        pen_.release();
        font_.release();
        brush_.release();
    }

    void ActionPaint(Gdiplus::Graphics * pg, CoordSys * pConv) {
        if (step_ == eRequireFirstPt)
            return;

        Gdiplus::Graphics & g = *pg;

        if (step_ == eRequireSecondPt) {
            auto smooth_mode = g.GetSmoothingMode();
            g.SetSmoothingMode(gp::SmoothingMode::SmoothingModeAntiAlias);
            g.DrawLine(pen_.get(), pConv->conv(pt1), pConv->conv(pt_moving));


            g.SetSmoothingMode(smooth_mode);
        }
        else if (step_ == eRequireLastPt) {
            auto smooth_mode = g.GetSmoothingMode();
            g.SetSmoothingMode(gp::SmoothingMode::SmoothingModeAntiAlias);
            auto current_pt = pConv->conv(pt_moving);
            auto mid_pt = pConv->conv(pt2);

            g.DrawLine(pen_.get(), pConv->conv(pt1), pConv->conv(pt2));
            g.DrawLine(pen_.get(), pConv->conv(pt2), current_pt);

            vPoint dir1 = pt2 - pt1;
            vPoint dir2 = pt2 - pt_moving;
            dir1.Normalize();
            dir2.Normalize();
            double angle = gu::Radian2Angle(acos(dir1.Dot(dir2)));

            //点由上到下，或点由下到上
            int a1_dir = dir1.Y() > 0 ? 1 : -1;

            //结果正是顺时针，为负则为逆时针
            int a2_dir = dir2.DotPerp(dir1) > 0 ? 1 : -1;
            


            const float Radian_size = 200;
            mid_pt.X -= Radian_size /2;
            mid_pt.Y -= Radian_size / 2;
            gp::RectF rc(mid_pt, gp::SizeF(Radian_size, Radian_size));
            g.FillPie(brush_.get(), rc, (float)gu::Radian2Angle(acos(-dir1.X()))  * a1_dir, (float)angle * a2_dir);


            current_pt.Y += -16;
            g.DrawString(ut::CFormatW(L"%.1lf°", angle), -1, font_.get(), current_pt, brush_.get());
            g.SetSmoothingMode(smooth_mode);
        }

    }
    void mouse_ldown(double x, double y, UINT status) {
        if (step_ == eRequireFirstPt) {
            pt1.X() = x;
            pt1.Y() = y;
            pt_moving.X() = x;
            pt_moving.Y() = y;
            step_ = eRequireSecondPt;
        }
        else if (step_ == eRequireSecondPt) {
            step_ = eRequireLastPt;
            pt2.X() = x;
            pt2.Y() = y;
            pt_moving.X() = x;
            pt_moving.Y() = y;
        }
        else if (step_ == eRequireLastPt) {
            step_ = eRequireFirstPt;

            vPoint dir1 = pt2 - pt1;
            vPoint dir2 = pt2 - pt_moving;
            dir1.Normalize();
            dir2.Normalize();
            double angle = gu::Radian2Angle(acos(dir1.Dot(dir2)));

            map_->Output(IMap::output_window, ut::CFormatW(L"point_first(%s) -> point_mid(%s) -> point_last(%s), angle:%.1lf°",
                (LPCTSTR)misc::format_point(pt1.X(), pt1.Y()),
                (LPCTSTR)misc::format_point(pt2.X(), pt2.Y()),
                (LPCTSTR)misc::format_point(pt_moving.X(), pt_moving.Y()),
                angle
                ).c_str());


            map_->refresh();
        }

    }
    void mouse_move(double x, double y, UINT status) {
        if (step_ == eRequireFirstPt)
            return;
        pt_moving.X() = x;
        pt_moving.Y() = y;
        map_->refresh();
    }
private:
    std::unique_ptr<gp::Pen> pen_;
    std::unique_ptr<gp::Font> font_;
    std::unique_ptr<gp::SolidBrush> brush_;


    ActionDrawEventType::slot action_slot_;
    MouseEventType::slot mouse_ldown_slot_;
    MouseEventType::slot mouse_move_slot_;
    Step step_;
    vPoint pt1;
    vPoint pt2;
    vPoint pt_moving;

};
COMMAND_FACTORY_REGISTE(AngleMeasureCommand);

