#include "stdafx.h"
#include "command.h"
#include "Ribbon.h"
#include "misc.h"
#include "guimisc.h"
#include "stl_util.h"

class DistMeasureCommand : public RadionCommand
{
private:
    enum cmd_state {
        eRequireFirstPt = 0,
        eRequireNext,
        eInvalid 
    };
public:
    virtual unsigned int getID()
    {
        return ID_DIST_MEASURE;
    }
    
    int getGroupID()
    {
        return BASE_RADIO_GROUP;
    }

    virtual bool isEnable()
    {
        return true;
    }

    virtual Command_exe_type getCommandType()
    {
        return  Command_exe_type::LikeRadioButton;
    }

    virtual void OnChecked()
    {
        action_slot_ = map_->action_draw_event.connect(std::bind(
            &DistMeasureCommand::ActionPaint, this, holder::_1, holder::_2));

        mouse_ldown_slot_ = map_->mouse_ldown_event.connect(
            std::bind(&DistMeasureCommand::mouse_ldown, this, holder::_1, holder::_2, holder::_3));

        mouse_move_slot_ = map_->mouse_move_event.connect(
            std::bind(&DistMeasureCommand::mouse_move, this, holder::_1, holder::_2, holder::_3));

        mouse_ldbclick_slot_ = map_->mouse_ldbclick_event.connect(
            std::bind(&DistMeasureCommand::mouse_ldbclick, this, holder::_1, holder::_2, holder::_3));

        key_slot_ = map_->mouse_keyup_event.connect(std::bind(&DistMeasureCommand::OnKeyBoradEvent, this,
            holder::_1, holder::_2, holder::_3));
        

        state_ = eRequireFirstPt;
        pen_.reset(new gp::Pen(gp::Color(100, 125, 0, 251), 2.0f) );
        pen_->SetDashStyle(gp::DashStyle::DashStyleDot);

        font_.reset(new gp::Font(L"Arial", 16));
        brush_.reset(new gp::SolidBrush(gp::Color(150, 255, 0, 0)));
        brush_capture_.reset(new gp::SolidBrush(gp::Color::Red));

    }

    virtual void OnUnChecked()
    {
        if (action_slot_.isConnected() == true) {
            action_slot_.disconnect();
            map_->refresh();
        }
        mouse_ldown_slot_.disconnect();
        mouse_move_slot_.disconnect();
        mouse_ldbclick_slot_.disconnect();
        key_slot_.disconnect();

        pen_.release();
        font_.release();
        brush_.release();
        brush_capture_.release();
    }
    double getLen() {
        double len = 0.0f;
        int pt_size = vpts_.size() - 1;
        for (int i = 0; i < pt_size; ++i) {
            len += (vpts_[i] - vpts_[i + 1]).Length();
        }
        len += (pre_point_ - moving_point_).Length();
        return len;
    }
    void ActionPaint(Gdiplus::Graphics * pg, CoordSys * pConv) {
        if (state_ != cmd_state::eRequireNext)
            return;

        if (vpts_.size() >= 2) {
            std::vector<gp::PointF> drawed_points;
            pConv->conv(vpts_.begin(), vpts_.end(), std::back_inserter(drawed_points));
            pg->DrawLines(pen_.get(), &drawed_points.front(), drawed_points.size());
        }

        auto spt = pConv->conv(pre_point_);
        auto spt_end = pConv->conv(moving_point_);
        pg->DrawLine(pen_.get(), spt, spt_end);

        double len = getLen();
        
        auto last_pt = pConv->conv(moving_point_);
        last_pt.Y += -16;
        pg->DrawString(misc::format_double(len), -1, font_.get(), last_pt, brush_.get());
        
    }
    void mouse_ldown(double x, double y, UINT status) {
        if (cmd_state::eRequireFirstPt == state_) {
            vpts_.clear();
            pre_point_.X() = x;
            pre_point_.Y() = y;
            moving_point_ = pre_point_;
            vpts_.push_back(pre_point_);
            state_ = cmd_state::eRequireNext;
        }
        else if (cmd_state::eRequireNext == state_) {
            pre_point_.X() = x;
            pre_point_.Y() = y;
            moving_point_ = pre_point_;
            vpts_.push_back(pre_point_);
        }
    }
    void mouse_move(double x, double y, UINT status) {
        if (state_ != cmd_state::eRequireNext)
            return;

        vPoint cur_pt(x, y);
        moving_point_ = cur_pt;
        map_->refresh();
    }

    void OutputLen() {
        if (state_ != cmd_state::eRequireNext)
            return;

        state_ = cmd_state::eRequireFirstPt;
        
        double len = getLen();
        CString strAll;
        
        for (auto & pt : vpts_) {
            strAll.Append(L"(");
            strAll.Append(misc::format_point(pt.X(), pt.Y()));
            strAll.Append(L")");
            strAll.Append(L"->");
        }

        if (vpts_.back() != moving_point_) {
            strAll.Append(L"(");
            strAll.Append(misc::format_point(moving_point_.X(), moving_point_.Y()));
            strAll.Append(L")");
        }
        else {
            strAll.Delete(strAll.GetLength() - 2, 2);
        }

        strAll.AppendFormat(L" => %s", misc::format_double(len));
        map_->Output(IMap::output_window, strAll);
        map_->refresh();
        vpts_.clear();
    }

    void OnKeyBoradEvent(TCHAR vk, UINT cRepeat, UINT flags) {
        if (vk == VK_ESCAPE || vk == VK_RETURN || vk == VK_SPACE) {
            OutputLen();
        }
    }

    void mouse_ldbclick(double x, double y, UINT status) {
        OutputLen();
    }
private:
    std::unique_ptr<gp::Pen> pen_;
    std::unique_ptr<gp::Font> font_;
    std::unique_ptr<gp::SolidBrush> brush_;
    std::unique_ptr<gp::SolidBrush> brush_capture_;
    

    ActionDrawEventType::slot action_slot_;
    MouseEventType::slot mouse_ldown_slot_;
    MouseEventType::slot mouse_move_slot_;
    MouseEventType::slot mouse_ldbclick_slot_;
    KeyBoardEventType::slot key_slot_;


    vPoint moving_point_;
    vPoint pre_point_;

    std::vector<vPoint> vpts_;
    cmd_state state_;

};
COMMAND_FACTORY_REGISTE(DistMeasureCommand);

