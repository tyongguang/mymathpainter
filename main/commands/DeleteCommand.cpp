#include "stdafx.h"
#include "command.h"
#include "Ribbon.h"

class DeleteCommand : public ICommand
{
private:

public:
    DeleteCommand()  {

    }
    virtual void OnConnected() {
        root_->UIEnable(getID(), false);
        select_changed_slot_ = map_->select_changed_event.connect(std::bind(&DeleteCommand::SelectChanged,
            this, holder::_1));
    }

    virtual void OnDisConnected() {
        root_ = nullptr;
        select_changed_slot_.disconnect();
    }

    virtual unsigned int getID()
    {
        return ID_DELETE;
    }

    int getGroupID()
    {
        return BASE_RADIO_GROUP;
    }

    virtual bool isEnable()
    {
        return true;
    }

    virtual Command_exe_type getCommandType()
    {
        return  Command_exe_type::LikeButton;
    }

    virtual void OnExecute(Command_value_type type, long value)
    {
        std::vector<IDrawable *> vec;
        map_->GetSelect(vec);
        for (auto & element : vec) {
            element->Delete();
        }
        map_->Select(nullptr);
        map_->refresh(draw_reson::draw_reson_Delete);
    }

    void SelectChanged(const std::vector<IDrawable *> * selected_element)
    {
        if (selected_element->size() == 0)
        {
            root_->UIEnable(getID(), false);
        }
        else {
            root_->UIEnable(getID(), true);
        }
    }
private:
    SelectChangedType::slot select_changed_slot_;
};
COMMAND_FACTORY_REGISTE(DeleteCommand);

