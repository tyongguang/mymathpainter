#include "stdafx.h"
#include "RoamCommand.h"
#include "Ribbon.h"


RoamCommand::RoamCommand()
{
    cursor_ = ::LoadCursor(NULL, IDC_SIZEALL);
}

void RoamCommand::OnChecked()
{
    mouse_move_slot = map_->mouse_move_event.connect(
        std::bind(&RoamCommand::mouse_move, this, holder::_1, holder::_2, holder::_3));

    mouse_ldown_slot = map_->mouse_ldown_event.connect(
        std::bind(&RoamCommand::mouse_ldown, this,  holder::_1, holder::_2, holder::_3));

    mouse_lup_slot = map_->mouse_lup_event.connect(
        std::bind(&RoamCommand::mouse_lup, this, holder::_1, holder::_2, holder::_3));

    map_->AddCursorFront(cursor_);
}

void RoamCommand::OnUnChecked()
{
    mouse_move_slot.disconnect();
    mouse_ldown_slot.disconnect();
    mouse_lup_slot.disconnect();
    map_->RemoveCursor(cursor_);
}

unsigned int RoamCommand::getID()
{
    return ID_ROAM;
}

bool RoamCommand::isEnable()
{
    return true;
}

Command_exe_type RoamCommand::getCommandType()
{
    return Command_exe_type::LikeRadioButton;
}

int RoamCommand::getGroupID()
{
    return BASE_RADIO_GROUP;
}

void RoamCommand::mouse_move(double x, double y, UINT status)
{
    if (map_ == nullptr)
        return;

    if (0 == (status & MK_LBUTTON)) {
        return;
    }

    map_->offset(x - pt_down.X(), y - pt_down.Y());
}

void RoamCommand::mouse_ldown(double x, double y, UINT status)
{
    if (map_ == nullptr)
        return;

    pt_down.X() = x;
    pt_down.Y() = y;
    SetCapture(map_->getHWND());

}

void RoamCommand::mouse_lup(double x, double y, UINT status)
{
    if (map_ == nullptr)
        return;

    ReleaseCapture();
}

RightRoamCommand::RightRoamCommand()
{
    mouse_move_slot.disconnect();
    mouse_rdown_slot.disconnect();
    mouse_rup_slot.disconnect();
}

bool RightRoamCommand::getDefaultCheckedValue()
{
    return true;
}

void RightRoamCommand::OnChecked()
{
    mouse_move_slot = map_->mouse_move_event.connect(
        std::bind(&RightRoamCommand::mouse_move, this, holder::_1, holder::_2, holder::_3));

    mouse_rdown_slot = map_->mouse_rdown_event.connect(
        std::bind(&RightRoamCommand::mouse_rdown, this, holder::_1, holder::_2, holder::_3));

    mouse_rup_slot = map_->mouse_rup_event.connect(
        std::bind(&RightRoamCommand::mouse_rup, this, holder::_1, holder::_2, holder::_3));

}

void RightRoamCommand::OnUnChecked()
{
    mouse_move_slot.disconnect();
    mouse_rdown_slot.disconnect();
    mouse_rup_slot.disconnect();
}

unsigned int RightRoamCommand::getID()
{
    return ID_RIGHT_ROAM;
}

bool RightRoamCommand::isEnable()
{
    return true;
}

Command_exe_type RightRoamCommand::getCommandType()
{
    return Command_exe_type::LikeCheckBox;
}

void RightRoamCommand::mouse_move(double x, double y, UINT status)
{
    if (map_ == nullptr)
        return;

    if (0 == (status & MK_RBUTTON)) {
        return;
    }

    map_->offset(x - pt_down.X(), y - pt_down.Y());
}

void RightRoamCommand::mouse_rdown(double x, double y, UINT status)
{
    if (map_ == nullptr)
        return;

    pt_down.X() = x;
    pt_down.Y() = y;
    SetCapture(map_->getHWND());

}

void RightRoamCommand::mouse_rup(double x, double y, UINT status)
{
    if (map_ == nullptr)
        return;

    ReleaseCapture();
}

