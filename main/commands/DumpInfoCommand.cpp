#include "stdafx.h"
#include "command.h"
#include "Ribbon.h"

class DumpInfoCommand : public ICommand
{
private:

public:
    DumpInfoCommand()  {

    }
    virtual void OnConnected() {
        root_->UIEnable(getID(), false);
        select_changed_slot_ = map_->select_changed_event.connect(std::bind(&DumpInfoCommand::SelectChanged,
            this, holder::_1));
    }

    virtual void OnDisConnected() {
        root_ = nullptr;
        select_changed_slot_.disconnect();
    }

    virtual unsigned int getID()
    {
        return ID_DUMP_INFO;
    }

    int getGroupID()
    {
        return BASE_RADIO_GROUP;
    }

    virtual bool isEnable()
    {
        return true;
    }

    virtual Command_exe_type getCommandType()
    {
        return  Command_exe_type::LikeButton;
    }

    virtual void OnExecute(Command_value_type type, long value)
    {
        std::vector<IDrawable *> vec;
        map_->GetSelect(vec);
        CString all_msg;
        for (auto & element : vec) {
            CString str;
            if (true == element->dump_info(str)) {
                all_msg.Append(str);
                all_msg.Append(L"\r\n");
              
            }
        }
        map_->Output(IMap::output_window, all_msg);
    }

    void SelectChanged(const std::vector<IDrawable *> * selected_element)
    {
        if (selected_element->size() == 0)
        {
            root_->UIEnable(getID(), false);
        }
        else {
            root_->UIEnable(getID(), true);
        }
    }
private:
    SelectChangedType::slot select_changed_slot_;
};
COMMAND_FACTORY_REGISTE(DumpInfoCommand);

