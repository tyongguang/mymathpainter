#pragma once
#include <memory>

namespace of {
    template<class TBase>
    class IObjectFactory
    {
    public:
        virtual TBase * CreateObject() = 0;
        virtual std::shared_ptr<TBase> CreateSharedObject() = 0;
    };

    template<class TBase, class T>
    class ObjectFactory : public IObjectFactory < TBase >
    {
    public:
        virtual TBase * CreateObject() {
            return new T();
        }
        virtual std::shared_ptr<TBase> CreateSharedObject() {
            return std::shared_ptr<T>(new T());
        }
    };

    template<class TBase>
    class ObjectFactoryAutoCreator {
    public:
        static ObjectFactoryAutoCreator & Instance() {
            static ObjectFactoryAutoCreator container;
            return container;
        }
        IObjectFactory<TBase> * Register(IObjectFactory<TBase> * cmd_factory) {
            cmd_factory_.push_back(std::shared_ptr<IObjectFactory<TBase>>(cmd_factory));
            return cmd_factory;
        }
        std::vector<std::shared_ptr<IObjectFactory<TBase>> > cmd_factory_;
    private:
        ObjectFactoryAutoCreator(){
        }

    };
}

#define OBJECT_FACTORY_REGISTE(base_class, command_class_name) \
	__declspec(selectany)  of::IObjectFactory<base_class> * factory_##command_class_name = of::ObjectFactoryAutoCreator<base_class>::Instance().Register(new of::ObjectFactory<base_class, command_class_name>())

/*
usage:
1.register
OBJECT_FACTORY_REGISTE(IMyCommand, ConnCommand);

2.createobject
for (auto & factory : of::ObjectFactoryAutoCreator<IMyCommand>::Instance().cmd_factory_) {
    IMyCommand * cmd = factory->CreateObject());
}
*/