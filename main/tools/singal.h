#pragma once
#include <map>
#include <functional>
#include <memory>

//已知存在的小问题
//构造方式必须使用std::function
//如 auto_signal<void(int, int) > s; 会编译失败
//必须如下： auto_signal<std::function<void(int, int)> > s;
template<class Func>
class signal {
public:
    typedef typename int Key;

    signal() :nextKey(0){
    }

    template<class FuncLike>
    Key connect(FuncLike f) {
        Key k = nextKey++;
        connections[k] = f;
        return k;
    }

    void disconnect(Key k){
        connections.erase(k);
    }

    template<class  ...Args>
    void call(Args... args) {
        for (auto &connection : connections){
            connection.second(std::forward<Args>(args)...);
        }
    }
private:
    Key nextKey;
    std::map<Key, Func> connections;
};

template<class Func>
class auto_signal {
public:
    class slot
    {
    public:
        //强制右值赋值！
        slot() {
        }

        slot(std::shared_ptr<signal<Func> > & i_signal, typename signal<Func>::Key & i_k)
            : w_singal_(i_signal), cur_key_(i_k) {
        }

        slot(slot && sl) : cur_key_(sl.cur_key_) {
            w_singal_.swap(sl.w_singal_);
        }

        void operator = (slot && sl) {
            swap(sl);
        }

        ~slot() {
            disconnect();
        }

        void swap(slot & sl) {
            disconnect();
            cur_key_ = sl.cur_key_;
            w_singal_.swap(sl.w_singal_);
        }

        void disconnect() {
            auto s_signal = w_singal_.lock();
            if (s_signal.get() == nullptr) {
                return;
            }
            s_signal->disconnect(cur_key_);
            w_singal_.reset();
        }
        bool isConnected() {
            return false == w_singal_.expired();
        }

        template<class FuncLike>
        void connect(auto_signal<Func> & s, FuncLike f) {
            this->cur_key_ = s.main_signal_->connect(f);
            w_singal_ = s.main_signal_;
        }
    private:
        slot(const slot & src);
        void operator = (const slot & src);
        std::weak_ptr<signal<Func> > w_singal_;
        typename signal<Func>::Key cur_key_;
    };

    friend slot;
    auto_signal() : main_signal_(std::make_shared<signal<Func>>()) {
    }

    template<class FuncLike>
    slot connect(FuncLike f) {
        auto k = main_signal_->connect(f);
        return slot(main_signal_, k);
    }

    void disconnect(typename signal<Func>::Key & k){
        main_signal_->disconnect(k);
    }

    template<class  ...Args>
    void call(Args... args) {
        main_signal_->call(std::forward<Args>(args)...);
    }

protected:
    std::shared_ptr<signal<Func> > main_signal_;
};