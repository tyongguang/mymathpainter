#ifndef __util_tyg_
#define __util_tyg_
#include <ctime>

class TimeMeasure {
public:
    TimeMeasure()
        : begin(std::clock()) {
        }
    void Restart() {
        begin  = std::clock();
    }
    unsigned long Elapse() {
        return std::clock() - begin;
    }
    double ElapseSecond() {
        return double(std::clock() - begin) / CLOCKS_PER_SEC;
    }
private:
    std::clock_t begin;
};
#endif
