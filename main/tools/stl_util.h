#ifndef __stl_util_h__
#define __stl_util_h__
#include <stdarg.h>
#include <sstream>
#include <vector>
#include <iterator>

namespace ut {
    class CFormat
    {
        public:
            CFormat(const char * fmt, ...) {
                if (NULL != fmt)
                {
                    va_list marker;
                    va_start(marker, fmt);
                    size_t nLength = _vscprintf(fmt, marker) + 1;
                    v.resize(nLength);
#ifndef WIN32
                    vsnprintf(&v[0],  nLength, fmt, marker);
#else
                    _vsnprintf_s(&v[0], v.size(), nLength, fmt, marker);
#endif
                    va_end(marker);
                }
            }

            template<class T>
            CFormat(const T & value) {
                std::stringstream ss;
                ss << value;
                ss.str().swap(v);
            }
            void swap(std::string & src) {
                v.swap(src);
            }
            const char * c_str() {
                return v.c_str();
            }
            const std::string & str() {
                return v;
            }
            operator const char * () {
                return v.c_str();
            }
        private:
#ifndef WIN32
            int _vscprintf (const char * format, va_list pargs) {
                int retval;
                va_list argcopy;
                va_copy(argcopy, pargs);
                retval = vsnprintf(NULL, 0, format, argcopy);
                va_end(argcopy);
                return retval;
            }
#endif
            std::string v;
    };


    class CFormatW
    {
    public:
        CFormatW(const wchar_t * fmt, ...) {
            if (NULL != fmt)
            {
                va_list marker;
                va_start(marker, fmt);
                size_t nLength = _vscwprintf(fmt, marker) + 1;
                v.resize(nLength);
#ifndef WIN32
                vsnprintf(&v[0], nLength, fmt, marker);
#else
                _vsnwprintf_s(&v[0], v.size(), nLength, fmt, marker);
#endif
                va_end(marker);
            }
        }

        template<class T>
        CFormatW(const T & value) {
            std::wstringstream ss;
            ss << value;
            ss.str().swap(v);
        }
        void swap(std::wstring & src) {
            v.swap(src);
        }
        const wchar_t * c_str() {
            return v.c_str();
        }
        const std::wstring & str() {
            return v;
        }
        operator const wchar_t * () {
            return v.c_str();
        }
    private:
#ifndef WIN32
        int _vscprintf(const char * format, va_list pargs) {
            int retval;
            va_list argcopy;
            va_copy(argcopy, pargs);
            retval = vsnprintf(NULL, 0, format, argcopy);
            va_end(argcopy);
            return retval;
        }
#endif
        std::wstring v;
    };

    template<template<class T, class All = std::allocator<T> > class Container, class TT>
    std::ostream & operator << (std::ostream & s , Container<TT> & c) {
        std::copy(c.begin(), c.end(), std::ostream_iterator<TT>(s, " ") );
        return s;
    }

    template<template<class T, class All = std::allocator<T> > class Container, class TT>
    void String2Container(const char * str, Container<TT> & container) {
        std::stringstream ss;
        ss << str;
        std::copy(std::istream_iterator<TT>(ss),
                std::istream_iterator<TT>(),
                std::back_inserter(container));
    }

    template <class ForwardIterator>
    void STLDeleteContainerPointers(ForwardIterator begin,
            ForwardIterator end) {
        while (begin != end) {
            ForwardIterator temp = begin;
            ++begin;
            delete *temp;
        }
    }

    template <class T>
    void STLDeleteElements(T *container) {
        if (!container) return;
        STLDeleteContainerPointers(container->begin(), container->end());
        container->clear();
    }

    template <class T>
    void STLDeleteValues(T *v) {
        if (!v) return;
        for (typename T::iterator i = v->begin(); i != v->end(); ++i) {
            delete i->second;
        }
        v->clear();
    }

}
#endif
