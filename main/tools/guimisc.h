#pragma  once

namespace gm
{
	class CClientRect:public CRect
	{
	public:
		CClientRect(HWND hWnd)
		{
			::GetClientRect(hWnd,this);
		}
	};

	class CWindowRect:public CRect
	{
	public:
		CWindowRect(HWND hWnd)
		{
			::GetWindowRect(hWnd,this);
		}
	};

	class CWindowText:public CString
	{
	public:
		CWindowText(HWND hWnd,int nMaxBuf=128)
		{
			::GetWindowText(hWnd,GetBuffer(nMaxBuf),nMaxBuf);
			ReleaseBuffer();
		}
	};

	class CWindowPlacement:public WINDOWPLACEMENT
	{
	public:
		CWindowPlacement(HWND hWnd,int nMaxBuf=128)
		{
			::GetWindowPlacement(hWnd,this);
		}
	};


    __inline static Gdiplus::Rect gdiConv(const RECT & rcSrc)
    {
        return Gdiplus::Rect(rcSrc.left, rcSrc.top, rcSrc.right - rcSrc.left, rcSrc.bottom - rcSrc.top);
    }

    __inline static Gdiplus::RectF CenterPointSquare(const Gdiplus::PointF & pt, float width)
    {
        Gdiplus::PointF center_pt(pt);
        center_pt.X -= width / 2;
        center_pt.Y -= width / 2;
        return Gdiplus::RectF(center_pt.X, center_pt.Y, width, width);
    }

    __inline static Gdiplus::RectF CenterPointRect(const Gdiplus::PointF & pt, float width, float height)
    {
        Gdiplus::PointF center_pt(pt);
        center_pt.X -= width / 2;
        center_pt.Y -= height / 2;
        return Gdiplus::RectF(center_pt.X, center_pt.Y, width, height);
    }

    __inline static Gdiplus::Point gdiConv(const POINT & pt)
    {
        return Gdiplus::Point(pt.x, pt.y);
    }
}