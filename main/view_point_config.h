#pragma once

#include "gdiplus.h"
#include "Wm5Vector2.h"


// struct vPoint
// {
//     vPoint(): x(0), y(0) {}
//     vPoint(double ix, double iy) :x(ix), y(iy) {}
//     vPoint(const vPoint & src) :x(src.x), y(src.y) {}
//     double x;
//     double y;
// };

typedef Wm5::Vector2d vPoint;