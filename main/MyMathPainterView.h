// MyMathPainterView.h : interface of the CMyMathPainterView class
//
/////////////////////////////////////////////////////////////////////////////

#pragma once
#include "atlgdix.h"
#include "layer.h"
#include "CCoordConvImpl.h"
#include <map>
#include <memory>
#include "singal.h"
#include "map.h"
#include "HighLightLayer.h"
#include "layers/AxisLayer.h"

class CMyMathPainterView 
    : public CWindowImpl<CMyMathPainterView>
    , public COffscreenDraw<CMyMathPainterView>
    , public CCoordConvImpl<CMyMathPainterView>
    , public IMap
{
public:
    CMyMathPainterView();
	DECLARE_WND_CLASS(NULL)

    typedef CCoordConvImpl<CMyMathPainterView> CoordinateSystem;
	BOOL PreTranslateMessage(MSG* pMsg);
    BOOL OnIdle();

	BEGIN_MSG_MAP(CMyMathPainterView)
        CHAIN_MSG_MAP(COffscreenDraw<CMyMathPainterView>)
        CHAIN_MSG_MAP(CCoordConvImpl<CMyMathPainterView>)
        MESSAGE_HANDLER(WM_CREATE, OnCreate)
        MESSAGE_HANDLER(WM_MOUSEWHEEL, OnMouseWheel)
        MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseEvent)
        MESSAGE_HANDLER(WM_LBUTTONDOWN, OnMouseEvent)
        MESSAGE_HANDLER(WM_LBUTTONUP, OnMouseEvent)
        MESSAGE_HANDLER(WM_LBUTTONDBLCLK, OnMouseEvent)
        MESSAGE_HANDLER(WM_RBUTTONDOWN, OnMouseEvent)
        MESSAGE_HANDLER(WM_RBUTTONUP, OnMouseEvent)
        MESSAGE_HANDLER(WM_MBUTTONDOWN, OnMouseEvent)
        MESSAGE_HANDLER(WM_MBUTTONDBLCLK, OnMouseEvent)
        MESSAGE_HANDLER(WM_KEYDOWN, OnKeyBoradEvent)
        MESSAGE_HANDLER(WM_KEYUP, OnKeyBoradEvent)
        MESSAGE_HANDLER(WM_SIZE, OnSize)
        MESSAGE_HANDLER(WM_SETCURSOR, OnSetCursor)
	END_MSG_MAP()

    //overwrite for COffscreenDraw
    void DoPaint(CDCHandle dc);

    //overwrite for CCoordConvImpl
    void OnLevelChanged(int before, int current);

    void refresh(int reason = draw_reson::draw_reson_Other);
    CWindow m_wndMain;
   
private:
    void CreateBaseLayer();

    LRESULT OnMouseWheel(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
    LRESULT OnMouseEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
    LRESULT OnKeyBoradEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
    LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
    LRESULT OnCreate(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
    LRESULT OnSetCursor(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

    virtual void offset(double x, double y);
    virtual void SetCenter(const vPoint & vpt);
    virtual vPoint  getCenter();

    virtual void ZoomDelta(int delta_value);
    virtual void ZoomAbs(int abs_value);
    virtual HWND getHWND() {
        return m_hWnd;
    }

    virtual void SelectMany(std::vector<IDrawable *> & elements, bool swap);
    virtual void Select(IDrawable * element);
    virtual void UnSelect();
    virtual void GetSelect(std::vector<IDrawable *> & elements);
    virtual void SpecialHeightLight(IDrawable * ele);

    virtual double ScreenSize2ViewSize(float size);
    virtual int getZoomLevel();
    virtual void FitRectZoom(const vPoint & pt1, const vPoint & pt2);
    virtual void Output(output_to where, const wchar_t * msg);
    virtual std::shared_ptr<ILayer> getActiveLayer();
    virtual void setActiveLayer(std::shared_ptr<ILayer> layer);
    virtual bool getCapturePoint(vPoint & pt, float pixel_range);
    virtual bool getRealPoint(vPoint & pt, float pixel_range);
    virtual bool getIntersectPoint(const vPoint & pt, vPoint * output_pt, float pixel_range = 8);
    virtual bool getSegments(const vPoint & pt, std::vector<Wm5::Segment2d> & segments, float pixel_range = 8.0F);

    virtual void AddMouseCaptureFilter(std::weak_ptr<IMouseCaptureFilter> filter);
    virtual void RemoveMouseCaptureFilter(std::weak_ptr<IMouseCaptureFilter> filter);

    virtual void AddCursorFront(HCURSOR cursor);
    virtual void AddCursorBack(HCURSOR cursor);
    virtual void RemoveCursor(HCURSOR cursor);

    virtual IAxisControl * getAxisControl();

private:
    std::list<HCURSOR> m_listCursor;
    std::set<UINT> m_message;
    int draw_reason_;

    HighLightLayer m_hlLayer;
    std::unique_ptr<AxisLayer> m_axis_layer;

    std::shared_ptr<ILayer> active_layer_;

};
