#include "stdafx.h"
#include <memory>
#include "layerView.h"
#include "misc/misc.h"
#include "layers/SimpleLayer.h"
#include "Dialogs/SetNameDlg.h"
#include <atltime.h>


layerView::layerView()
    :map_(nullptr)
{

}

layerView::~layerView()
{

}

#define LAYER_INDEX_ACTIVE 0
#define LAYER_INDEX_VISIBLE 1
#define LAYER_INDEX_SELECTABLE 2
#define LAYER_INDEX_NAME 3

LRESULT layerView::OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
    m_acitve.LoadIcon(IDI_LAYER_ACTIVE);
    m_hide.LoadIcon(IDI_LAYER_HIDE);
    m_select.LoadIcon(IDI_LAYER_SELECT);
    m_unselect.LoadIcon(IDI_LAYER_UNSELECT);
    m_show.LoadIcon(IDI_LAYER_SHOW);

    HWND hWndToolBar = m_tb.CreateSimpleTool(m_hWnd, ATL_IDW_TOOLBAR, 16, 16, 5);
    m_tb.AddBtn(this, IDS_NEWLAYER, IDB_NEW_LAYER, NULL, TBSTYLE_AUTOSIZE | BTNS_SHOWTEXT);
    m_tb.AddBtn(this, IDS_DELLAYER, IDB_REMOVE_LAYER, NULL, TBSTYLE_AUTOSIZE | BTNS_SHOWTEXT);
    m_tb.AddBtn(this, IDS_UP_LAYER, IDB_UP, NULL, TBSTYLE_AUTOSIZE | BTNS_SHOWTEXT);
    m_tb.AddBtn(this, IDS_DOWN_LAYER, IDB_DOWN, NULL, TBSTYLE_AUTOSIZE | BTNS_SHOWTEXT);
    m_tb.AddBtn(this, IDS_LAYER_RENAME, IDB_RENAME, NULL, TBSTYLE_AUTOSIZE | BTNS_SHOWTEXT);
    
  
     CreateSimpleReBar(ATL_SIMPLE_REBAR_NOBORDER_STYLE);
     AddSimpleReBarBand(hWndToolBar, NULL, TRUE);


     m_hWndClient = m_view.Create(m_hWnd, CRect(0, 0, 0, 0), NULL, LVS_OWNERDRAWFIXED | LVS_REPORT | LVS_SHAREIMAGELISTS | WS_CHILD | WS_HSCROLL | WS_VISIBLE | LVS_SINGLESEL, 0);
     m_view.AddColumn(L"  ", LAYER_INDEX_ACTIVE); //活动
     m_view.AddColumn(L"  ", LAYER_INDEX_VISIBLE); //可见
     m_view.AddColumn(L"  ", LAYER_INDEX_SELECTABLE); //可选
     m_view.InsertColumn(LAYER_INDEX_NAME, L"名称", LVCFMT_LEFT, cs_text_width);

     m_view.SetExtendedListViewStyle(LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER);



     m_view.ModifyStyle(0, LVS_NOCOLUMNHEADER, TRUE);
    return 0;
}

LRESULT layerView::OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
    return 0;
}

BOOL layerView::LoadBitmap(HBITMAP * pBitmap, UINT nResourceID)
{
    Gdiplus::Bitmap *  bitMap;
    if (FALSE == misc::LoadPNGFromRes(_Module.m_hInstResource, nResourceID, &bitMap))
        return FALSE;
    bitMap->GetHBITMAP(Gdiplus::Color(255, 255, 255, 255), pBitmap);
    delete bitMap;
    return TRUE;
}
std::shared_ptr<ILayer> layerView::newLayer(LPCTSTR szLayerName)
{
    std::shared_ptr<ILayer>  pLayer(new SimpleLayer());
    if (szLayerName == nullptr) {
        SetNameDlg dlg;
        dlg.m_strName = CTime::GetCurrentTime().Format(L"%Y%m%d%H%M%S");
        if (IDOK == dlg.DoModal()) {
            pLayer->name = dlg.m_strName;
        }
        else {
            pLayer->name = CTime::GetCurrentTime().Format(L"%Y%m%d%H%M%S");
        }
    }
    else {
        pLayer->name = szLayerName;
    }
   
    gm::CClientRect rc(map_->getHWND());
    pLayer->setSize(rc.Width(), rc.Height());
    float layer_key = map_->layers.size() == 0 ? 100.0f : map_->layers.rbegin()->first + 10.0f;
    map_->layers.insert(std::make_pair(layer_key, pLayer));
    int i = m_view.GetItemCount();
    m_view.AddItem(i, 0, L"");
    DWORD_PTR item_key = *(DWORD_PTR*)&layer_key;
    m_view.SetItemData(i, item_key);
    return pLayer;
}


LRESULT layerView::OnNewLayer(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
{
    newLayer();
    return 0;
}

LRESULT layerView::OnDelLayer(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
{
    int index = m_view.GetSelectedIndex();
    if (index == -1)
        return 0;


    bool needChangeActiveLayer = false;

    DWORD_PTR item_data = m_view.GetItemData(index);
    float layer_key = *(float*)&item_data;

    auto iter = map_->layers.find(layer_key);
    if (map_->getActiveLayer() == iter->second) {
        needChangeActiveLayer = true;
    }

    map_->layers.erase(layer_key);

    m_view.DeleteItem(index);

    if (needChangeActiveLayer == true) {
        if (map_->layers.size() == 0) {
            map_->setActiveLayer(newLayer());
        }
        else{
            map_->setActiveLayer(map_->layers.begin()->second);
        }
    }


    map_->refresh(draw_reson::draw_reson_Force);
    return 0;
}

void layerView::DrawItem(LPDRAWITEMSTRUCT di)
{
    CDCHandle dc(di->hDC);
    COLORREF crOldTextColor = dc.GetTextColor();
    COLORREF crOldBkColor = dc.GetBkColor();

    // Draw the text.  
    CRect cell_rcItem;


    auto layer = getLayer(di->itemID);
    if (layer.get() == nullptr)
        return;

    int column_count = m_view.GetHeader().GetItemCount();
    for (int i = 0; i < column_count; i++)
    {
        m_view.GetSubItemRect(di->itemID, i, LVIR_LABEL, cell_rcItem);
        int rcHeight = cell_rcItem.Height();
        int iconTop = cell_rcItem.top + (rcHeight - 16) / 2;
        if (i == LAYER_INDEX_ACTIVE) {
            if (layer == map_->getActiveLayer())
                dc.DrawIconEx(cell_rcItem.left, iconTop, m_acitve, 8, 16, 0, NULL, DI_NORMAL);
        }

        if (i == LAYER_INDEX_VISIBLE) {
            if (layer->visiable == false) {
                dc.DrawIconEx(cell_rcItem.left, iconTop, m_hide, 16, 16, 0, NULL, DI_NORMAL);
            }
            else{
                dc.DrawIconEx(cell_rcItem.left, iconTop, m_show, 16, 16, 0, NULL, DI_NORMAL);
            }
        }


        if (i == LAYER_INDEX_SELECTABLE) {
            if (layer->selectable == false) {
                dc.DrawIconEx(cell_rcItem.left, iconTop, m_unselect, 16, 16, 0, NULL, DI_NORMAL);
            }
            else{
                dc.DrawIconEx(cell_rcItem.left, iconTop, m_select, 16, 16, 0, NULL, DI_NORMAL);
            }
        }


        if (i == LAYER_INDEX_NAME) {
            if ((di->itemAction | ODA_SELECT) &&
                (di->itemState & ODS_SELECTED))
            {
                dc.SetTextColor(::GetSysColor(COLOR_HIGHLIGHTTEXT));
                dc.SetBkColor(::GetSysColor(COLOR_HIGHLIGHT));
                dc.FillSolidRect(&cell_rcItem,
                    ::GetSysColor(COLOR_HIGHLIGHT));
            }
            else
            {
                COLORREF bg_color;

                if (di->itemID % 2)
                    bg_color = RGB(244, 244, 240);
                else
                    bg_color = RGB(250, 250, 245);

                if ((di->itemAction | ODA_FOCUS) &&
                    (di->itemState & ODS_FOCUS))
                {
                    bg_color = misc::PixelAlpha(bg_color, RGB(90, 158, 241), 80);
                }
                dc.FillSolidRect(&cell_rcItem, bg_color);
            }
            cell_rcItem.left += 5;
            dc.DrawText(layer->name, -1, &cell_rcItem, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
        }
    }
    CPen penRowSplit;
    penRowSplit.CreatePen(PS_SOLID, 1, RGB(210, 210, 210));
    CPenHandle oldPen = dc.SelectPen(penRowSplit);
    dc.MoveTo(di->rcItem.left, di->rcItem.bottom - 1);
    dc.LineTo(di->rcItem.right, di->rcItem.bottom - 1);
    dc.SelectPen(oldPen);

    dc.SetTextColor(crOldTextColor);
    dc.SetBkColor(crOldBkColor);
}

void layerView::MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct)
{
//     CClientDC dc(m_hWnd);
//     TEXTMETRIC tm = { 0 };
//     dc.GetTextMetrics(&tm);

    lpMeasureItemStruct->itemHeight = 24 ;
}

LRESULT layerView::OnNotify(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
    LPNMHDR pNMHdr = (LPNMHDR)lParam;
    if (pNMHdr->hwndFrom == m_view)
    {
        LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHdr);
        if (pNMHdr->code == NM_CLICK) {
            ListCtrlClick(pNMItemActivate);
        }
        if (pNMHdr->code == NM_DBLCLK) {
            ListCtrlDbClick(pNMItemActivate);
        }
    }
    else {
        bHandled = FALSE;
    }
    return 0;
}

void layerView::ListCtrlClick(LPNMITEMACTIVATE item)
{
    CRect cell_rcItem;
    m_view.GetSubItemRect(item->iItem, item->iSubItem, LVIR_LABEL, cell_rcItem);
    if (cell_rcItem.PtInRect(item->ptAction) == FALSE)
        return;

    auto layer = getLayer(item->iItem);
    if (layer.get() == nullptr)
        return;
    bool layerChanged = false;
    switch (item->iSubItem)
    {
    case LAYER_INDEX_ACTIVE:
        map_->setActiveLayer(layer);
        m_view.Invalidate();
        layerChanged = true;
        break;
    case LAYER_INDEX_VISIBLE:
        layer->visiable = !layer->visiable;
        m_view.InvalidateRect(&cell_rcItem);
        layerChanged = true;
        break;
    case LAYER_INDEX_SELECTABLE:
        layer->selectable = !layer->selectable;
        m_view.InvalidateRect(&cell_rcItem);
        layerChanged = true;
        break;
    default:
        break;
    }
    if (true == layerChanged) {
        map_->UnSelect();
        map_->refresh(draw_reson::draw_reson_Force);
    }
   
}

void layerView::ListCtrlDbClick(LPNMITEMACTIVATE item)
{
    CRect cell_rcItem;
    m_view.GetSubItemRect(item->iItem, item->iSubItem, LVIR_LABEL, cell_rcItem);
    if (cell_rcItem.PtInRect(item->ptAction) == FALSE)
        return;

    auto layer = getLayer(item->iItem);
    if (layer.get() == nullptr)
        return;

    if (item->iSubItem != LAYER_INDEX_NAME)
        return;

    map_->setActiveLayer(layer);
    m_view.Invalidate();
    //map_->UnSelect();
    map_->refresh(draw_reson::draw_reson_Force);
}

void layerView::InitLayerView(IMap * map)
{
    map_ = map;
    map_->setActiveLayer(newLayer(L"默认图层"));
}

std::shared_ptr<ILayer> layerView::getLayer(int index)
{
    DWORD_PTR item_data = m_view.GetItemData(index);
    float layer_key = *(float*)&item_data;
    auto iter = map_->layers.find(layer_key);
    if (iter == map_->layers.end())
        return std::shared_ptr<ILayer>();

    return iter->second;
}

LRESULT layerView::OnSize(UINT /*uMsg*/, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
    bHandled = false;
    int cur_width = LOWORD(lParam);
    auto header = m_view.GetHeader();
    int column_count = header.GetItemCount() - 1;
    for (int i = 0; i < column_count; ++i) {
        cur_width -= m_view.GetColumnWidth(i);
    }
    if (m_view.GetStyle() & WS_VSCROLL) {
        cur_width -= GetSystemMetrics(SM_CXVSCROLL);
    }
    if (cur_width > cs_text_width)
        m_view.SetColumnWidth(column_count, cur_width);
    return 0;
}

LRESULT layerView::OnRename(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
{
    int index = m_view.GetSelectedIndex();
    if (-1 == index)
        return 0;
    auto layer = getLayer(index);
    SetNameDlg dlg;
    dlg.m_strName = layer->name;
    if (IDOK == dlg.DoModal()) {
        layer->name = dlg.m_strName;
        m_view.Invalidate();
    }
    return 0;
}
