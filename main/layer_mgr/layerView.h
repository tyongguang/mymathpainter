#pragma once
#include "YGToolBar.h"
#include "resource.h"
#include "guimisc.h"
#include "map.h"
class layerView
    : public CFrameWindowImpl < layerView >
    , public CYGToolBar::IResourceLoader
    , public COwnerDraw<layerView>
{
public:
    layerView();
    ~layerView();
    DECLARE_WND_CLASS(_T("layerView"))

    BEGIN_MSG_MAP(layerView)
        MESSAGE_HANDLER(WM_CREATE, OnCreate)
        MESSAGE_HANDLER(WM_DESTROY, OnDestroy)
        COMMAND_ID_HANDLER(IDS_NEWLAYER, OnNewLayer)
        COMMAND_ID_HANDLER(IDS_DELLAYER, OnDelLayer)
        COMMAND_ID_HANDLER(IDS_LAYER_RENAME, OnRename)
        
        MESSAGE_HANDLER(WM_NOTIFY, OnNotify)
        MESSAGE_HANDLER(WM_SIZE, OnSize)
        CHAIN_MSG_MAP(COwnerDraw<layerView>)
        CHAIN_MSG_MAP(CFrameWindowImpl<layerView>)
        REFLECT_NOTIFICATIONS()
    END_MSG_MAP()

    void InitLayerView(IMap * map);
    LRESULT OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
    LRESULT OnNotify(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
    LRESULT OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
    LRESULT OnSize(UINT /*uMsg*/, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

    virtual BOOL LoadBitmap(HBITMAP * pBitmap, UINT nResourceID);
    LRESULT OnNewLayer(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
    LRESULT OnDelLayer(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
    LRESULT OnRename(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
    void DrawItem(LPDRAWITEMSTRUCT /*lpDrawItemStruct*/);

    void MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct);

    void ListCtrlClick(LPNMITEMACTIVATE item);
    void ListCtrlDbClick(LPNMITEMACTIVATE item);
private:
    std::shared_ptr<ILayer> newLayer(LPCTSTR szLayerName = nullptr);
    std::shared_ptr<ILayer> getLayer(int index);
    CYGToolBar m_tb;
    CListViewCtrl m_view;

    const static int cs_text_width = 120;

    CIcon m_acitve;
    CIcon m_hide;
    CIcon m_select;
    CIcon m_unselect;
    CIcon m_show;

    IMap * map_;
};