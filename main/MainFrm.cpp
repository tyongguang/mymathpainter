// MainFrm.cpp : implmentation of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Ribbon.h"
#include "resource.h"

#include "aboutdlg.h"
#include "MyMathPainterView.h"
#include "MainFrm.h"
#include "misc.h"
#include "stl_util.h"



BOOL CMainFrame::PreTranslateMessage(MSG* pMsg)
{
	if(CRibbonFrameWindowImpl<CMainFrame>::PreTranslateMessage(pMsg))
		return TRUE;
     if (pMsg->message == WM_MOUSEWHEEL)
     {
         CWindow  wnd = WindowFromPoint(pMsg->pt);
         CWindow cur_focus_wnd = GetFocus();
         if (wnd != cur_focus_wnd)
         {
             wnd.SetFocus();
            // wnd.PostMessage(pMsg->message, pMsg->wParam, pMsg->lParam);
         }
     }
	return m_view.PreTranslateMessage(pMsg);
}

BOOL CMainFrame::OnIdle()
{
	UIUpdateToolBar();
    m_view.OnIdle();
	return FALSE;
}

LRESULT CMainFrame::OnCreate(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{


	bool bRibbonUI = RunTimeHelper::IsRibbonUIAvailable();
	if (bRibbonUI)
		UIAddMenu(GetMenu(), true);
	else
		CMenuHandle(GetMenu()).DeleteMenu(ID_VIEW_RIBBON, MF_BYCOMMAND);

	// create command bar window
	HWND hWndCmdBar = m_CmdBar.Create(m_hWnd, rcDefault, NULL, ATL_SIMPLE_CMDBAR_PANE_STYLE);
	// attach menu
	m_CmdBar.AttachMenu(GetMenu());
	// load command bar images
	m_CmdBar.LoadImages(IDR_MAINFRAME);
	// remove old menu
	SetMenu(NULL);

	HWND hWndToolBar = CreateSimpleToolBarCtrl(m_hWnd, IDR_MAINFRAME, FALSE, ATL_SIMPLE_TOOLBAR_PANE_STYLE);

	CreateSimpleReBar(ATL_SIMPLE_REBAR_NOBORDER_STYLE);
	AddSimpleReBarBand(hWndCmdBar);
	AddSimpleReBarBand(hWndToolBar, NULL, TRUE);

//	CreateSimpleStatusBar();

    m_hWndStatusBar = m_wndStatusBar.Create(*this);
    int anPanes[] = { ID_DEFAULT_PANE, IDS_MOUSE_POS, IDS_LEVEL };
    m_wndStatusBar.SetPanes(anPanes, _countof(anPanes), true);

	//m_hWndClient = m_view.Create(m_hWnd, rcDefault, NULL, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN, WS_EX_CLIENTEDGE);
    InitControls();
	UIAddToolBar(hWndToolBar);
	UISetCheck(ID_VIEW_TOOLBAR, 1);
	UISetCheck(ID_VIEW_STATUS_BAR, 1);

	// register object for message filtering and idle updates
	CMessageLoop* pLoop = _Module.GetMessageLoop();
	ATLASSERT(pLoop != NULL);
	pLoop->AddMessageFilter(this);
	pLoop->AddIdleHandler(this);

    ShowRibbonUI(bRibbonUI);
    UISetCheck(ID_VIEW_RIBBON, bRibbonUI);

    mosue_move_slot = m_view.mouse_move_event.connect([&](double x, double y, UINT key_status) {
        CString str;
        str.Append(misc::format_double(x));
        str.Append(L", ");
        str.Append(misc::format_double(y));
        m_wndStatusBar.SetPaneText(IDS_MOUSE_POS, str);
    });

    level_changed_slot = m_view.level_changed_event.connect([&](int , int after) {
        CString level;
        level.Format(L"L:%d", after);
        m_wndStatusBar.SetPaneText(IDS_LEVEL, level);
    });

    m_wndStatusBar.SetPaneText(IDS_LEVEL, ut::CFormatW(L"L:%d", m_view.getLevel()));
    
    UISetCheck(ID_RIGHT_ROAM, true, true);
    m_LayerView.InitLayerView(&m_view);
    m_selectView.InitSelectionView(&m_view);
    m_bookmarkView.InitBookMarkView(&m_view);
    
    InitCreateCommand();


    m_centerY.SetRepresentativeString(L"1122.3344");
    m_centerY.SetDecimalPlaces(5.0, true);
    m_centerY.SetMax(100 * 10000.0, true);
    m_centerY.SetMin(-100 * 10000.0, true);
    m_centerY.SetIncrement(1.12, true);


    
	return 0;
}


LRESULT CMainFrame::OnDestroy(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled)
{
	// unregister message filtering and idle updates
	CMessageLoop* pLoop = _Module.GetMessageLoop();
	ATLASSERT(pLoop != NULL);
	pLoop->RemoveMessageFilter(this);
	pLoop->RemoveIdleHandler(this);
    m_cmdRoot.UnRegisterAll();
	bHandled = FALSE;
	return 1;
}

LRESULT CMainFrame::OnFileExit(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	PostMessage(WM_CLOSE);
	return 0;
}

LRESULT CMainFrame::OnFileNew(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	// TODO: add code to initialize document
	return 0;
}

LRESULT CMainFrame::OnViewToolBar(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	static BOOL bVisible = TRUE;	// initially visible
	bVisible = !bVisible;
	CReBarCtrl rebar = m_hWndToolBar;
	int nBandIndex = rebar.IdToIndex(ATL_IDW_BAND_FIRST + 1);	// toolbar is 2nd added band
	rebar.ShowBand(nBandIndex, bVisible);
	UISetCheck(ID_VIEW_TOOLBAR, bVisible);
	UpdateLayout();
	return 0;
}

LRESULT CMainFrame::OnViewStatusBar(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	BOOL bVisible = !::IsWindowVisible(m_hWndStatusBar);
	::ShowWindow(m_hWndStatusBar, bVisible ? SW_SHOWNOACTIVATE : SW_HIDE);
	UISetCheck(ID_VIEW_STATUS_BAR, bVisible);
	UpdateLayout();
	return 0;
}

LRESULT CMainFrame::OnViewRibbon(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	ShowRibbonUI(!IsRibbonUI());
	UISetCheck(ID_VIEW_RIBBON, IsRibbonUI());
	return 0;
}

LRESULT CMainFrame::OnAppAbout(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	CAboutDlg dlg;
	dlg.DoModal();
	return 0;
}
LRESULT CMainFrame::OnUICommand(UINT /*uMsg*/, WPARAM wParam, LPARAM lParam, BOOL& /*bHandled*/)
{
    int id = LOWORD(wParam);
    int from = HIWORD(wParam);
    bool isforce = false;
    if (from == 1) {
        //from Accelerator	
        isforce = true;
    }
    m_cmdRoot.OnExecute(id, &m_view, isforce);
    return 0;
}

void CMainFrame::InitCreateCommand()
{
    m_cmdRoot.SetUI(GetIUIFrameworkPtr());

    for (auto & factory : of::ObjectFactoryAutoCreator<ICommand>::Instance().cmd_factory_) {
        m_cmdRoot.Register(factory->CreateSharedObject(), &m_view);
    }
}

LRESULT CMainFrame::CenterChanged(WORD /*wID*/, DOUBLE * lVal, BOOL& /*bHandled*/)
{
    return 0;
}

 HRESULT CMainFrame::DoUpdateProperty(UINT nCmdID, REFPROPERTYKEY key, const PROPVARIANT* ppropvarCurrentValue, 
     PROPVARIANT* ppropvarNewValue)
 {
     return __super::DoUpdateProperty(nCmdID, key, ppropvarCurrentValue, ppropvarNewValue);
 }

 LRESULT CMainFrame::OnOutputMessage(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& /*bHandled*/)
 {
     IMap::output_to where = (IMap::output_to)wParam;
     const wchar_t * msg = (const wchar_t *)lParam;
     if (where & IMap::clipboard) 
         misc::Copy(msg, m_hWnd);

     if (where & IMap::statusbar)
         m_wndStatusBar.SetPaneText(ID_DEFAULT_PANE, msg);

     if (where & IMap::output_window) {
         m_OutputCommandView.AppendText(msg);
         m_OutputCommandView.AppendText(L"\r\n");
     }
     return 0;
 }

 struct MarkersLayoutC : public DockingMarkers::LayoutPane
 {
     MarkersLayoutC() : LayoutPane(112, 112,		// total size of marker's group.
         DockingMarkers::MarkerPane(CPoint(4, 40), IDB_BITMAP_LEFT, CRect(0, 0, 32, 32)),		// left marker.
         DockingMarkers::MarkerPane(CPoint(40, 4), IDB_BITMAP_TOP, CRect(0, 0, 32, 32)),			// top marker.
         DockingMarkers::MarkerPane(CPoint(76, 40), IDB_BITMAP_RIGHT, CRect(0, 0, 32, 32)),		// right marker.
         DockingMarkers::MarkerPane(CPoint(40, 76), IDB_BITMAP_BOTTOM, CRect(0, 0, 32, 32)),	// bottom marker.
         DockingMarkers::MarkerPane(CPoint(0, 0), IDB_BITMAP_TABS, CRect(36, 36, 76, 76)),		// marker of tabs.
         RGB(255, 0, 255))	// color of mask (pixels which don't show).
     {
     }
 };

 bool CMainFrame::InitControls()
 {
     m_view.Create(m_hWnd, rcDefault, NULL, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN, WS_EX_CLIENTEDGE, 12345);
     m_view.m_wndMain = m_hWnd;
     if (m_MultiPaneCtrl.CreateX(m_hWnd, WS_CHILD | WS_VISIBLE,
         CRect(0, 0, 0, 0), 100/*id of MultiPaneCtrl*/) == false)
         return  false;

     m_LayerView.Create(m_hWnd, CRect(0, 0, 0, 0), NULL, WS_CHILD | WS_VISIBLE, 0, _U_MENUorID(301).m_hMenu);
     m_selectView.Create(m_hWnd, CRect(0, 0, 0, 0), NULL, WS_CHILD | WS_VISIBLE, 0, _U_MENUorID(310).m_hMenu);
     m_bookmarkView.Create(m_hWnd, CRect(0, 0, 0, 0), NULL, WS_CHILD | WS_VISIBLE, 0, _U_MENUorID(320).m_hMenu);
     
     m_OutputCommandView.Create(m_hWnd, CRect(0, 0, 0, 0), NULL, WS_CHILD | WS_VISIBLE | WS_VSCROLL | ES_MULTILINE | ES_WANTRETURN, 0, 330);
     m_OutputCommandView.SetFont(AtlGetDefaultGuiFont());
     m_OutputCommandView.SetMargins(4, 4);
     m_OutputCommandView.SetLimitText(10 * 1024 * 1024);
     m_elementPropView.Create(m_hWnd,CRect(0,0,0,0), NULL, WS_CHILD | ES_MULTILINE,0, 304);

     CImageList imagelistSys;
     CBitmap bmpSys;
     imagelistSys.Create(14, 14, ILC_COLOR24 | ILC_MASK, 7, 0);
     bmpSys.LoadBitmap(IDB_BITMAP_SYS);
     imagelistSys.Add(bmpSys, RGB(255, 0, 255));
     m_MultiPaneCtrl.SetSystemImageList(&imagelistSys);

     m_MultiPaneCtrl.SetCursors(IDC_CURSOR_TAB, IDC_CURSOR_HORZ, IDC_CURSOR_VERT, IDC_CURSOR_MOVING,
         IDC_CURSOR_DISABLE);

     m_MultiPaneCtrl.SetDockMarkers(MarkersLayoutC(),
         DockingMarkers::Params(40, true, 14), 50);
     m_MultiPaneCtrl.RemoveTabEnable(true);
     m_MultiPaneCtrl.DragTabEnable(true);

     LayoutWindows();

     
     m_hWndClient = m_MultiPaneCtrl;

     m_MultiPaneCtrl.SetAbilityManager(this);
     m_MultiPaneCtrl.SetNotifyManager(this);
     return 0;
 }

 void CMainFrame::LayoutWindows()
 {
     MultiPaneCtrl::Tabs tabs;
     tabs.Add(m_view, _T("主视图"), -1);
     tabs.Add(m_OutputCommandView, _T("命令输出"), -1);
     tabs.Add(m_LayerView, _T("图层"), -1);
     tabs.Add(m_selectView, _T("选择"), -1);
     tabs.Add(m_elementPropView, _T("属性"), -1);
     tabs.Add(m_bookmarkView, _T("书签"), -1);
     
     //NULL => 根pane
     //ConvertToLine(hParent, 水平/垂直 ) 表于对hPane拆成两个pane，而且返回第左边或上边
     //Add(hParent)  表于生成hPane的右边或下边，且返回其pane值
     //简单的说ConvertToLine 拆分并返回第一部分，然后再用Add 生成第二部分


     //左
     HPANE hLeft = m_MultiPaneCtrl.ConvertToLine(NULL, true);

     //左上
     HPANE hLeftTop = m_MultiPaneCtrl.ConvertToLine(hLeft, false);
     m_MultiPaneCtrl.AddTab(hLeftTop, tabs[0]);//主视图

     //左下
     HPANE hLeftBottom = m_MultiPaneCtrl.Add(hLeft);
     m_MultiPaneCtrl.AddTab(hLeftBottom, tabs[1]);//命令输出

     //右
     HPANE hRight = m_MultiPaneCtrl.Add(NULL);
     HPANE hRightTop = m_MultiPaneCtrl.ConvertToLine(hRight, false);

     //右上
     m_MultiPaneCtrl.AddTab(hRightTop, tabs[2]);//图层


     //右下
     HPANE hRightBottom = m_MultiPaneCtrl.Add(hRight);

     //右下上
     HPANE hRightBmTop = m_MultiPaneCtrl.ConvertToLine(hRightBottom, false);
     m_MultiPaneCtrl.AddTab(hRightBmTop, tabs[3]); //选择

     //右下下
     HPANE hRightBmBm = m_MultiPaneCtrl.Add(hRightBottom);
     m_MultiPaneCtrl.AddTab(hRightBmBm, tabs[5]);//书签
     m_MultiPaneCtrl.AddTab(hRightBmBm, tabs[4]);//属性

     m_MultiPaneCtrl.SetEqualPaneSize();

     m_MultiPaneCtrl.Update();

     //必须创建完之后才能设置
     m_MultiPaneCtrl.SetPortion(hLeft, 0.85, SPACE_BETWEEN); //左边 0.9
     m_MultiPaneCtrl.SetPortion(hLeftTop, 0.8, SPACE_BETWEEN); //左（上下关系）
     m_MultiPaneCtrl.SetPortion(hRightTop, 0.2, SPACE_PREVIOUS);
     m_MultiPaneCtrl.SetPortion(hRightBmTop, 0.3, SPACE_PREVIOUS);

     WTL::CFontHandle guifont = AtlGetDefaultGuiFont();
     LOGFONT lf;
     guifont.GetLogFont(&lf);
     lf.lfHeight = -12;
     lstrcpy(lf.lfFaceName, L"微软雅黑");
     m_MultiPaneCtrl.SetFont(&lf);


 }

