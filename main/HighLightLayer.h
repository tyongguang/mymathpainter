#pragma once
#include "layer.h"
#include "RTree.h"

class HighLightStyle : public IDrawStyle
{
public:
    HighLightStyle(bool inverse = false) {

        Gdiplus::Color pen_col = Gdiplus::Color(150, 255, 0, 0);
        float pen_width = 4.0;
        Gdiplus::Color brush_col = Gdiplus::Color(180, 255, 0, 0);
        if (inverse)
            pen_col.SetFromCOLORREF(pen_col.ToCOLORREF() ^ 0xffffff);

        if (inverse)
            brush_col.SetFromCOLORREF(brush_col.ToCOLORREF() ^ 0xffffff);


        pen_.reset(new Gdiplus::Pen(pen_col, pen_width));
        brush_.reset(new Gdiplus::SolidBrush(brush_col));

        raw_pen_.CreatePen(PS_SOLID, int(pen_width), pen_col.ToCOLORREF());
        raw_brush_.CreateSolidBrush(brush_col.ToCOLORREF());
    }
    virtual const Gdiplus::Pen * getPen() {
        return pen_.get();
    }
    virtual const Gdiplus::Brush * getBrush() {
        return brush_.get();
    }
    virtual float getPointRenderWidth() {
        return 6.0;
    }
    virtual CPenHandle getPenHandle() {
        return (HPEN)raw_pen_;
    }
    virtual CBrushHandle getBrushHandle() {
        return (HBRUSH)raw_brush_;
    }
private:
    std::unique_ptr<Gdiplus::Pen> pen_;
    std::unique_ptr<Gdiplus::Brush> brush_;

    WTL::CPen raw_pen_;
    WTL::CBrush raw_brush_;
};

class HighLightLayer : public CacheDrawlayer
{
public:
    HighLightLayer() : inverse_style(true)
    {
    }
    ~HighLightLayer()
    {
    }

    void SelectMany(std::vector<IDrawable *> & elements, bool swap);
    void Select(IDrawable * element);
    void UnSelect();
    void GetSelect(std::vector<IDrawable *> & elements);
    void AddRTree(IDrawable * element);
    void SpecialHeightLight(IDrawable * element, CoordSys * cs, CDCHandle dc);
    virtual bool needRedraw(int reason);

    virtual void draw_elements(Gdiplus::Graphics &g, CoordSys * pConv);
private:
    HighLightStyle default_style;
    HighLightStyle inverse_style;;

    std::vector<IDrawable *>  m_selected_elements;
    RTree<IDrawable *, double, 2> rtree_;
    std::vector<IDrawable *> unBound_element_;

};