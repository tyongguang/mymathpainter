#pragma once
#include "singal.h"
#include "layer.h"

typedef auto_signal<std::function<void(double, double, UINT) > > MouseEventType;
typedef auto_signal<std::function<void(TCHAR, UINT, UINT) > > KeyBoardEventType;
typedef auto_signal<std::function<void(Gdiplus::Graphics *, CoordSys * ) > > ActionDrawEventType;

typedef auto_signal<std::function<void(const std::vector<IDrawable *> *) > > SelectChangedType;


class IMouseCaptureFilter {
public:
    virtual void getName(CString & name) = 0;
    virtual bool Filter(vPoint & vpt) = 0;
};


class IMap
{
public:
    enum  output_to {
        clipboard = 1 << 0,
        statusbar = 1 << 1,
        output_window = 1 <<2 
    };
    MouseEventType mouse_move_event;
    MouseEventType mouse_ldown_event;
    MouseEventType mouse_lup_event;
    MouseEventType mouse_ldbclick_event;
    MouseEventType mouse_rdown_event;
    MouseEventType mouse_rup_event;
    MouseEventType mouse_mdown_event;
    MouseEventType mouse_mdbclick_event;

    KeyBoardEventType mouse_keydown_event;
    KeyBoardEventType mouse_keyup_event;

    ActionDrawEventType action_draw_event;
    SelectChangedType select_changed_event;
    auto_signal<std::function<void(int, int) > > level_changed_event;
    std::map<float, std::shared_ptr<ILayer> > layers;
    std::vector<std::weak_ptr<IMouseCaptureFilter>> mouseCaptures;

    virtual std::shared_ptr<ILayer> getActiveLayer() = 0;
    virtual void setActiveLayer(std::shared_ptr<ILayer> layer) = 0;
    virtual IAxisControl * getAxisControl() = 0;
    virtual void Output(output_to where, const wchar_t * msg) = 0;
    virtual void SelectMany(std::vector<IDrawable *> & elements, bool swap = true) = 0;
    virtual void GetSelect(std::vector<IDrawable *> & elements) = 0;
    virtual void Select(IDrawable * element) = 0;
    virtual void UnSelect() = 0;
    virtual void SpecialHeightLight(IDrawable * ele) = 0;

    virtual void offset(double x_offset, double y_offset) = 0;
    virtual void SetCenter(const vPoint & vpt) = 0;

    virtual int getZoomLevel() = 0;
    virtual void ZoomDelta(int delta_value) = 0;
    virtual void ZoomAbs(int abs_value) = 0;
    virtual void FitRectZoom(const vPoint & pt1, const vPoint & pt2) = 0;
    virtual vPoint getCenter() = 0;
    virtual void refresh(int reason = draw_reson::draw_reson_Other ) = 0;
    virtual HWND getHWND() = 0;
    virtual double ScreenSize2ViewSize(float size) = 0;
    virtual bool getCapturePoint(vPoint & pt, float pixel_range = 8.0F) = 0;
    virtual bool getRealPoint(vPoint & pt, float pixel_range = 8.0F) = 0;
    virtual bool getIntersectPoint(const vPoint & pt, vPoint * output_pt, float pixel_range = 8.0F) = 0;
    virtual bool getSegments(const vPoint & pt, std::vector<Wm5::Segment2d> & segments, float pixel_range = 8.0F) = 0;

    //filter
    virtual void AddMouseCaptureFilter(std::weak_ptr<IMouseCaptureFilter>  filter) = 0;
    virtual void RemoveMouseCaptureFilter(std::weak_ptr<IMouseCaptureFilter>  filter) = 0;

    //cursor
    virtual void AddCursorFront(HCURSOR cursor) = 0;
    virtual void AddCursorBack(HCURSOR cursor) = 0;
    virtual void RemoveCursor(HCURSOR cursor) = 0;
};