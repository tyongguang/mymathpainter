#include "stdafx.h"
#include <memory>
#include "selectionView.h"
#include "misc/misc.h"
#include "layers/SimpleLayer.h"

class PreviewCoordConv : public ICoordConv
{
public:
    PreviewCoordConv(const CRect & rc, IDrawable * target_ele) 
        :  can_render_(true)
    {
        RtreeBound rb;
        if (false == target_ele->getBound(rb)){
            can_render_ = false;
            return;
        }
        cur_width_ = rc.Width();
        cur_height_ = rc.Height();
        vPoint pt_left_top;
        vPoint pt_right_bottom;
        pt_left_top.X() = rb.boundMin[0];
        pt_left_top.Y() = rb.boundMin[1];

        pt_right_bottom.X() = rb.boundMax[0];
        pt_right_bottom.Y() = rb.boundMax[1];


        vPoint mid((pt_left_top.X() + pt_right_bottom.X()) / 2,
            (pt_left_top.Y() + pt_right_bottom.Y()) / 2);

        double vHeight = pt_right_bottom.Y() - pt_left_top.Y();
        double vWidth = pt_right_bottom.X() - pt_left_top.X();

        double expect_k1 = cur_height_ / vHeight;
        double expect_k2 = cur_width_ / vWidth;

        k_ = MIN(expect_k1, expect_k2);

        auto & center_pt = rc.CenterPoint();
        Gdiplus::PointF screen_center(float(center_pt.x), float(center_pt.y));
        ptOffset.X() = screen_center.X - k() * mid.X();
        ptOffset.Y() = screen_center.Y + k() * mid.Y();


    }
    virtual vPoint Screen2View(const Gdiplus::PointF & pt)
    {
        /*  View坐标：（x0, y0), Screen坐标: (x1, y1)
        x0 = (x1 - a )/ k
        y0 = -(y1 - b )/ k
        */
        return vPoint(
            (pt.X - a()) / k(),
            -(pt.Y - b()) / k()
            );
    }

    virtual Gdiplus::PointF View2Screen(const vPoint & vpt)
    {
        /*  View坐标：（x0, y0), Screen坐标: (x1, y1)
        x1 = k * x0 + a
        y1 = -k * y0 + b
        这里y 使用-k， 是因为windows的y是向下为正方向，一般是坐标是向上为正方向
        */
        return Gdiplus::PointF(
            float(k() * vpt.X() + a()),
            float(-k() * vpt.Y() + b())
            );
    }

    virtual double ScreenSize2ViewSize(double sSize) {
        return double(sSize) / k();
    }

    virtual double ViewSize2ScreenSize(double vSize) {
        return double(vSize) * k();
    }

    virtual double vleft() {
        return (0 - a()) / k();
    }

    virtual double vtop() {
        return -(0 - b()) / k();
    }

    virtual double vright() {
        return (cur_width_ - a()) / k();
    }

    virtual double vbottom() {
        return -(cur_height_ - b()) / k();;
    }

    virtual vPoint center() {
        return CenterPoint();
    }
    virtual vPoint extent() {
        return vPoint(
            fabs(cur_width_ * k() / 2 - a()),
            fabs(cur_height_ * k() / 2 - b())
            );
    }

    virtual float swidth(){
        return float(cur_width_);
    }

    virtual float sheight(){
        return float(cur_height_);
    }
    bool CanRender() {
        return can_render_;
    }

    vPoint CenterPoint() {
        Gdiplus::PointF screen_center(float(cur_width_) / 2, float(cur_height_) / 2);
        return Screen2View(screen_center);
    }
private:

    bool can_render_;
     inline double k() {
         return k_;
     }
     inline double a() {
         return  ptOffset.X();
     }
     inline double b() {
         return  ptOffset.Y();
     }
     vPoint ptOffset;
     double k_;
     int cur_height_;
     int cur_width_;

};

selectionView::selectionView()
    :map_(nullptr), m_draw_preivew(true)
{

}

selectionView::~selectionView()
{

}

#define SELECTION_INDEX_PREVIEW 0
//#define SELECTION_INDEX_LOCK 1
#define SELECTION_INDEX_TYPE 1


LRESULT selectionView::OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
    m_lock.LoadIcon(IDI_LOCK);
    m_unlock.LoadIcon(IDI_UNLOCK);


    m_hWndClient = m_view.Create(m_hWnd, CRect(0, 0, 0, 0), NULL, LVS_OWNERDRAWFIXED | LVS_REPORT | LVS_SHAREIMAGELISTS | WS_CHILD | WS_HSCROLL | WS_VISIBLE | LVS_SINGLESEL, 0);
    m_view.InsertColumn(SELECTION_INDEX_PREVIEW, L" ", LVCFMT_LEFT, 32);
 //   m_view.InsertColumn(SELECTION_INDEX_LOCK, L" ", LVCFMT_LEFT, 32);
    m_view.InsertColumn(SELECTION_INDEX_TYPE, L" ", LVCFMT_LEFT, cs_text_width);

    m_view.SetExtendedListViewStyle(LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER);


    m_view.ModifyStyle(0, LVS_NOCOLUMNHEADER, TRUE);
    return 0;
}

LRESULT selectionView::OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
    return 0;
}

void selectionView::DrawItem(LPDRAWITEMSTRUCT di)
{
     CDCHandle dc(di->hDC);
     COLORREF crOldTextColor = dc.GetTextColor();
     COLORREF crOldBkColor = dc.GetBkColor();
 
     CRect cell_rcItem;
 
     IDrawable * ele = (IDrawable *)m_view.GetItemData(di->itemID);
     int column_count = m_view.GetHeader().GetItemCount();
     
 
     for (int i = 0; i < column_count; i++)
     {
         m_view.GetSubItemRect(di->itemID, i, LVIR_LABEL, cell_rcItem);
         int rcHeight = cell_rcItem.Height();
         int iconTop = 0;
         if (i == SELECTION_INDEX_PREVIEW) {
             if (true == m_draw_preivew) {
                 cell_rcItem.DeflateRect(-1, -1, 2, 2);
                 PreviewCoordConv conv(cell_rcItem, ele);
                 if (conv.CanRender() == false)
                     continue;
                 CoordSys cs(&conv);
                 gp::Graphics g(dc);
                 g.SetSmoothingMode(gp::SmoothingMode::SmoothingModeAntiAlias);
                 ele->draw(g, &cs, &style_);
             }
             else {
                 CString strText;
                 strText.Format(L"%d.", di->itemID);
                 dc.DrawText(strText, -1, &cell_rcItem, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
             }
         }
 
         if (i == SELECTION_INDEX_TYPE) {
             if ((di->itemAction | ODA_SELECT) &&
                 (di->itemState & ODS_SELECTED))
             {
                 dc.SetTextColor(::GetSysColor(COLOR_HIGHLIGHTTEXT));
                 dc.SetBkColor(::GetSysColor(COLOR_HIGHLIGHT));
                 dc.FillSolidRect(&cell_rcItem,
                     ::GetSysColor(COLOR_HIGHLIGHT));
             }
             else
             {
                 COLORREF bg_color;

                 if (di->itemID % 2)
                     bg_color = RGB(244, 244, 240);
                 else
                     bg_color = RGB(250, 250, 245);
          

                 if ((di->itemAction | ODA_FOCUS) &&
                     (di->itemState & ODS_FOCUS))
                 {
                     bg_color = misc::PixelAlpha(bg_color, RGB(90, 158, 241), 80);
                 }
                 dc.FillSolidRect(&cell_rcItem, bg_color);
      
             }
 
         
      
             CString strText;
             if (ele->getType() == GeomType::Expression) {
                 ele->dump_info(strText);
             }
             else{
                 OLECHAR* bstrGuid;
                 StringFromCLSID(ele->getID(), &bstrGuid);
                 strText = bstrGuid;
                 ::CoTaskMemFree(bstrGuid);
             }
             cell_rcItem.left += 4;
             dc.DrawText(strText, -1, &cell_rcItem, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
         }
     }
     CPen penRowSplit;
     penRowSplit.CreatePen(PS_SOLID, 1, RGB(210, 210, 210));
     CPenHandle oldPen = dc.SelectPen(penRowSplit);
     dc.MoveTo(di->rcItem.left, di->rcItem.bottom - 1);
     dc.LineTo(di->rcItem.right, di->rcItem.bottom - 1);
     dc.SelectPen(oldPen);
 
     dc.SetTextColor(crOldTextColor);
     dc.SetBkColor(crOldBkColor);
}

void selectionView::MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct)
{

    if (m_draw_preivew == false) {
        CClientDC dc(m_hWnd);
        TEXTMETRIC tm = { 0 };
        dc.GetTextMetrics(&tm);
        lpMeasureItemStruct->itemHeight = tm.tmHeight;
    }
    else {
        lpMeasureItemStruct->itemHeight = 32;
    }
}

LRESULT selectionView::OnNotify(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
    LPNMHDR pNMHdr = (LPNMHDR)lParam;
    if (pNMHdr->hwndFrom == m_view)
    {
        LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHdr);
        if (pNMHdr->code == NM_CLICK) {
            ListCtrlClick(pNMItemActivate);
        }
        if (pNMHdr->code == NM_DBLCLK) {
            ListCtrlDbClick(pNMItemActivate);
        }
    }
    else {
        bHandled = FALSE;
    }
    return 0;
}

void selectionView::ListCtrlClick(LPNMITEMACTIVATE item)
{

}

void selectionView::ListCtrlDbClick(LPNMITEMACTIVATE item)
{
    IDrawable * ele = (IDrawable *)m_view.GetItemData(item->iItem);
    map_->SpecialHeightLight(ele);
}
void selectionView::SelectChanged(const std::vector<IDrawable *> * selected_element)
{
    std::vector<IDrawable *> elements;
    m_view.DeleteAllItems();
    map_->GetSelect(elements);
    int i = m_view.GetItemCount();
    for (auto & ele : elements) {
        m_view.AddItem(i, 0, L"");
        m_view.SetItemData(i, (DWORD_PTR)ele);
        i++;
    }


    if (elements.size() > 50) {
        m_draw_preivew = false;
    }
    else{
        m_draw_preivew = true;
    }
    gm::CClientRect rc(m_hWnd);

    //强迫触发MeasureItem
    this->SetWindowPos(NULL, 0, 0, rc.Width() - 1, rc.Height() - 1, SWP_NOMOVE);
    this->SetWindowPos(NULL, 0, 0, rc.Width(), rc.Height(), SWP_NOMOVE);
    m_view.Invalidate();
}

void selectionView::InitSelectionView(IMap * map)
{
    map_ = map;
    select_changed_slot_ = map_->select_changed_event.connect(std::bind(&selectionView::SelectChanged,
        this, holder::_1));

}

LRESULT selectionView::OnSize(UINT /*uMsg*/, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
    bHandled = false;
    int cur_width = LOWORD(lParam);
    auto header = m_view.GetHeader();
    int column_count = header.GetItemCount() - 1;
    for (int i = 0; i < column_count; ++i) {
        cur_width -= m_view.GetColumnWidth(i);
    }
    if (m_view.GetStyle() & WS_VSCROLL) {
        cur_width -= GetSystemMetrics(SM_CXVSCROLL);
    }
    if (cur_width > cs_text_width)
        m_view.SetColumnWidth(column_count, cur_width);
    return 0;
}
