#pragma once
#include "resource.h"
#include "guimisc.h"
#include "map.h"

class PreviewStyle : public IDrawStyle
{
public:
    PreviewStyle() {

        Gdiplus::Color pen_col = Gdiplus::Color(150, 255, 0, 0);
        float pen_width = 2.0;
        Gdiplus::Color brush_col = Gdiplus::Color(180, 255, 0, 0);


        pen_.reset(new Gdiplus::Pen(pen_col, pen_width));
        brush_.reset(new Gdiplus::SolidBrush(brush_col));

        raw_pen_.CreatePen(PS_SOLID, int(pen_width), pen_col.ToCOLORREF());
        raw_brush_.CreateSolidBrush(brush_col.ToCOLORREF());
    }
    virtual const Gdiplus::Pen * getPen() {
        return pen_.get();
    }
    virtual const Gdiplus::Brush * getBrush() {
        return brush_.get();
    }
    virtual float getPointRenderWidth() {
        return 6.0;
    }
    virtual CPenHandle getPenHandle() {
        return (HPEN)raw_pen_;
    }
    virtual CBrushHandle getBrushHandle() {
        return (HBRUSH)raw_brush_;
    }
private:
    std::unique_ptr<Gdiplus::Pen> pen_;
    std::unique_ptr<Gdiplus::Brush> brush_;

    WTL::CPen raw_pen_;
    WTL::CBrush raw_brush_;
};

class selectionView
    : public CFrameWindowImpl < selectionView >
    , public COwnerDraw<selectionView>
{
public:
    selectionView();
    ~selectionView();
    DECLARE_WND_CLASS(_T("selectionView"))

    BEGIN_MSG_MAP(selectionView)
        MESSAGE_HANDLER(WM_CREATE, OnCreate)
        MESSAGE_HANDLER(WM_DESTROY, OnDestroy)
        MESSAGE_HANDLER(WM_SIZE, OnSize)
        MESSAGE_HANDLER(WM_NOTIFY, OnNotify)
        CHAIN_MSG_MAP(COwnerDraw<selectionView>)
        CHAIN_MSG_MAP(CFrameWindowImpl<selectionView>)
        REFLECT_NOTIFICATIONS()
    END_MSG_MAP()

    void InitSelectionView(IMap * map);
    LRESULT OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
    LRESULT OnNotify(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
    LRESULT OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
    LRESULT OnSize(UINT /*uMsg*/, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
//     LRESULT OnNewLayer(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
//     LRESULT OnDelLayer(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
    void DrawItem(LPDRAWITEMSTRUCT /*lpDrawItemStruct*/);

    void MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct);

    void ListCtrlClick(LPNMITEMACTIVATE item);
    void ListCtrlDbClick(LPNMITEMACTIVATE item);
private:
    void SelectChanged(const std::vector<IDrawable *> * selected_element);

    CListViewCtrl m_view;

    CIcon m_lock;
    CIcon m_unlock;

    bool m_draw_preivew;
    const static int cs_text_width = 200;

    IMap * map_;
    PreviewStyle style_;

    SelectChangedType::slot select_changed_slot_;
};