#pragma once
class OutputEditWnd 
    : public CWindowImpl<OutputEditWnd, CEdit>
    , public CEditCommands<OutputEditWnd>
{
public:
    DECLARE_WND_CLASS(NULL)
    BEGIN_MSG_MAP(OutputEditWnd)
        MESSAGE_HANDLER(WM_CONTEXTMENU, OnContextMenu)
        CHAIN_MSG_MAP_ALT(CEditCommands<OutputEditWnd>, 1)
    END_MSG_MAP()

    LRESULT OnContextMenu(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

};

#define ADD_EDIT_CMD(theChainMember) \
if (uMsg==WM_COMMAND&&(HWND)theChainMember==GetFocus())\
{\
	SendMessage((HWND)theChainMember,uMsg,wParam,lParam);\
}