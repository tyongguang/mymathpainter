#include "stdafx.h"
#include <memory>
#include "bookmarkView.h"
#include "misc/misc.h"
#include "layers/SimpleLayer.h"
#include "Dialogs/SetNameDlg.h"
#include <atltime.h>

struct bookmark_data
{
    vPoint ptCenter;
    int level;
};

bookmarkView::bookmarkView()
    :map_(nullptr)
{

}

bookmarkView::~bookmarkView()
{

}



LRESULT bookmarkView::OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
    m_acitve.LoadIcon(IDI_LAYER_ACTIVE);
    m_hide.LoadIcon(IDI_LAYER_HIDE);
    m_select.LoadIcon(IDI_LAYER_SELECT);
    m_unselect.LoadIcon(IDI_LAYER_UNSELECT);
    m_show.LoadIcon(IDI_LAYER_SHOW);

    HWND hWndToolBar = m_tb.CreateSimpleTool(m_hWnd, ATL_IDW_TOOLBAR, 16, 16, 3);
    m_tb.AddBtn(this, IDS_BOOKMARK_ADD, IDB_BOOKMARK_ADD, NULL, TBSTYLE_AUTOSIZE | BTNS_SHOWTEXT);
    m_tb.AddBtn(this, IDS_BOOKMARK_REMOVE, IDB_BOOKMARK_REMOVE, NULL, TBSTYLE_AUTOSIZE | BTNS_SHOWTEXT);
    m_tb.AddBtn(this, IDS_BOOKMARK_RENAME, IDB_RENAME, NULL, TBSTYLE_AUTOSIZE | BTNS_SHOWTEXT);
    

     CreateSimpleReBar(ATL_SIMPLE_REBAR_NOBORDER_STYLE);
     AddSimpleReBarBand(hWndToolBar, NULL, TRUE);


     m_hWndClient = m_view.Create(m_hWnd, CRect(0, 0, 0, 0), NULL,  LVS_REPORT | LVS_SHAREIMAGELISTS | WS_CHILD | WS_HSCROLL | WS_VISIBLE | LVS_SINGLESEL, 0);
     m_view.InsertColumn(0, L"坐标", LVCFMT_LEFT, 160);
     m_view.InsertColumn(1, L"名称", LVCFMT_LEFT, 100);


     m_view.SetExtendedListViewStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES |LVS_EX_UNDERLINEHOT);



    return 0;
}

LRESULT bookmarkView::OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
    return 0;
}

BOOL bookmarkView::LoadBitmap(HBITMAP * pBitmap, UINT nResourceID)
{
    Gdiplus::Bitmap *  bitMap;
    if (FALSE == misc::LoadPNGFromRes(_Module.m_hInstResource, nResourceID, &bitMap))
        return FALSE;
    bitMap->GetHBITMAP(Gdiplus::Color(255, 255, 255, 255), pBitmap);
    delete bitMap;
    return TRUE;
}



LRESULT bookmarkView::OnNotify(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
    LPNMHDR pNMHdr = (LPNMHDR)lParam;
    if (pNMHdr->hwndFrom == m_view)
    {
        LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHdr);
        if (pNMHdr->code == NM_CLICK) {
            ListCtrlClick(pNMItemActivate);
        }
        if (pNMHdr->code == NM_DBLCLK) {
            ListCtrlDbClick(pNMItemActivate);
        }
    }
    else {
        bHandled = FALSE;
    }
    return 0;
}

void bookmarkView::ListCtrlClick(LPNMITEMACTIVATE item)
{
  
   
}

void bookmarkView::ListCtrlDbClick(LPNMITEMACTIVATE item)
{
    bookmark_data * pbd = (bookmark_data *)m_view.GetItemData(item->iItem);
    map_->SetCenter(pbd->ptCenter);
    map_->ZoomAbs(pbd->level);
}

void bookmarkView::InitBookMarkView(IMap * map)
{
    map_ = map;
}



LRESULT bookmarkView::OnSize(UINT /*uMsg*/, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
    bHandled = false;
    int cur_width = LOWORD(lParam);
    auto header = m_view.GetHeader();
    int column_count = header.GetItemCount() - 1;
    for (int i = 0; i < column_count; ++i) {
        cur_width -= m_view.GetColumnWidth(i);
    }
    if (m_view.GetStyle() & WS_VSCROLL) {
        cur_width -= GetSystemMetrics(SM_CXVSCROLL);
    }
    if (cur_width > cs_text_width)
        m_view.SetColumnWidth(column_count, cur_width);
    return 0;
}

LRESULT bookmarkView::OnRename(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
{
    int first_index = -1;
    int nSelectedCount = 0;
    int count = m_view.GetItemCount();
    for (int i = count - 1; i >= 0; --i) {
        if (m_view.GetItemState(i, LVIS_SELECTED) == LVIS_SELECTED) {
            if (first_index == -1)
                first_index = i;
            nSelectedCount++;
        }
    }
    if (nSelectedCount != 1) {
        MessageBox(L"要选择1个书签重命名");
        return 0;
    }


    SetNameDlg dlg;
    m_view.GetItemText(first_index, 1, dlg.m_strName);
    if (IDOK != dlg.DoModal()) {
        return 0;
    }

    m_view.SetItemText(first_index, 1, dlg.m_strName);

    return 0;
}

LRESULT bookmarkView::OnNewBookmark(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
{
    bookmark_data * bd = new bookmark_data();
    bd->ptCenter = map_->getCenter();
    bd->level = map_->getZoomLevel();

    SetNameDlg dlg;
    CString strName;
    strName = dlg.m_strName = CTime::GetCurrentTime().Format(L"%Y%m%d%H%M%S");
    if (IDOK != dlg.DoModal()) {
        return 0;
    }

    strName = dlg.m_strName;
    int index = m_view.GetItemCount();
    m_view.InsertItem(index, misc::format_point(bd->ptCenter.X(), bd->ptCenter.Y()));
    m_view.SetItemText(index, 1, strName);
    m_view.SetItemData(index, (DWORD_PTR)bd);
    return 0;
}

LRESULT bookmarkView::OnDelBookmark(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
{
    int count = m_view.GetItemCount();
    for (int i = count - 1; i >= 0; --i) {
        if (m_view.GetItemState(i, LVIS_SELECTED) == LVIS_SELECTED) {
            bookmark_data * pbd = (bookmark_data *)m_view.GetItemData(i);
            delete pbd;
            m_view.DeleteItem(i);
        }
    }

    return 0;
}
