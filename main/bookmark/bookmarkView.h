#pragma once
#include "YGToolBar.h"
#include "resource.h"
#include "guimisc.h"
#include "map.h"
class bookmarkView
    : public CFrameWindowImpl < bookmarkView >
    , public CYGToolBar::IResourceLoader
{
public:
    bookmarkView();
    ~bookmarkView();
    DECLARE_WND_CLASS(_T("bookmarkView"))

    BEGIN_MSG_MAP(bookmarkView)
        MESSAGE_HANDLER(WM_CREATE, OnCreate)
        MESSAGE_HANDLER(WM_DESTROY, OnDestroy)

        COMMAND_ID_HANDLER(IDS_BOOKMARK_ADD, OnNewBookmark)
        COMMAND_ID_HANDLER(IDS_BOOKMARK_REMOVE, OnDelBookmark)
        COMMAND_ID_HANDLER(IDS_BOOKMARK_RENAME, OnRename)
        
        MESSAGE_HANDLER(WM_NOTIFY, OnNotify)
        MESSAGE_HANDLER(WM_SIZE, OnSize)
        CHAIN_MSG_MAP(CFrameWindowImpl<bookmarkView>)
        REFLECT_NOTIFICATIONS()
    END_MSG_MAP()

    void InitBookMarkView(IMap * map);
    LRESULT OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
    LRESULT OnNotify(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
    LRESULT OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
    LRESULT OnSize(UINT /*uMsg*/, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

    virtual BOOL LoadBitmap(HBITMAP * pBitmap, UINT nResourceID);
    LRESULT OnNewBookmark(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
    LRESULT OnDelBookmark(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
    LRESULT OnRename(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
  

    void ListCtrlClick(LPNMITEMACTIVATE item);
    void ListCtrlDbClick(LPNMITEMACTIVATE item);
private:
    std::shared_ptr<ILayer> newLayer(LPCTSTR szLayerName = nullptr);
    CYGToolBar m_tb;
    CListViewCtrl m_view;

    const static int cs_text_width = 120;

    CIcon m_acitve;
    CIcon m_hide;
    CIcon m_select;
    CIcon m_unselect;
    CIcon m_show;

    IMap * map_;
};