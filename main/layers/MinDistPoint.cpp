#include "stdafx.h"
#include "MinDistPoint.h"
#include "geom.h"

MinDistPoint::MinDistPoint(const vPoint &pt, double range, ILayer * layer)
{
    point_exist_ = false;
    gu::Bound2d bound_single(pt, range);
    std::vector<IDrawable *> selected;
    layer->Query(bound_single, selected);
    if (selected.size() == 0) {
        point_exist_ = false;
        return;
    }

    *this = MinDistPoint(pt, range, selected);
}

MinDistPoint::MinDistPoint(const vPoint &pt, double range, std::map<float, std::shared_ptr<ILayer> > * layers)
{
    point_exist_ = false;
    gu::Bound2d bound_single(pt, range);
    std::vector<IDrawable *> selected;
    for (auto & layer : *layers) {
        layer.second->Query(bound_single, selected);
    }
    if (selected.size() == 0) {
        point_exist_ = false;
        return;
    }

    *this = MinDistPoint(pt, range, selected);
}

MinDistPoint::MinDistPoint(const vPoint &pt, double range, IDrawable * element)
{
    vPoint sel_pt = element->nearestPoint(pt, range);

    if (sel_pt.X() == DBL_MAX || sel_pt.Y() == DBL_MAX) {
        point_exist_ = false;
        return;
    }
    if ((sel_pt - pt).SquaredLength() > range * range) {
        point_exist_ = false;
        return;
    }
    point_exist_ = true;
    min_dist_point_ = pt;
}

MinDistPoint::MinDistPoint(const vPoint &pt, double range, std::vector<IDrawable *> & vec)
{
    point_exist_ = false;
    double min_dist = range * range;
    vPoint target_pt;
    for (auto & sel_element : vec) {
        auto cur_pt = sel_element->nearestPoint(pt, range);
        if (cur_pt.X() == DBL_MAX)
            continue;

        double cur_dist = (cur_pt - pt).SquaredLength();
        if (cur_dist < min_dist) {
            min_dist = cur_dist;
            target_pt = cur_pt;
            point_exist_ = true;
        }
    }
    if (point_exist_ == true) {
        min_dist_point_ = target_pt;
    }
}

MinDistPoint::MinDistPoint(const vPoint &pt, double range, std::vector<vPoint> & vec)
{
    point_exist_ = false;
    auto & iter = std::min_element(vec.begin(), vec.end(), [=](vPoint & pt1, vPoint pt2) {
        return (pt1 - pt).SquaredLength() < (pt2 - pt).SquaredLength();
    });
    if ((*iter - pt).SquaredLength() > range * range) {
        point_exist_ = false;
        return;
    }
    point_exist_ = true;
    min_dist_point_ = *iter;
}

MinDistPoint::MinDistPoint(const vPoint &pt, const vPoint &pt1, const vPoint &pt2)
{
    base_cmp(pt, pt1, pt2);
}

MinDistPoint::MinDistPoint(const vPoint &pt, MinDistPoint &mdp1, MinDistPoint &mdp2)
{
    base_cmp(pt, mdp1.getPoint(), mdp2.getPoint());
}

void MinDistPoint::base_cmp(const vPoint &pt, const vPoint &pt1, const vPoint &pt2)
{
    if (pt1.X() == DBL_MAX && pt2.X() == DBL_MAX) {
        point_exist_ = false;
        return;
    }
    point_exist_ = true;

    if (pt1.X() == DBL_MAX) {
        min_dist_point_ = pt2;
        return;
    }

    if (pt2.X() == DBL_MAX) {
        min_dist_point_ = pt1;
        return;
    }

    if ((pt - pt1).SquaredLength() > (pt - pt2).SquaredLength()) {
        min_dist_point_ = pt2;
    }
    else {
        min_dist_point_ = pt1;
    }
}
