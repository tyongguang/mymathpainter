#pragma once
#include "layer.h"
#include "rtree.h"
#include "geom.h"
class SimpleLine : public DrawableBase
{
public:
    SimpleLine();
    ~SimpleLine();

    virtual vPoint nearestPoint(const vPoint & pt, double range) {
        return vPoint(0, 0);
    }
    virtual vPoint nearestIntrPoint(vPoint & pt) {
        return vPoint(0, 0);
    }
    virtual GeomType getType() {
        return GeomType::Composite;
    }
    virtual bool getBound(RtreeBound & bound);
    virtual void draw(Gdiplus::Graphics &g, CoordSys * pConv, IDrawStyle * style);
    virtual void high_light(Gdiplus::Graphics &g, CoordSys * pConv, IDrawStyle * style);
    virtual bool hit_test(const RtreeBound & bound);
private:
    std::vector<vPoint> line_;
    std::unique_ptr<gu::Bound2d> bound_;
};

