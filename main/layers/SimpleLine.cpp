#include "stdafx.h"
#include "SimpleLine.h"
#include "Wm5ContBox2.h"



SimpleLine::SimpleLine()
{
    for (double i = -M_PI * 20; i <= M_PI * 20; i += 0.1) {
        double y = sin(i);
        line_.emplace_back(i, y);

    }
    auto box = Wm5::ContAlignedBox(line_.size(), &line_.front());
    bound_.reset(new gu::Bound2d(box));
}


SimpleLine::~SimpleLine()
{
}

void SimpleLine::draw(Gdiplus::Graphics &g, CoordSys * pConv,IDrawStyle * style)
{
    std::vector<gp::PointF> render_line;
    pConv->conv(line_.begin(), line_.end(), std::back_inserter(render_line));
    g.DrawLines(style->getPen(), &render_line.front(), render_line.size());


    vPoint pt1(1000, 1000);
    vPoint pt2(1100, 1200);
    auto scrPt1 = pConv->conv(pt1);
    auto scrPt2 = pConv->conv(pt2);
    g.DrawLine(style->getPen(), scrPt1, scrPt2);
}

bool SimpleLine::hit_test(const RtreeBound & bound)
{
    return false;
}

bool SimpleLine::getBound(RtreeBound & bound)
{
    bound = *bound_.get();
    return true;
}

void SimpleLine::high_light(Gdiplus::Graphics &g, CoordSys * pConv, IDrawStyle * style)
{

}
