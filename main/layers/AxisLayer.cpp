#include "stdafx.h"
#include "AxisLayer.h"
#include <vector>
#include <memory>
#include "Layer_zindex_def.h"
#include <Wm5Vector2.h>
#include "misc.h"
#include <Wm5IntrRay2Box2.h>
#include "stl_util.h"



class AxisScale {
public:
    AxisScale(double vSize, double vMinValue, double vMaxValue, Gdiplus::Graphics &g, Gdiplus::Font &font, CoordSys * pConv) {
        int value = 1;    //基值
        double multi = 1;    //放大倍数
        is_valid_ = true;
        precision_ = 0;
        multi = 1;

        if (vSize > 1) {
            int iValue = int(vSize);
            int i = iValue;
            for (; i >= 10; i /= 10) {
                multi *= 10;
                precision_++;
            }
            value = i;
        }
        else {
            double i = vSize;
            for (; i < 1; i *= 10) {
                multi /= 10;
                precision_++;
            }
            value = (int)i;
        }

        if (value >= 3 && value < 6)
            value = 5;
        else if (value >= 6) {
            value = 10;
            precision_--;
        }

        scale_ = value * multi;

        double max_value = int(vMaxValue / scale_) * scale_;
        double min_value = int(vMinValue / scale_) * scale_;
        if (max_value > INT_MAX || min_value < INT_MIN) {
            is_valid_ = false;
            return;
        }

        while (1) {
            float text_width1 = getSize(getString(max_value).c_str(), g, font).Width;
            float text_width2 = getSize(getString(min_value).c_str(), g, font).Width;
            double scale_width = pConv->base_conv->ViewSize2ScreenSize(scale_);
            if (scale_width > MAX(text_width1, text_width2))
                break;
            if (value == 1)
                value = 2;
            else if (value == 2)
                value = 5;
            else if (value == 5)
                value = 10;
            else if (value == 10) {
                value = 2;
                multi *= 10;
            }
            scale_ = value * multi;
            max_value = int(vMaxValue / scale_) * scale_;
            min_value = int(vMinValue / scale_) * scale_;

            if (max_value > INT_MAX || min_value < INT_MIN) {
                is_valid_ = false;
                return;
            }
        }
    }

    std::wstring  getString(double cur_value) {
        if (scale_ >= 1) {
            return ut::CFormatW(L"%d", (int)cur_value).c_str();
        }
        CString fromat;
        fromat.Format(L"%%.%df", precision_);
        return ut::CFormatW(fromat, cur_value).c_str();
    }


    gp::SizeF getSize(const wchar_t * str, gp::Graphics & g, gp::Font & font) {
        gp::RectF layoutRect(0, 0, 100, 50);
        gp::RectF outputRect;
        g.MeasureString(str, -1, &font, layoutRect, &outputRect);
        return gp::SizeF(outputRect.Width, outputRect.Height);
    }

    double getScale() {
        return scale_;
    }

    bool valid() {
        return is_valid_;
    }
private:
    bool is_valid_;
    double scale_;
    int precision_; //位数，用于显示浮点数精度
};

AxisLayer::AxisLayer() 
    : font_(&gp::FontFamily(L"Arial"), 12)
    , brush_(gp::Rect(0, 0, 120, 120), gp::Color::Red, gp::Color::Yellow, gp::LinearGradientModeHorizontal)
    , pen_(gp::Color(0, 0, 0), 1.0)
    , grid_pen_(gp::Color::Color(50, 0x80, 0x80, 0x80), 1.0f)
    , scale_brush_(gp::Color::Blue)
    , grid_scale_brush_(gp::Color::Color(180, 0x80, 0x80, 0x80))
{
//    grid_pen_.SetDashStyle(gp::DashStyle::DashStyleDash); //这种模式下性能较差
    rw_showGrid_ = true;
    rw_showGridText_ = true;
    rw_showMeasureScale_ = true;
    rw_visiable_ = true;

}

void AxisLayer::draw(Gdiplus::Graphics &g, CoordSys * pConv, int reason)
{
    if (false == rw_visiable_)
        return;

    //画XY轴
    double vRight = pConv->base_conv->vright();
    double vLeft = pConv->base_conv->vleft();
    double vTop = pConv->base_conv->vtop();
    double vBottom = pConv->base_conv->vbottom();

    vPoint ptX1(vLeft, 0);
    vPoint ptX2(vRight, 0);

    vPoint ptY1(0, vTop);
    vPoint ptY2(0, vBottom);

    double vSizeBase_scale = pConv->base_conv->ScreenSize2ViewSize(40);
    std::unique_ptr<AxisScale> as;


  
    auto Axis_X_begin = pConv->conv(ptX1);
    auto Axis_Y_begin = pConv->conv(ptY1);
    bool Axis_X_visible = Axis_X_begin.Y < pConv->base_conv->sheight() && Axis_X_begin.Y > 0;
    bool Axis_Y_visible = Axis_Y_begin.X < pConv->base_conv->swidth() && Axis_Y_begin.X > 0;

    if (Axis_X_visible || rw_showGrid_) {
        if (as.get() == nullptr) {
            as.reset(new AxisScale(vSizeBase_scale, vLeft, vRight, g, font_, pConv));
        }

        if (as->valid() == true) {
            //画刻度
            double scale = as->getScale();

            int left_count = int(vLeft / scale);
            for (double i = left_count * scale; i < vRight; i += scale) {
                if (fabs(i) < 0.000000001) {
                    continue;
                }
                DrawXScale(as.get(), g, pConv, i,  Axis_X_visible);
            }
        }
    }


    if (Axis_Y_visible || rw_showGrid_) {
        if (as.get() == nullptr) {
            as.reset(new AxisScale(vSizeBase_scale, vLeft, vRight, g, font_, pConv));
        }

        if (as->valid() == true) {
            double scale = as->getScale();
             int bottom_count = int(vBottom / scale);
             for (double i = bottom_count * scale; i < vTop; i += scale) {
                 if (fabs(i) < 0.000000001) {
                     continue;
                 }
                 DrawYScale(as.get(), g, pConv, i,  Axis_Y_visible);
             }
        }
    }
    this->r_scale_valid_ = false;
    if (as.get() != nullptr && as->valid()) {
        this->r_scale_ = as->getScale();
        this->r_scale_valid_ = true;
    }

    //画X轴
    if (Axis_X_visible) {
        g.DrawLine(&pen_, Axis_X_begin, pConv->conv(ptX2));
    }

    //画Y轴
    if (Axis_Y_visible) {
        g.DrawLine(&pen_, Axis_Y_begin, pConv->conv(ptY2));
    }

    //画比例尺
    if (rw_showMeasureScale_)
        DrawMeasureScale(g, pConv);

}

void AxisLayer::DrawXScale(AxisScale * as, gp::Graphics & g, CoordSys * pConv, double i, bool showAxis)
{
    vPoint v(i, 0);
    std::wstring s(as->getString(i));
    auto str_size = as->getSize(s.c_str(), g, font_);
    auto spt_ori = pConv->conv(v);

    if (showAxis) {
        auto spt(spt_ori);
        auto spt2 = spt;
        spt2.Y -= 4;
        g.DrawLine(&pen_, spt, spt2);

        spt.X -= float(str_size.Width / 2);
        g.DrawString(s.c_str(), -1, &font_, spt, &scale_brush_);
    }
    if (rw_showGrid_) {
        auto spt(spt_ori);
        gp::PointF spt1(spt.X, 0);
        gp::PointF spt2(spt.X, pConv->base_conv->sheight());

        g.DrawLine(&grid_pen_, spt1, spt2);
        if (showAxis == false && rw_showGridText_ == true) {
            spt1.X -= float(str_size.Width / 2);
            g.DrawString(s.c_str(), -1, &font_, spt1, &grid_scale_brush_);

        }
    }
}

void AxisLayer::DrawYScale(AxisScale * as, gp::Graphics & g, CoordSys * pConv, double i, bool showAxis)
{
    vPoint v(0, i);
    std::wstring s(as->getString(i));
    auto str_size = as->getSize(s.c_str(), g, font_);
    auto spt_ori = pConv->conv(v);
    if (showAxis) {
        auto spt(spt_ori);
        auto spt2 = spt;
        spt2.X += 4;
        g.DrawLine(&pen_, spt, spt2);

        spt2.X += 2;
        spt2.Y -= str_size.Height / 2;
        g.DrawString(s.c_str(), -1, &font_, spt2, &scale_brush_);
    }
    if (rw_showGrid_) {
        auto spt(spt_ori);
        gp::PointF spt1(0, spt.Y);
        gp::PointF spt2(pConv->base_conv->swidth(), spt.Y);
        g.DrawLine(&grid_pen_, spt1, spt2);
        if (showAxis == false && rw_showGridText_ == true) {
            spt1.Y -= float(str_size.Height);
            g.DrawString(s.c_str(), -1, &font_, spt1, &grid_scale_brush_);
        }
    }
}

void AxisLayer::DrawMeasureScale(gp::Graphics & g, CoordSys * pConv)
{
    //画比例尺      
    gp::PointF scale_0(2.0, float(pConv->base_conv->sheight() - 20.0));
    gp::PointF scale_0_h(2.0, float(pConv->base_conv->sheight() - 23.0));

    gp::PointF scale_1(102.0, float(pConv->base_conv->sheight() - 20.0));
    gp::PointF scale_1_h(102.0, float(pConv->base_conv->sheight() - 23.0));

    g.DrawLine(&pen_, scale_0, scale_0_h);
    g.DrawLine(&pen_, scale_0, scale_1);
    g.DrawLine(&pen_, scale_1, scale_1_h);
    vPoint scale_view_0 = pConv->conv(scale_0);
    vPoint scale_view_1 = pConv->conv(scale_1);
    double len = (scale_view_0 - scale_view_1).Length();

    scale_0.X += 4;
    g.DrawString(misc::format_double(len), -1, &font_, scale_0, &brush_);
}
