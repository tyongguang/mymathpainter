#include "stdafx.h"
#include "SimpleLayer.h"
#include <tuple>
#include "geom.h"

SimpleLayer::SimpleLayer()
{
}


SimpleLayer::~SimpleLayer()
{
}

void SimpleLayer::draw_elements(Gdiplus::Graphics &g, CoordSys * pConv)
{
    RtreeBound bb;
    pConv->getRtreeBound(bb);
    auto input_args = std::make_tuple(&g, pConv, this, &bb);
    using input_args_type = decltype(input_args);
    rtree_.Search(bb.boundMin, bb.boundMax, [](IDrawable * element, void * args)
    {
        input_args_type * pInput = static_cast<input_args_type *>(args);
        auto & g = *std::get<0>(*pInput);
        auto pConv = std::get<1>(*pInput);
        auto pThis = std::get<2>(*pInput);
        auto &query_bound = *std::get<3>(*pInput);

        //if (true == element->hit_test(query_bound))
        element->draw(g, pConv, &pThis->default_style);
        
        return true;
    }, &input_args);

    for (auto & unbound : unBound_element_) {
        if (unbound.second->hit_test(bb) == true) {
            unbound.second->draw(g, pConv, &default_style);
        }
    }
}

void SimpleLayer::AddElement(std::shared_ptr<IDrawable> & ele)
{
    ele->SetLayer(this);
    draw_elements_.insert(std::make_pair(ele->getID(), ele));
    RtreeBound rb;
    if (true == ele->getBound(rb)) {
        rtree_.Insert(rb.boundMin, rb.boundMax, ele.get());
    }
    else {
        unBound_element_.insert(std::make_pair(ele->getID(), ele.get()));
    }
}

void SimpleLayer::Query(const RtreeBound & bound, std::vector<IDrawable * > &output)
{
    auto input_args = std::make_tuple(&output, this, &bound);
    using input_args_type = decltype(input_args);
    rtree_.Search(bound.boundMin, bound.boundMax, [](IDrawable * element, void * args)
    {
        input_args_type * pInput = static_cast<input_args_type *>(args);
        auto & output = *std::get<0>(*pInput);
        auto pThis = std::get<1>(*pInput);
        auto &query_bound = *std::get<2>(*pInput);

        if (true == element->hit_test(query_bound))
            output.push_back(element);

        return true;
    }, &input_args);

    for (auto & unbound : unBound_element_) {
        if (unbound.second->hit_test(bound) == true) {
            output.push_back(unbound.second);
        }
    }
}

bool SimpleLayer::getBound(RtreeBound & bound)
{
    return rtree_.GetBound(bound.boundMin, bound.boundMax);
}

void SimpleLayer::Delete(IDrawable * element)
{
    RtreeBound rb;
    if (true == element->getBound(rb)) {
        rtree_.Remove(rb.boundMin, rb.boundMax, element);
    }
    else {
        unBound_element_.erase(element->getID());
    }
    draw_elements_.erase(element->getID());
}

void SimpleLayer::RemoveAll()
{
    rtree_.RemoveAll();
    draw_elements_.clear();
    unBound_element_.clear();
}

