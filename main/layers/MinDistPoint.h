#pragma once
#include "layer.h"

class MinDistPoint
{
public:
    MinDistPoint(const vPoint &pt, double range, ILayer * layer);

    MinDistPoint(const vPoint &pt, double range, std::map<float, std::shared_ptr<ILayer> > * layers);
    MinDistPoint(const vPoint &pt, double range, IDrawable * element);
    MinDistPoint(const vPoint &pt, double range, std::vector<IDrawable *> & vec);
    MinDistPoint(const vPoint &pt, double range, std::vector<vPoint> & vec);
    MinDistPoint(const vPoint &pt, const vPoint &pt1, const vPoint &pt2);
    MinDistPoint(const vPoint &pt, MinDistPoint &mdp1, MinDistPoint &mdp2);
    inline const vPoint & getPoint() {
        return min_dist_point_;
    }
    inline bool exists() {
        return point_exist_;
    }
private:
    void base_cmp(const vPoint &pt, const vPoint &pt1, const vPoint &pt2);
    vPoint min_dist_point_;
    bool point_exist_;
};

