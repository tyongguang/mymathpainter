#pragma once
#include "layer.h"
#include <vector>
#include <memory>
#include "rtree.h"

class SimpleStyle : public IDrawStyle
{
public:
    SimpleStyle() {
        Gdiplus::Color pen_col = Gdiplus::Color(0, 0, 0);
        float pen_width = 1.0;
        Gdiplus::Color brush_col = Gdiplus::Color(55, 27, 194);


        pen_.reset(new Gdiplus::Pen(pen_col, pen_width));
        brush_.reset(new Gdiplus::SolidBrush(brush_col));

        raw_pen_.CreatePen(PS_SOLID, int(pen_width), pen_col.ToCOLORREF());
        raw_brush_.CreateSolidBrush(brush_col.ToCOLORREF());
    }
    virtual const Gdiplus::Pen * getPen() {
        return pen_.get();
    }
    virtual const Gdiplus::Brush * getBrush() {
        return brush_.get();
    }
    virtual float getPointRenderWidth() {
        return 5.0;
    }
    virtual CPenHandle getPenHandle() {
        return (HPEN) raw_pen_;
    }
    virtual CBrushHandle getBrushHandle() {
        return (HBRUSH)raw_brush_;
    }
private:
    std::unique_ptr<Gdiplus::Pen> pen_;
    std::unique_ptr<Gdiplus::Brush> brush_;
    WTL::CPen raw_pen_;
    WTL::CBrush raw_brush_;
};


class SimpleLayer :public CacheDrawlayer
    
{
public:
    SimpleLayer();
    ~SimpleLayer();
    virtual void Query(const RtreeBound & bound, std::vector<IDrawable * > &output);

    virtual void draw_elements(Gdiplus::Graphics &g, CoordSys * pConv);
    virtual void AddElement(std::shared_ptr<IDrawable> & ele);
    virtual bool getBound(RtreeBound & bound);
    virtual void Delete(IDrawable * element);
    virtual void RemoveAll();
    SimpleStyle default_style;
private:
    std::map<MGuid, std::shared_ptr<IDrawable>> draw_elements_;
    RTree<IDrawable *, double, 2> rtree_;
    std::map<MGuid, IDrawable * > unBound_element_;
};
