#pragma once
#include "layer.h"


class AxisScale;
class AxisLayer : public IAxisControl
{
public:
    AxisLayer();
    virtual void draw(Gdiplus::Graphics &g, CoordSys * pConv, int reason);
    void DrawScale(Gdiplus::Graphics &g, CoordSys * pConv);
protected:
    void DrawXScale(AxisScale * as, gp::Graphics & g, CoordSys * pConv, double i, bool showAxis);
    void DrawYScale(AxisScale * as, gp::Graphics & g, CoordSys * pConv, double i, bool showAxis);
    void DrawMeasureScale(gp::Graphics & g, CoordSys * pConv);
    gp::Pen pen_;
    gp::Pen grid_pen_;
    gp::Font font_;
    gp::LinearGradientBrush brush_;
    gp::SolidBrush scale_brush_;
    gp::SolidBrush grid_scale_brush_;
};

