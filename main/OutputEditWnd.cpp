#include "stdafx.h"
#include "OutputEditWnd.h"



#define MES_COPY        _T("����")
#define MES_SELECTALL   _T("ȫѡ")
#define MES_DELETE      _T("���")

LRESULT OutputEditWnd::OnContextMenu(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
    HWND hwndContext = (HWND)wParam;
    CPoint point = CPoint(GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));


    SetFocus();
    CMenu menu;
    menu.CreatePopupMenu();
    BOOL bReadOnly = GetStyle() & ES_READONLY;
    DWORD flags;

    DWORD sel = GetSel();
    flags = LOWORD(sel) == HIWORD(sel) ? MF_GRAYED : 0;
    menu.InsertMenu(0, MF_BYPOSITION | flags, ID_EDIT_COPY,
        MES_COPY);

    int len = GetWindowTextLength();
    flags = (!len || (LOWORD(sel) == 0 && HIWORD(sel) ==
        len)) ? MF_GRAYED : 0;
    menu.InsertMenu(1, MF_BYPOSITION | flags, ID_EDIT_SELECT_ALL,
        MES_SELECTALL);



    menu.InsertMenu(2, MF_BYPOSITION | MF_SEPARATOR);
    menu.InsertMenu(3, MF_BYPOSITION, ID_EDIT_CLEAR_ALL,
        MES_DELETE);


    menu.TrackPopupMenu(TPM_LEFTALIGN | TPM_LEFTBUTTON |
        TPM_RIGHTBUTTON, point.x, point.y, hwndContext);
    return 0;
}
