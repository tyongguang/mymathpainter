#include "stdafx.h"
#include "ExpressionParser.h"
#include <sstream>

#define CHECK_SURE_RESULT(r, errCode) if (r == false ) { errors.push_back(errCode); return false;}
#define  SYS_FN(name) sys_math_lib::expression##name


#define  FN_ADD sys_math_lib::expression_add
#define  FN_MIN sys_math_lib::expression_min
#define  FN_MUL sys_math_lib::expression_mul
#define  FN_DIV sys_math_lib::expression_div

struct sys_math_lib {
    inline static bool expression_add(double a, double b, double * y) {
        *y = a + b;
        return !isnan(*y);
    }

    inline static bool expression_min(double a, double b, double * y) {
        *y = a - b;
        return !isnan(*y);
    }

    inline static bool expression_mul(double a, double b, double * y) {
        *y = a * b;
        return !isnan(*y);
    }

    inline static bool expression_div(double a, double b, double * y) {
        *y = a / b;
        return !isnan(*y);
    }


    sys_math_lib() {
    }

    inline static bool expression_sin(double a, double b, double * y) {
        *y = sin(a);
        return !isnan(*y);
    }
    inline static bool expression_cos(double a, double b, double * y) {
        *y = cos(a);
        return !isnan(*y);
    }
    inline static bool expression_tan(double a, double b, double * y) {
        *y = tan(a);
        return !isnan(*y);
    }
  
    inline static bool expression_ctg(double a, double b, double * y) {
        *y = 1/tan(a);
        return !isnan(*y);
    }
    

    inline static bool expression_asin(double a, double b, double * y) {
        *y = asin(a);
        return !isnan(*y);
    }
    inline static bool expression_acos(double a, double b, double * y) {
        *y = acos(a);
        return !isnan(*y);
    }
    inline static bool expression_atan(double a, double b, double * y) {
        *y = atan(a);
        return !isnan(*y);
    }

    inline static bool expression_sqrt(double a, double b, double * y) {
        *y = sqrt(a);
        return !isnan(*y);
    }

    inline static bool expression_log(double a, double b, double * y) {
        *y = log10(a);
        return !isnan(*y);
    }

    inline static bool expression_ln(double a, double b, double * y) {
        *y = log(a);
        return !isnan(*y);
    }

    inline static bool expression_pow(double a, double b, double * y) {
        *y = pow(a, b);
        return !isnan(*y);
    }
    inline static bool expression_power(double a, double b, double * y) {
        *y = pow(a, b);
        return !isnan(*y);
    }

    inline static bool expression_exp(double a, double b, double * y) {
        *y = exp(a);
        return !isnan(*y);
    }

    inline static bool expression_abs(double a, double b, double * y) {
        *y = fabs(a);
        return !isnan(*y);
    }
    inline static bool expression_sigmoid(double a, double b, double * y) {
        *y = 1 / (1 + exp(-a));
        return !isnan(*y);
    }
    inline static bool expression_tanh(double a, double b, double * y) {
        double m = exp(a);
        double n = exp(-a);
        *y = (m - n) / (m + n);
        return !isnan(*y);
    }
    
};

#define INIT_SYS_FN(fn_map, fn_cnt_map, cnt, fn)  fn_map.insert(std::make_pair(#fn, (IExpressionNode::expr_fn_type)SYS_FN(_##fn))); \
    fn_cnt_map[#fn] = cnt

sys_fn::sys_fn()
{
    INIT_SYS_FN(sys_fns, sys_fns_params_count, 1, sin);
    INIT_SYS_FN(sys_fns, sys_fns_params_count, 1, cos);
    INIT_SYS_FN(sys_fns, sys_fns_params_count, 1, tan);
    INIT_SYS_FN(sys_fns, sys_fns_params_count, 1, ctg);
    INIT_SYS_FN(sys_fns, sys_fns_params_count, 1, asin);
    INIT_SYS_FN(sys_fns, sys_fns_params_count, 1, acos);
    INIT_SYS_FN(sys_fns, sys_fns_params_count, 1, atan);
    INIT_SYS_FN(sys_fns, sys_fns_params_count, 1, sqrt);
    INIT_SYS_FN(sys_fns, sys_fns_params_count, 1, log);
    INIT_SYS_FN(sys_fns, sys_fns_params_count, 1, ln);
    INIT_SYS_FN(sys_fns, sys_fns_params_count, 1, exp);
    INIT_SYS_FN(sys_fns, sys_fns_params_count, 1, abs);
    INIT_SYS_FN(sys_fns, sys_fns_params_count, 1, sigmoid);
    INIT_SYS_FN(sys_fns, sys_fns_params_count, 1, tanh);
    INIT_SYS_FN(sys_fns, sys_fns_params_count, 2, pow);
    INIT_SYS_FN(sys_fns, sys_fns_params_count, 2, power);


    AddSymbol("pi", 3.14159265358979323846);
    AddSymbol("e", 2.718281828459);
    
}

void sys_fn::AddNewFuction(const char * newfunctionName, IExpressionNode::expr_fn_type fn, bool is_two_param)
{
    sys_fns.insert(std::make_pair(std::string(newfunctionName), fn));
    if (is_two_param == true)
        sys_fns_params_count[newfunctionName] = 2;
    else
        sys_fns_params_count[newfunctionName] = 1;
}

void sys_fn::AddSymbol(const char * symbol, double value)
{
    sys_symbol[symbol] = value;
}

ExpressionParser::ExpressionParser(const char * str) : parser_(str)
{
    ok_ = true;
    if (false == Parse_E(expression_)) {
        ok_ = false;
        format_error();
        return;
    }
    parser_.EatSpace();
    if (parser_.isEnd() == false) {
        ok_ = false;
        std::stringstream ss;
        ss << "第" << parser_.CurPos() << "个字符 \'" << parser_.CurChar() << "\'无法识别";
        msg_ = ss.str();
    }
}

bool ExpressionParser::Parse_E(std::shared_ptr<IExpressionNode> & output_exp)
{
    ExpressionNode * new_node(new ExpressionNode());
    output_exp.reset(new_node);
    CHECK_SURE_RESULT(Parse_T(new_node->left), AllError::eError_E_T);
    CHECK_SURE_RESULT(Parse_E1(output_exp), AllError::eError_E_E1);
    if (new_node->fn == nullptr) {
        auto left = new_node->left; //必须先hold住，要不下面赋值会将父亲析构，而导致，自己也析构
        output_exp = left;
    }

    return true;
}

bool ExpressionParser::Parse_T(std::shared_ptr<IExpressionNode> & output_exp)
{
    ExpressionNode * new_node(new ExpressionNode());
    output_exp.reset(new_node);

    CHECK_SURE_RESULT(Parse_F(new_node->left), AllError::eError_T_F);
    CHECK_SURE_RESULT(Parse_T1(output_exp), AllError::eError_T_T1);
    if (new_node->fn == nullptr) {
        auto left = new_node->left; //必须先hold住，要不下面赋值会将父亲析构，而导致，自己也析构
        output_exp = left;
    }
    return true;
}

bool ExpressionParser::Parse_E1(std::shared_ptr<IExpressionNode> & parent_exp)
{
    if (parser_.isEnd() == true)
        return true;

    char c = parser_.CurChar();
    if (c == '+' || c == '-') {
        //parent_exp->setFn(c == '+' ? FN_ADD : FN_MIN);

        //这里统统为正号，让负号传给后面处理
        parent_exp->setFn(FN_ADD); 
        if (c == '+') {
            parser_.AdvanceSpace();
        }

        ExpressionNode * new_node(new ExpressionNode());
        std::shared_ptr<IExpressionNode> s_new_node(new_node);
        parent_exp->setRight(s_new_node);
        CHECK_SURE_RESULT(Parse_T(new_node->left), AllError::eError_E1_T);
        CHECK_SURE_RESULT(Parse_E1(s_new_node), AllError::eError_E1_E1);
        if (new_node->fn == nullptr) {
            parent_exp->setRight(new_node->left);
            //s_new_node,过完这个作用域后，将析构
        }
        return true;

    }

    //因为有eof ，如果什么都匹配上就走人
    return true;
}

bool ExpressionParser::Parse_T1(std::shared_ptr<IExpressionNode> & parent_exp)
{
    if (parser_.isEnd() == true)
        return true;

    char c = parser_.CurChar();
    if (c == '*' || c == '/') {

        //parent_exp->setFn(c == '*' ? FN_MUL : FN_DIV);

        //这里统统为乘号，让除号传给后面处理
        parent_exp->setFn(FN_MUL);
        if (c == '*') {
            parser_.AdvanceSpace();
        }

        ExpressionNode * new_node(new ExpressionNode());
        std::shared_ptr<IExpressionNode> s_new_node(new_node);
        parent_exp->setRight(s_new_node);

        CHECK_SURE_RESULT(Parse_F(new_node->left), AllError::eError_T1_F);
        CHECK_SURE_RESULT(Parse_T1(s_new_node), AllError::eError_T1_T1);
        if (new_node->fn == nullptr) {
            parent_exp->setRight(new_node->left);
            //s_new_node,过完这个作用域后，将析构
        }
        return true;
    }
    //因为有eof ，如果什么都匹配上就走人
    return true;
}

bool ExpressionParser::Parse_F(std::shared_ptr<IExpressionNode> & output_exp)
{
    char c = parser_.CurChar();
    if (c == '-' || c == '+') {
        parser_.AdvanceSpace();
        CHECK_SURE_RESULT(Parse_F1(output_exp), AllError::eError_F_F1);
        if (c == '-') {
            output_exp->setNegative();
        }
        return true;
    }
    if (c == '/') {
        parser_.AdvanceSpace();
        CHECK_SURE_RESULT(Parse_F1(output_exp), AllError::eError_F_F1);
        output_exp->setDiv();
        return true;

    }
    return Parse_F1(output_exp);

}


bool ExpressionParser::Parse_F1(std::shared_ptr<IExpressionNode> & output_exp)
{
    char c = parser_.CurChar();
    if (c >= '0' && c <= '9') {
        double result;
        parser_.GetDouble(&result);
        output_exp.reset(new ValueNode(result));
        parser_.EatSpace();
        return true;
    }
    if (c == '(') {
        parser_.AdvanceSpace();
        CHECK_SURE_RESULT(Parse_E(output_exp), AllError::eError_F1_E);
        CHECK_SURE_RESULT(parser_.Sure(')'), AllError::eError_F1_brace2);
        parser_.EatSpace();
        return true;
    }

    if (c == 'x' || c == 'X') {
        output_exp.reset(new ValueXNode());
        parser_.AdvanceSpace();
        return true;
    }
    std::string symbol;
    auto type = TestSymbol(symbol);
    if (type == SymbolType::notSymbol) {
        CHECK_SURE_RESULT(false, AllError::eError_F);
        return false;
    }
    else if (type == SymbolType::isNumber) {
        //assert sys_fn::Instance().sys_symbol.find(symbol) != sys_fn::Instance().sys_symbol.end()
        double symbol_value = sys_fn::Instance().sys_symbol[symbol];
        output_exp.reset(new ValueNode(symbol_value));
        parser_.EatSpace();
        return true;

    }
    else if (type == SymbolType::isFunction) {
        if (true == FunctionMatch(output_exp, symbol)) {
            return true;
        }
    }

    CHECK_SURE_RESULT(false, AllError::eError_F);
}

void ExpressionParser::getSearchString(std::string & s_begin, std::string & s_end)
{
    char c = parser_.CurChar();
    s_begin.push_back(tolower(c));
 

    s_end.push_back(tolower(c));
    s_end.push_back('}');

}

ExpressionParser::SymbolType ExpressionParser::TestSymbol(std::string & symbol)
{
    std::string s_begin, s_end;
    getSearchString(s_begin, s_end);

    auto & cur_number_symbol = sys_fn::Instance().sys_symbol;
    auto iter_end = cur_number_symbol.upper_bound(s_end);


    size_t max_name_len = 0;
    SymbolType  type = SymbolType::notSymbol;

    for (auto iter = cur_number_symbol.lower_bound(s_begin); iter != iter_end; ++iter) {
        const std::string & fn_name = iter->first;
        size_t fn_name_len = fn_name.size();
        if (0 == _strnicmp(fn_name.c_str(), parser_.getCurrentPoint(), fn_name_len)) {
            if (fn_name_len > max_name_len) {
                max_name_len = fn_name_len;
                symbol = fn_name;
                type = SymbolType::isNumber;

            }
        }
    }


    auto & cur_function_symbol = sys_fn::Instance().sys_fns;
    auto iter_function_end = cur_function_symbol.upper_bound(s_end);


    for (auto iter = cur_function_symbol.lower_bound(s_begin); iter != iter_function_end; ++iter) {
        const std::string & fn_name = iter->first;
        size_t fn_name_len = fn_name.size();
        if (0 == _strnicmp(fn_name.c_str(), parser_.getCurrentPoint(), fn_name_len)) {
            if (fn_name_len > max_name_len) {
                max_name_len = fn_name_len;
                symbol = fn_name;
                type = SymbolType::isFunction;
            }
        }
    }
    if (max_name_len != 0) {
        parser_.offset(max_name_len);
        parser_.EatSpace();
    }
    return type;
}

bool ExpressionParser::FunctionMatch(std::shared_ptr<IExpressionNode> & output_exp, const std::string & symbol)
{
    IExpressionNode::expr_fn_type  fn = sys_fn::Instance().sys_fns[symbol];
    bool is_two_param = sys_fn::Instance().sys_fns_params_count[symbol] == 2;


    ExpressionNode * node = new ExpressionNode(fn);
    output_exp.reset(node);


    CHECK_SURE_RESULT(parser_.Sure('('), AllError::eError_M_brace1);
    parser_.EatSpace();
    CHECK_SURE_RESULT(Parse_E(node->left), AllError::eError_M_E);
    if (is_two_param) {
        CHECK_SURE_RESULT(parser_.Sure(','), AllError::eError_M_comma);
        parser_.EatSpace();

        CHECK_SURE_RESULT(Parse_E(node->right), AllError::eError_M_E);
        parser_.EatSpace();
    }
    CHECK_SURE_RESULT(parser_.Sure(')'), AllError::eError_M_brack2);
    parser_.EatSpace();
    return true;
}

void ExpressionParser::format_error()
{
    if (errors.size() == 0)
        return;
    AllError err_type = errors.front();
    std::stringstream ss;
    if (parser_.isEnd()) {
        ss << "表达式不完整 ： ";
    }
    else {
        ss << "第" << parser_.CurPos() << "个字符 \'" << parser_.CurChar() << "\'出现问题: ";
    }
    switch (err_type)
    {
    case ExpressionParser::AllError::eError_E_T:
        ss << "E -> TE1 中的F";
        break;
    case ExpressionParser::AllError::eError_E_E1:
        ss << "E -> TE1 error";
        break;
    case ExpressionParser::AllError::eError_T_F:
        ss << "T -> F T1 中的F";
        break;
    case ExpressionParser::AllError::eError_T_T1:
        ss << "T -> F T1 中的T1";
        break;
    case ExpressionParser::AllError::eError_E1_T:
        ss << "E1 -> +|- TE1 中的T";
        break;
    case ExpressionParser::AllError::eError_E1_E1:
        ss << "E1 -> +|- TE1 中的E1";
        break;
    case ExpressionParser::AllError::eError_T1_F:
        ss << "T1 -> *|/ FT1 中的F";
        break;
    case ExpressionParser::AllError::eError_T1_T1:
        ss << "T1 -> *|/ FT1 中的T1";
        break;
    case ExpressionParser::AllError::eError_F_F1:
        ss << "F -> -|+|F1 中的F1";
        break;
    case ExpressionParser::AllError::eError_F1_E:
        ss << "F1 -> num|(E)|x|M 中的E";
        break;
    case ExpressionParser::AllError::eError_F1_brace2:
        ss << "F1 -> num|(E)|x|M 中的)";
        break;
    case ExpressionParser::AllError::eError_M_brace1:
        ss << "F1 -> num|(E)|x|M 中的)";
        break;
    case ExpressionParser::AllError::eError_M_comma:
        ss << "函数需要输入两个参数，用逗号分隔";
        break;
    case ExpressionParser::AllError::eError_M_brack2:
        ss << "M -> Fuc(E)|Func(E,E) 中的)";
        break;
    case ExpressionParser::AllError::eError_M_E:
        ss << "M -> Fuc(E)|Func(E,E) 中的E";
        break;
    case ExpressionParser::AllError::eError_M_FUNC:
        if (parser_.isEnd() == false)
            ss << "找不到以 " << parser_.CurChar() << " 开头的函数";
        break;
    case ExpressionParser::AllError::eError_F:
        ss << "F无法匹配";
        break;
    default:
        break;
    }
    msg_ = ss.str();
}

