#pragma once
#include <string>

class ParseString
{
public:
    ParseString() :m_nPos(0)
    {

    }
	ParseString(const char * pString)
		:m_pString(pString),m_nPos(0)
	{
	}

    inline void SetString(const char * pString)
    {
        m_pString = pString;
    }
	inline bool isEnd()
	{
		if (m_pString[m_nPos] == '\0')
			return true;
		return false;
	}
	inline bool isPreviewEnd()
	{
		if (m_pString[m_nPos + 1] == '\0')
			return true;
		return false;
	}
	inline char PreviewChar()
	{
		return m_pString[m_nPos + 1];
	}
	inline char BackChar()
	{
		return m_pString[m_nPos - 1];
	}
	inline char CurChar()
	{
		return m_pString[m_nPos];
	}

    inline void offset(int i) {
        m_nPos += i;
    }
	inline void Advance()
	{
		m_nPos++;
	}
	inline void AdvanceSpace()
	{
		do 
		{
			m_nPos++;
		} while (m_pString[m_nPos] == ' '|| m_pString[m_nPos] == '\t' || m_pString[m_nPos] == '\r'|| m_pString[m_nPos] == '\n');
	}
	inline void EatSpace()
	{
		 while (m_pString[m_nPos] == ' '|| m_pString[m_nPos] == '\t' || m_pString[m_nPos] == '\r'|| m_pString[m_nPos] == '\n')
		 {
			m_nPos++;
		 }
	}
	inline bool Sure(char c)
	{
		bool b = m_pString[m_nPos] == c;
		if (b == true)
			Advance();
		return b;
	}
	inline int CurPos()
	{
		return m_nPos;
	}
	bool GetInt(int * pResult);
    bool GetDouble(double * pResult);
	bool GetBrokenString(std::string & str, char nBrokenKey, char nBrokenKey2 =  -1);
    inline int Char2Int(char c)
    {
        return c - '0';
    }
    inline const char * getCurrentPoint() {
        return m_pString + m_nPos;
    }
    

protected:
	const char * m_pString;
	int m_nPos;
};