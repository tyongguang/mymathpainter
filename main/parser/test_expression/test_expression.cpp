// test_expression.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include "../ExpressionParser.h"
#include "../../tools/util.h"


void string_replace(std::string& strBig, const std::string & strsrc, const std::string &strdst)
{
    std::string::size_type pos = 0;
    while ((pos = strBig.find(strsrc, pos)) != std::string::npos)
    {
        strBig.replace(pos, strsrc.length(), strdst);
        pos += strdst.length();
    }
}

double fun(double x) {
    return cos(x * x * (2 * (5 * (3 + x) - x * x / 2) - 5 * sin(x)));
}
int _tmain(int argc, _TCHAR* argv[])
{
    const char *  expressions[] = {
        "2-1-x"
        ,"sqrt(x)"
        , "sqrt(-x *)"
        , "sqrt(-x)"
        , "sqrt(-x * -2.1)"
        , "sqrt(-x * -x)"
        , "log(x)"
        , "tan(x)"
        , "cos(x)"
        , "abs(x)"
        , "abs(-x)"
        ,"-x"
        , "(x + 10)"
        , "x + 10"
        , "10 + x"
        , "10 * x"
        , "2 + 10 * x"
        , "x * 10"
        , "x * 0.001"
        , "(x * 10)"
        , "(-x + 10)"
        , "cos(x * x * (2 * (5 * (3 + x) -  x * x /2) - 5 * sin(x)))"
        , "tan(5 + 2 * x) + x * x - x"
        , "x"
        , "-x"
        , "--x"
        , "x * x"
        , "sin(x)"
        , "-x * x"
        , "(x) * (x -1)"
        , "(x) * x -1"
        , "pow(x,3)"
        , "pow( x, 3)"
        , "pow(x)"
        , "pow(1-1/x, x)"

    };
    for (auto & ex : expressions) {
        ExpressionParser parser(ex);
        const double value = -1;
        double result;
        std::string xx(ex);
        string_replace(xx, "x", "(-1)");

        if (parser.isOK()) {

            if (true == parser.getResult()->getValue(value, &result)) {
                std::cout << "expression:" << xx << " =>   "<< " " <<
                    result << std::endl;
            }
            else {
                std::cout << "expression:" << xx << " => " << " value invalid " << std::endl;
            }
        }
        else {
            std::cout << "expression:" << ex << "   parse fail! =>" << parser.getMsg() << std::endl;
        }
    }

    //���ܲ���
    TimeMeasure tm;
    
    for (int i = 0; i < 100000; ++i) {
        fun(i  % 100);
    }
    std::cout << "c:" << tm.Elapse() << std::endl;

    ExpressionParser parser("cos(x * x * (2 * (5 * (3 + x) -  x * x /2) - 5 * sin(x)))");
    
    tm.Restart();
    double xx;
    for (int i = 0; i < 100000; ++i) {
        parser.getResult()->getValue((i % 100), &xx);
    }
    std::cout << "me:" << tm.Elapse() << std::endl;

	return 0;
}

