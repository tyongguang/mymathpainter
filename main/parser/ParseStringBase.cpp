#include "stdafx.h"
#include "ParseStringBase.h"


bool ParseString::GetInt( int * pResult )
{
	const int nMaxInt = 9;//int的最大值为9位数
	char c = '\0';
	int nPos = 0;
	int nResult = 0;
	while(nPos < nMaxInt)
	{
		c = this->CurChar();
		if (c >='0' && c <= '9')
		{
			if (nPos != 0)
				nResult *= 10;

			nResult += c - '0';
			nPos ++;
			this->Advance();
		}else
		{
			break;
		}
	}
	if (nPos == 0 || nPos >= nMaxInt)
		return false;

	* pResult = nResult;
	return true;
}

bool ParseString::GetBrokenString( std::string & str, char nBrokenKey, char nBrokenKey2)
{
	char c = '\0';
	int nPosBegin = m_nPos;
	this->Advance();
	do 
	{
		c = this->CurChar();
		if (c == nBrokenKey || c == ' ' || isEnd() || (nBrokenKey2 != -1 && c == nBrokenKey2) )
		{
			str.assign(m_pString + nPosBegin, m_nPos - nPosBegin);
			return true;
		}
		this->Advance();
    } while (false == isEnd());
	return false;
}

bool ParseString::GetDouble(double * pResult)
{
    char c = '\0';
    
    int nResult = 0;
    long long nResult2 = 0;
    long long nResult2Len = 1;
    bool bFindPoint = false;
    int plus_minus= 1;
    bool match_number = false;
    while (false == isEnd())
    {
        c = this->CurChar();
        //负号必须出现在数字前
        if (c == '-' && match_number == false) {
            plus_minus = -1;
            this->Advance();
        }
        else if (c >= '0' && c <= '9')
        {
            match_number = true;
            if (bFindPoint == false)
            {
                nResult *= 10;
                nResult += Char2Int(c);

            }
            else
            {
                nResult2 *= 10;
                nResult2Len *= 10;
                nResult2 += Char2Int(c);
            }
            this->Advance();
        }
        else if (c == '.') {
            bFindPoint = true;
            this->Advance();
        }else{
            break;
        }
    }
    if (match_number == false)
        return false;
    *pResult = plus_minus * (nResult + (double)nResult2 / nResult2Len);
    return true;

}