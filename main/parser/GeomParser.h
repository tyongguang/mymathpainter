#pragma once
#include <string>
#include "ParseStringBase.h"
#include "layer.h"

class GeomParser
{
public:
    GeomParser(const char * str);
    bool isOK() {
        return ok_;
    }
    std::string & getMsg() {
        return msg_;
    }
    void getResult(std::vector<std::shared_ptr<IDrawable> > & vec) {
        vec.clear();
        vec_.swap(vec);
    }

private:
    bool ParseLineString();
    bool ParseRay();
    bool ParseLine();
    bool ParseRay2();
    bool ParseLine2();
    bool ParsePoint();
    bool ParsePolygon();
    bool BeginParse();
    bool ParseXY(vPoint & pt);
    bool ParseManyPoints(std::vector<vPoint> & pts);
    std::vector<std::shared_ptr<IDrawable> > vec_;
    bool ok_;
    std::string msg_;
    ParseString parser_;
};