#include "stdafx.h"
#include "GeomParser.h"
#include "geom/geomLineString.h"
#include "geom/geomPoint.h"
#include "geom/geomPolygon.h"
#include "geom/geomRay.h"
#include "geom/geomLine.h"


GeomParser::GeomParser(const char * str) :parser_(str), ok_(false)
{
    ok_ = BeginParse();
}

bool GeomParser::BeginParse()
{
    while (parser_.isEnd() == false)
    {
 
        parser_.EatSpace();
        std::string strHeader;
        parser_.GetBrokenString(strHeader, '(');

        if (0 == _stricmp(strHeader.c_str(), "LINESTRING"))
        {
            if (false == ParseLineString())
                return false;

        }
        else if (0 == _stricmp(strHeader.c_str(), "POINT")){
            if (false == ParsePoint())
                return false;
        }
        else if (0 == _stricmp(strHeader.c_str(), "POLYGON")){
            if (false == ParsePolygon())
                return false;
        }
        else if (0 == _stricmp(strHeader.c_str(), "RAY")){
            if (false == ParseRay())
                return false;
        }
        else if (0 == _stricmp(strHeader.c_str(), "RAY2")){
            if (false == ParseRay2())
                return false;
        }
        else if (0 == _stricmp(strHeader.c_str(), "LINE")){
            if (false == ParseLine())
                return false;
        }
        else if (0 == _stricmp(strHeader.c_str(), "LINE2")){
            if (false == ParseLine2())
                return false;
        }
        else {
            msg_ = "\"" + strHeader + "\" 未定义";
            return false;
        }
    }
    return true;
}


bool GeomParser::ParseLineString()
{
    std::vector<vPoint> pts;
    if (true == ParseManyPoints(pts)) {
        vec_.push_back(std::shared_ptr<IDrawable>(new geomLineString(pts, true)));
        return true;
    }
    return true;
}

bool GeomParser::ParsePolygon()
{
    parser_.EatSpace();
    if (false == parser_.Sure('(')) {
        msg_ = "Polygon后找不到 （";
        return false;
    }

    std::vector<vPoint> pts;
    vPoint vpt;
    while (parser_.isEnd() == false)
    {
        std::vector<vPoint> pts;
        if (true == ParseManyPoints(pts)) {
            vec_.push_back(std::shared_ptr<IDrawable>(new geomPolygon(pts, true)));
        }
        else{
            return false;
        }

        
        if (parser_.CurChar() == ',') {
            parser_.AdvanceSpace();
            continue;
        }
        else if (parser_.CurChar() == ')') {
            parser_.AdvanceSpace();
            return true;
        }
        else {
            msg_ = "Polygon无效字符：" + parser_.CurChar();
            return false;
        }
    }

    return true;
}

bool GeomParser::ParsePoint()
{
    parser_.EatSpace();
    if (false == parser_.Sure('(')) {
        msg_ = "Point后找不到 （";
        return false;
    }

    vPoint vpt;
    while (parser_.isEnd() == false)
    {
        if (false == ParseXY(vpt))
            return false;

        if (false == parser_.Sure(')')) {
            msg_ = "POINT后找不到 )";
            return false;
        }

        vec_.push_back(std::shared_ptr<IDrawable>(new geomPoint(vpt)));
        ok_ = true;
        parser_.EatSpace();
        return true;

    }
    return true;
}

bool GeomParser::ParseXY(vPoint & pt)
{
    parser_.EatSpace();
    if (false == parser_.GetDouble(&pt.X())) {
        msg_ = "解释double X错误";
        return false;
    }

    parser_.EatSpace();
    if (false == parser_.GetDouble(&pt.Y())) {
        msg_ = "解释double Y错误";
        return false;
    }
    parser_.EatSpace();
    return true;
}

bool GeomParser::ParseManyPoints(std::vector<vPoint> & pts)
{
    parser_.EatSpace();
    if (false == parser_.Sure('(')) {
        msg_ = "多点[Linestring /Polygon] 项找不到 （";
        return false;
    }

    vPoint vpt;
    while (parser_.isEnd() == false)
    {
        if (false == ParseXY(vpt))
            return false;

        pts.push_back(vpt);
        if (parser_.CurChar() == ',') {
            parser_.AdvanceSpace();
            continue;
        }
        else if (parser_.CurChar() == ')') {
            parser_.AdvanceSpace();
            return true;
        }
        else {
            msg_ = "多点[Linestring /Polygon]：" + parser_.CurChar();
            return false;
        }


    }
    return false;
}

bool GeomParser::ParseRay()
{
    std::vector<vPoint> pts;
    if (true == ParseManyPoints(pts)) {
        if (pts.size() != 2) {
            msg_ = "Ray点只有两个！！！";
            return false;
        }
        pts[1].Normalize();
        vec_.push_back(std::shared_ptr<IDrawable>(new geomRay(pts[0], pts[1])));
        return true;
    }
    return true;
}

bool GeomParser::ParseLine()
{
    std::vector<vPoint> pts;
    if (true == ParseManyPoints(pts)) {
        if (pts.size() != 2) {
            msg_ = "Line点只有两个！！！";
            return false;
        }
        pts[1].Normalize();
        vec_.push_back(std::shared_ptr<IDrawable>(new geomLine(pts[0], pts[1])));
        return true;
    }
    return true;
}

bool GeomParser::ParseRay2()
{
    std::vector<vPoint> pts;
    if (true == ParseManyPoints(pts)) {
        if (pts.size() != 2) {
            msg_ = "Ray2点只有两个！！！";
            return false;
        }
        auto dir = pts[1] - pts[0];
        dir.Normalize();
        vec_.push_back(std::shared_ptr<IDrawable>(new geomRay(pts[0], dir)));
        return true;
    }
    return true;
}

bool GeomParser::ParseLine2()
{
    std::vector<vPoint> pts;
    if (true == ParseManyPoints(pts)) {
        if (pts.size() != 2) {
            msg_ = "Line2点只有两个！！！";
            return false;
        }
        auto dir = pts[1] - pts[0];
        dir.Normalize();
        vec_.push_back(std::shared_ptr<IDrawable>(new geomLine(pts[0], dir)));
        return true;
    }
    return true;
}
