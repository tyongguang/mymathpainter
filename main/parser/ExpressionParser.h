#pragma once
#include <string>
#include <memory>
#include <vector>
#include <functional>
#include <map>
#include "ParseStringBase.h"


class IExpressionNode {
public:
    IExpressionNode() : sign(1), is_div(false) {}
    typedef std::function<bool(double a, double b, double *)> expr_fn_type;
    virtual ~IExpressionNode() {}
    virtual bool getValue(double x, double * y) = 0;
    virtual void setLeft(std::shared_ptr<IExpressionNode> & ileft) {
    }

    virtual void setRight(std::shared_ptr<IExpressionNode> & iRight) {
    }

    virtual void setFn(expr_fn_type ifn) {
    }
    void setNegative() {
        sign = -1;
    };
    void setDiv() {
        is_div = true;
    }
    short sign;
    bool is_div;
};
    
class ValueNode : public IExpressionNode {
public:
    ValueNode(double v) :value(v) {
    }

    virtual bool getValue(double x, double * y) {
        *y = value * sign;
        if (is_div)
            *y = 1.0f / *y;
        return true;
    }
    double value;
};


class ValueXNode : public IExpressionNode {
public:
    virtual bool getValue(double x, double * y) {
        *y = x * sign;
        if (is_div)
            *y = 1.0f / *y;
        return true;
    }
};

class ExpressionNode : public IExpressionNode {
public:
    ExpressionNode() {}
    ~ExpressionNode(){}
    ExpressionNode(expr_fn_type ifn) : fn(ifn) {
    }
    virtual bool getValue(double x, double * y) {
        double a = 0;
        double b = 0;
        double result = 0;

        // -100 ， 这种情况，左边是空的
        if (left.get()) {
            bool a_result = left->getValue(x, &a);
            if (a_result == false)
                return false;
            //最后一个叶子
            if (fn == nullptr) {
                *y = a * sign;
                if (is_div)
                    *y = 1.0f / *y;
                return !isnan(*y);
            }
        }

        if (right.get()) {
            //此时函数必须不需要右值
            bool b_result = right->getValue(x, &b);
            if (b_result == false)
                return false;
        }

        if (false == fn(a, b, &result))
            return false;

        *y = result * sign;
        if (is_div)
            *y = 1.0f / *y;
        return !isnan(*y);
    }
    virtual void setLeft(std::shared_ptr<IExpressionNode> & ileft) {
        left = ileft;
    }
    virtual void setRight(std::shared_ptr<IExpressionNode> & iRight) {
        right = iRight;
    }

    virtual void setFn(expr_fn_type  ifn) {
        fn = ifn;
    }
    std::function<bool(double a, double b, double *)> fn;
    std::shared_ptr<IExpressionNode> left;
    std::shared_ptr<IExpressionNode> right;


};
class ExpressionParser;

class sys_fn {
public:
    friend ExpressionParser;
    static sys_fn & Instance() {
        static sys_fn sfn;
        return sfn;
    }
    void AddNewFuction(const char * newfunctionName, IExpressionNode::expr_fn_type fn, bool is_two_param);
    void AddSymbol(const char * symbol, double value);

private:
    std::map<std::string, IExpressionNode::expr_fn_type > sys_fns;
    std::map<std::string, double > sys_symbol;
    std::map<std::string, unsigned char > sys_fns_params_count;
    sys_fn();
};

class ExpressionParser
{
public:

    ExpressionParser(const char * str);
    bool isOK() {
        return ok_;
    }
    std::string & getMsg() {
        return msg_;
    }
    std::shared_ptr<IExpressionNode> getResult() {
        return expression_;
    }

    enum class AllError
    {
        eError_E_T = 1000
        , eError_E_E1
        , eError_T_F
        , eError_T_T1
        , eError_E1_T
        , eError_E1_E1
        , eError_T1_F
        , eError_T1_T1
        , eError_F_F1
        , eError_F1_E
        , eError_F1_brace2
        , eError_F
        , eError_M_brace1
        , eError_M_comma
        , eError_M_brack2
        , eError_M_E
        , eError_M_FUNC
        
    };
    enum class SymbolType
    {
        isFunction = 0,
        isNumber,
        notSymbol
    };
private:
    void format_error();
    bool Parse_E(std::shared_ptr<IExpressionNode> & output_exp);

    bool Parse_T(std::shared_ptr<IExpressionNode> & output_exp);
    bool Parse_E1(std::shared_ptr<IExpressionNode> & parent_exp);
    bool Parse_T1(std::shared_ptr<IExpressionNode> & parent_exp);

    bool Parse_F(std::shared_ptr<IExpressionNode> & output_exp);
    bool Parse_F1(std::shared_ptr<IExpressionNode> & output_exp);

    SymbolType TestSymbol(std::string & symbol);
    bool FunctionMatch(std::shared_ptr<IExpressionNode> & output_exp, const std::string & symbol);


    void getSearchString(std::string & s_begin, std::string & s_end);
    std::vector<AllError> errors;
    bool ok_;
    std::string msg_;
    ParseString  parser_;
    AllError error_code;
    std::shared_ptr<IExpressionNode> expression_;
};