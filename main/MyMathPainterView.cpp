// MyMathPainterView.cpp : implementation of the CMyMathPainterView class
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Ribbon.h"
#include "resource.h"
#include "guimisc.h"
#include <set>
#include <tuple>

#include "MyMathPainterView.h"


#include "layers/SimpleLayer.h"
#include "geom/geomPoint.h"
#include "geom/geomLineString.h"
#include "geom/geomPolygon.h"
#include "layers/MinDistPoint.h"
#include "geom/geomExpression.h"
#include <math.h>

CMyMathPainterView::CMyMathPainterView()
    :draw_reason_(draw_reson::draw_reson_Force)
{

}

BOOL CMyMathPainterView::PreTranslateMessage(MSG* pMsg)
{
	pMsg;
	return FALSE;
}

BOOL CMyMathPainterView::OnIdle()
{
    for (auto & msg : m_message) {
        PostMessage(msg, 0, 0);
    }
    m_message.clear();

    return FALSE;
}


LRESULT CMyMathPainterView::OnCreate(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
    CreateBaseLayer();
    return 0;
}

void CMyMathPainterView::DoPaint(CDCHandle dc)
{
    gm::CClientRect rc(m_hWnd);
    dc.FillSolidRect(&rc, RGB(255, 255, 255));

    gp::Graphics g(dc);
    g.SetSmoothingMode(gp::SmoothingMode::SmoothingModeAntiAlias);

    if (this->is_ready_ == false)
        return;

    CoordSys cs(this);
    m_axis_layer->draw(g, &cs, draw_reason_);
    for (auto & layer : layers) {
        layer.second->draw(g, &cs, draw_reason_);
    }



    m_hlLayer.draw(g, &cs, draw_reason_);

    action_draw_event.call(&g, &cs);
    draw_reason_ = draw_reson::draw_reson_Other;
}

LRESULT CMyMathPainterView::OnMouseEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
    UINT Flags = (UINT)wParam;
    gp::PointF Pt = gp::PointF(float(GET_X_LPARAM(lParam)), float(GET_Y_LPARAM(lParam)));
    auto vpos = this->Screen2View(Pt);
    if (uMsg != WM_MOUSEMOVE)
        SetFocus();

    static vPoint pre_capture_point(DBL_MAX, DBL_MAX);
    static bool pre_capture_status = false;
    //��겶��
    bool bCapture = false;
    for (auto & capture : mouseCaptures) {
        if (capture.expired() == true) 
            continue;
        std::shared_ptr<IMouseCaptureFilter> shared_filter(capture.lock());
        if (true == shared_filter->Filter(vpos)) {
            bCapture = true;
            
            break;
        }
    }

    switch (uMsg)
    {
    case  WM_MOUSEMOVE:
        mouse_move_event.call(vpos.X(), vpos.Y(), Flags);
        break;
    case WM_LBUTTONDOWN:
        mouse_ldown_event.call(vpos.X(), vpos.Y(), Flags);
        break;
    case WM_LBUTTONUP:
        mouse_lup_event.call(vpos.X(), vpos.Y(), Flags);
        break;
    case WM_LBUTTONDBLCLK:
        
        mouse_ldbclick_event.call(vpos.X(), vpos.Y(), Flags);
        break;
    case WM_RBUTTONDOWN:
        mouse_rdown_event.call(vpos.X(), vpos.Y(), Flags);
        break;
    case WM_RBUTTONUP:
        mouse_rup_event.call(vpos.X(), vpos.Y(), Flags);
        break;
    case WM_MBUTTONDOWN:
        mouse_mdown_event.call(vpos.X(), vpos.Y(), Flags);
        break;
    case WM_MBUTTONDBLCLK:
        mouse_mdbclick_event.call(vpos.X(), vpos.Y(), Flags);
        break;
    default:
        break;
    }
    if (pre_capture_status != bCapture ||
        (bCapture == true && pre_capture_point != vpos)
        ) 
    {
        this->refresh();
    }

    pre_capture_status = bCapture;
    if (bCapture) {
        pre_capture_point = vpos;
    }
    return 0;
}

LRESULT CMyMathPainterView::OnKeyBoradEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
    TCHAR vk = (TCHAR)wParam;
    UINT cRepeat = (UINT)lParam & 0xFFFF;
    UINT flags = (UINT)HIWORD(lParam);
    switch (uMsg)
    {
    case  WM_KEYDOWN:
        mouse_keydown_event.call(vk, cRepeat, flags);
        break;
    case WM_KEYUP:
        mouse_keyup_event.call(vk, cRepeat, flags);
        break;
    }
    return 0;
}

void CMyMathPainterView::OnLevelChanged(int before, int current)
{
    draw_reason_ |= draw_reson::draw_reson_Zoom;
    level_changed_event.call(before, current);
}

vPoint CMyMathPainterView::getCenter()
{
    return CenterPoint();
}

void CMyMathPainterView::offset(double x, double y)
{
    view_pt_offset(x, y);
    refresh(draw_reson::draw_reson_Roam);
}


void CMyMathPainterView::SetCenter(const vPoint & vpt)
{
    view_pt_SetCenter(vpt);
    refresh(draw_reson::draw_reson_Roam);
}

void CMyMathPainterView::ZoomDelta(int delta_value)
{
    CoordinateSystem::Zoom(delta_value);
    refresh(draw_reson::draw_reson_Zoom);
}

void CMyMathPainterView::ZoomAbs(int abs_value)
{
    CoordinateSystem::ZoomAbs(abs_value);
    refresh(draw_reson::draw_reson_Zoom);
}

int CMyMathPainterView::getZoomLevel()
{
    return CoordinateSystem::getLevel();
}

LRESULT CMyMathPainterView::OnMouseWheel(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
    CoordinateSystem::OnMouseWheelZoom(uMsg, wParam, lParam, bHandled);
    refresh(draw_reson::draw_reson_Zoom);
    return 0;
}

void CMyMathPainterView::refresh(int reason)
{
    draw_reason_ |= reason;
    Invalidate(FALSE);
}

double CMyMathPainterView::ScreenSize2ViewSize(float size)
{
    return CoordinateSystem::ScreenSize2ViewSize(size);
}

LRESULT CMyMathPainterView::OnSize(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM lParam, BOOL& bHandled)
{
    bHandled = false;
    draw_reason_  |= draw_reson::draw_reson_Size;
    short cur_height = HIWORD(lParam);
    short cur_width = LOWORD(lParam);
    for (auto & layer : layers) {
        layer.second->setSize(cur_width, cur_height);
    }
    m_hlLayer.setSize(cur_width, cur_height);
    return 0;
}

void CMyMathPainterView::FitRectZoom(const vPoint & pt1, const vPoint & pt2)
{
    CoordinateSystem::FitRectZoom(pt1, pt2);
    refresh(draw_reson::draw_reson_Zoom | draw_reson::draw_reson_Roam);
}

void CMyMathPainterView::SelectMany(std::vector<IDrawable *> & elements, bool swap)
{
    std::vector<IDrawable *> vec;
    vec = elements;
    m_hlLayer.SelectMany(elements, swap);
    select_changed_event.call(&vec);
}

void CMyMathPainterView::Select(IDrawable * element)
{
    m_hlLayer.Select(element);
    std::vector<IDrawable *> vec;
    if (element != nullptr) {
        vec.push_back(element);
    }
    select_changed_event.call(&vec);
}

void CMyMathPainterView::UnSelect()
{
    m_hlLayer.UnSelect();
}

void CMyMathPainterView::GetSelect(std::vector<IDrawable *> & elements)
{
    m_hlLayer.GetSelect(elements);
}

void CMyMathPainterView::SpecialHeightLight(IDrawable * ele)
{
    CoordSys cs(this);
    CClientDC dc(m_hWnd);
    m_hlLayer.SpecialHeightLight(ele, &cs, dc.m_hDC);
    refresh();
}

void CMyMathPainterView::Output(output_to where, const wchar_t * msg)
{
    m_wndMain.SendMessage(WM_OUTPUT, where, (LPARAM)msg);
}

void CMyMathPainterView::CreateBaseLayer()
{
    m_axis_layer.reset(new AxisLayer());
}

std::shared_ptr<ILayer> CMyMathPainterView::getActiveLayer()
{
    return active_layer_;
}

void CMyMathPainterView::setActiveLayer(std::shared_ptr<ILayer> layer)
{
    active_layer_ = layer;
}


bool CMyMathPainterView::getCapturePoint(vPoint & pt, float pixel_range)
{
    double range = ScreenSize2ViewSize(pixel_range);
    MinDistPoint mdp(pt, range, &layers);
    if (mdp.exists()) {
        pt = mdp.getPoint();
        return true;
    }
    return false;
}


bool CMyMathPainterView::getRealPoint(vPoint & pt, float pixel_range)
{
    double range = ScreenSize2ViewSize(pixel_range);
    gu::Bound2d bound(pt, range);
    std::vector<IDrawable *> selected;
    for (auto & layer : layers) {
        if (layer.second->visiable == true && layer.second->selectable == true)
            layer.second->Query(bound, selected);
    }
    std::vector<vPoint> candidate_set;
    for (auto & ele : selected) {
        if (ele->getType() == GeomType::Point) {
            candidate_set.push_back(ele->nearestIntrPoint(pt));
        }
    }
    if (candidate_set.size() == 0) {
        return false;
    }
    if (candidate_set.size() == 1) {
        pt = candidate_set.front();
        return true;
    }
    
    MinDistPoint mdp(pt, range, candidate_set);
    if (mdp.exists()) {
        pt = mdp.getPoint();
        return true;
    }
    return false;
}

bool CMyMathPainterView::getIntersectPoint(const vPoint & pt, vPoint * output_pt, float pixel_range /*= 8*/)
{
    double range = ScreenSize2ViewSize(pixel_range);

    gu::Bound2d bound(pt, range);
    std::vector<IDrawable *> selected;

    for (auto & layer : layers) {
        if (layer.second->visiable == true)
            layer.second->Query(bound, selected);
    }
    size_t size = selected.size();
    if (size == 0)
        return false;

    if (size == 1) {
        if (true == selected.front()->IntersectWithAxis(bound, output_pt))
            return true;
        return false;
    }


    for (size_t i = 0; i < size - 1; ++i) {
        for (size_t j = i + 1; j < size; ++j) {
            if (true == selected[i]->IntersectWith(selected[j], bound, output_pt)) {
                return true;
            }
        }
    }
    return false;
}

bool CMyMathPainterView::getSegments(const vPoint & pt, std::vector<Wm5::Segment2d> & segments, float pixel_range /*= 8.0F*/)
{
    double range = ScreenSize2ViewSize(pixel_range);

    gu::Bound2d bound(pt, range);
    std::vector<IDrawable *> selected;

    for (auto & layer : layers) {
        if (layer.second->visiable == true)
            layer.second->Query(bound, selected);
    }
    size_t size = selected.size();
    if (size == 0)
        return false;

    std::vector<Wm5::Segment2d> sub_segs;
    for (auto & element : selected) {
        if (true == element->getSegment(bound, sub_segs)) {
            segments.insert(segments.end(), sub_segs.begin(), sub_segs.end());
        }
        sub_segs.clear();
    }
    if (segments.size() > 0)
        return true;

    return false;
}

void CMyMathPainterView::AddMouseCaptureFilter(std::weak_ptr<IMouseCaptureFilter> filter)
{
    mouseCaptures.push_back(filter);
}

void CMyMathPainterView::RemoveMouseCaptureFilter(std::weak_ptr<IMouseCaptureFilter> filter)
{
    if (filter.expired())
        return;

    std::shared_ptr<IMouseCaptureFilter> shared_filter(filter.lock());
    for (auto iter = mouseCaptures.begin(); iter != mouseCaptures.end(); ) {
        if (iter->expired()) {
            iter = mouseCaptures.erase(iter);
            continue;
        }

        if (iter->lock() == shared_filter) {
            mouseCaptures.erase(iter);
            break;
        }

        ++iter;
    }
}

void CMyMathPainterView::AddCursorFront(HCURSOR cursor)
{
    m_listCursor.push_front(cursor);
}

void CMyMathPainterView::AddCursorBack(HCURSOR cursor)
{
    m_listCursor.push_back(cursor);
}

void CMyMathPainterView::RemoveCursor(HCURSOR cursor)
{
    for (auto iter = m_listCursor.begin(); iter != m_listCursor.end(); ++iter) {
        if (*iter == cursor) {
            m_listCursor.erase(iter);
            return;
        }
    }
}

LRESULT CMyMathPainterView::OnSetCursor(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
    if (m_listCursor.size() == 0) {
        bHandled = false;
        return FALSE;
    }
    ::SetCursor(m_listCursor.front());
    bHandled = TRUE;
    return TRUE;

}

IAxisControl * CMyMathPainterView::getAxisControl()
{
    return m_axis_layer.get();
}
