#pragma once
#include "layer.h"
#include "RTree.h"
#include "geom.h"

class geomLineString : public DrawableBase
{
public:
    geomLineString(const std::vector<vPoint> & pts);
    geomLineString(std::vector<vPoint> & pts, bool swap);
    ~geomLineString();
    virtual vPoint nearestPoint(const vPoint & pt, double range);
    virtual vPoint nearestIntrPoint(vPoint & pt);

    virtual GeomType getType();
    virtual void draw(Gdiplus::Graphics &g, CoordSys * pConv, IDrawStyle * style);
    virtual void high_light(Gdiplus::Graphics &g, CoordSys * pConv, IDrawStyle * style);
    virtual bool hit_test(const RtreeBound & bound);
    virtual bool getBound(RtreeBound & bound);
    virtual bool getSegment(RtreeBound & bound, std::vector<Wm5::Segment2d> & segments);
    virtual bool dump_info(CString & info);
private:
    void draw_inner(Gdiplus::Graphics &g, CoordSys * pConv, IDrawStyle * style, bool is_draw_points = false);
    void InitPoints();
    std::vector<vPoint> pts_;
    RTree<int, double, 2> rtree_;
    gu::Bound2d bound_;
};

