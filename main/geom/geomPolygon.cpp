#include "stdafx.h"
#include <sstream>
#include "geomPolygon.h"
#include "Wm5IntrSegment2Box2.h"
#include "Wm5ContPointInPolygon2.h"

geomPolygon::geomPolygon(const std::vector<vPoint> & pts) :pts_(pts)
{
    InitPoints();
}

geomPolygon::geomPolygon(std::vector<vPoint> & pts, bool swap)
{
    if (swap) {
        pts_.swap(pts);
    }
    else {
        pts_ = pts;
    }
    InitPoints();

}

void geomPolygon::InitPoints()
{
    int size = pts_.size() - 1;
    for (int i = 0; i < size; ++i) {
        gu::Bound2d bb(pts_[i], pts_[i + 1]);
        rtree_.Insert(bb.boundMin, bb.boundMax, i);
        bound_.extend(pts_[i]);
    }
    bound_.extend(pts_[size]);
}

geomPolygon::~geomPolygon()
{
}

void geomPolygon::draw(Gdiplus::Graphics &g, CoordSys * pConv, IDrawStyle * style)
{
    std::vector<gp::PointF> spts;
    size_t pt_count = pts_.size();
    spts.reserve(pt_count);
    gp::PointF pre_point;
    double avg_len = 0;
    for (size_t i = 0; i < pt_count; ++i) {
        auto spt = pConv->conv(pts_[i]);
        if (i != 0) {
            avg_len += max(abs(pre_point.X - spt.X), abs(pre_point.Y - spt.Y));
        }
        pre_point = spt;
        spts.push_back(spt);
    }
    avg_len /= pt_count;

    if (avg_len < 10) {
        //�õ�������
        g.SetInterpolationMode(gp::InterpolationMode::InterpolationModeInvalid);
        g.SetCompositingQuality(gp::CompositingQuality::CompositingQualityInvalid);
        g.SetSmoothingMode(gp::SmoothingMode::SmoothingModeInvalid);
        g.DrawPolygon(style->getPen(), &spts.front(), spts.size());
    }
    else {
        g.SetInterpolationMode(gp::InterpolationMode::InterpolationModeHighQuality);
        g.SetCompositingQuality(gp::CompositingQuality::CompositingQualityHighQuality);
        g.SetSmoothingMode(gp::SmoothingMode::SmoothingModeAntiAlias);

        g.DrawPolygon(style->getPen(), &spts.front(), spts.size());
    }
}

bool geomPolygon::hit_test(const RtreeBound & bound)
{
    Wm5::PointInPolygon2d polygon(pts_.size(), &pts_.front());
    vPoint bound_pt1(bound.boundMin[0], bound.boundMin[1]);
    vPoint bound_pt2(bound.boundMin[0], bound.boundMax[1]);
    vPoint bound_pt3(bound.boundMax[0], bound.boundMin[1]);
    vPoint bound_pt4(bound.boundMax[0], bound.boundMax[1]);
    for (auto & pt : { &bound_pt1, &bound_pt2, &bound_pt3, &bound_pt4 }) {
        if (true == polygon.Contains(*pt))
            return true;
    }

    Wm5::Box2d bound_box;
    gu::CreateBoxFromBound(bound_box, bound);
    bool found = false;
    auto input_args = std::make_tuple(&bound, this, &bound_box, &found);
    using input_args_type = decltype(input_args);
    rtree_.Search(bound.boundMin, bound.boundMax, [](int index, void * args)
    {
        input_args_type * pInput = static_cast<input_args_type *>(args);
        auto & query_bound = *std::get<0>(*pInput);
        auto pThis = std::get<1>(*pInput);
        auto &bound_box = *std::get<2>(*pInput);
        auto &found = *std::get<3>(*pInput);

        Wm5::Segment2d seg(pThis->pts_[index], pThis->pts_[index + 1]);
        Wm5::IntrSegment2Box2d intr(seg, bound_box, false);
        if (intr.Test() == true)
        {
            found = true;
            return false;   //��ֹrtree������
        }
        return true;
    }, &input_args);
    return found;
}

bool geomPolygon::getBound(RtreeBound & bound)
{
    bound = bound_;
    return true;
}

GeomType geomPolygon::getType()
{
    return GeomType::Polygon;
}

vPoint geomPolygon::nearestIntrPoint(vPoint & pt)
{
    return vPoint(0, 0);
}

vPoint geomPolygon::nearestPoint(const vPoint & pt, double range)
{
    return vPoint(0, 0);
}

void geomPolygon::high_light(Gdiplus::Graphics &g, CoordSys * pConv, IDrawStyle * style)
{
    std::vector<gp::PointF> spts;
    pConv->conv(pts_.begin(), pts_.end(), std::back_inserter(spts));
    g.FillPolygon(style->getBrush(), &spts.front(), spts.size());
}

bool geomPolygon::getSegment(RtreeBound & bound, std::vector<Wm5::Segment2d> & segments)
{
    Wm5::Box2d bound_box;
    gu::CreateBoxFromBound(bound_box, bound);
    std::vector<Wm5::Segment2d> result;
    auto input_args = std::make_tuple(&bound, this, &bound_box, &result);
    using input_args_type = decltype(input_args);
    rtree_.Search(bound.boundMin, bound.boundMax, [](int index, void * args)
    {
        input_args_type * pInput = static_cast<input_args_type *>(args);
        auto & query_bound = *std::get<0>(*pInput);
        auto pThis = std::get<1>(*pInput);
        auto &bound_box = *std::get<2>(*pInput);
        std::vector<Wm5::Segment2d> & segs_result = *std::get<3>(*pInput);

        Wm5::Segment2d seg(pThis->pts_[index], pThis->pts_[index + 1]);
        Wm5::IntrSegment2Box2d intr(seg, bound_box, false);
        if (intr.Test() == true)
        {
            segs_result.push_back(seg);
            return true;
        }
        return true;
    }, &input_args);
    if (result.size() > 0) {
        segments.swap(result);
        return true;
    }

    return false;
}

bool geomPolygon::dump_info(CString & info)
{
    std::wostringstream ss;
    ss.precision(12);
    ss << "POLYGON((";
    for (auto & pt : pts_) {
        ss << pt.X() << " " << pt.Y() << " ,";
    }
    ss.seekp(-1, ss.cur);
    ss << "))";
    info = ss.str().c_str();
    return true;
}


