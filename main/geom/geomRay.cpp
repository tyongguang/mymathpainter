#include "stdafx.h"
#include "geomRay.h"
#include "geom.h"
#include "Wm5ContBox2.h"
#include "guimisc.h"
#include "Wm5DistPoint2Ray2.h"
#include "Wm5IntrRay2Box2.h"

geomRay::geomRay(const vPoint & pt, const vPoint & dir) :ray_(pt, dir)
{

}

geomRay::~geomRay()
{
}

void geomRay::draw(Gdiplus::Graphics &g, CoordSys * pConv, IDrawStyle * style)
{
    RtreeBound bound;
    bound.boundMin[0] = pConv->base_conv->vleft();
    bound.boundMin[1] = pConv->base_conv->vbottom();

    bound.boundMax[0] = pConv->base_conv->vright();
    bound.boundMax[1] = pConv->base_conv->vtop();

    Wm5::Box2d bound_box;
    gu::CreateBoxFromBound(bound_box, bound);
    Wm5::IntrRay2Box2d intr(ray_, bound_box);
    if (intr.Find() == false)
        return;

    g.SetInterpolationMode(gp::InterpolationMode::InterpolationModeHighQuality);
    g.SetCompositingQuality(gp::CompositingQuality::CompositingQualityHighQuality);
    g.SetSmoothingMode(gp::SmoothingMode::SmoothingModeAntiAlias);

    auto intersection_type = intr.GetIntersectionType();

    if (Wm5::Intersector<double, Wm5::Vector2d>::IT_SEGMENT == intersection_type) {
        g.DrawLine(style->getPen(), pConv->conv(intr.GetPoint(0)), pConv->conv(intr.GetPoint(1)));
        return;
    }

    if (Wm5::Intersector<double, Wm5::Vector2d>::IT_POINT == intersection_type) {
        g.DrawLine(style->getPen(), pConv->conv(ray_.Origin), pConv->conv(intr.GetPoint(0)));
        return;
    }

}

bool geomRay::hit_test(const RtreeBound & bound)
{
    Wm5::Box2d bound_box;
    gu::CreateBoxFromBound(bound_box, bound);
    return Wm5::IntrRay2Box2d(ray_, bound_box).Test();
}

bool geomRay::getBound(RtreeBound & bound)
{
    return false;
}

GeomType geomRay::getType()
{
    return GeomType::Ray;
}

vPoint geomRay::nearestIntrPoint(vPoint & pt)
{
    return vPoint(DBL_MAX, DBL_MAX);
}

vPoint geomRay::nearestPoint(const vPoint & pt, double range)
{
    Wm5::DistPoint2Ray2d dist(pt, ray_);
    double distance = dist.Get();
    auto target_point = dist.GetPoint();
    return target_point;
}

void geomRay::high_light(Gdiplus::Graphics &g, CoordSys * pConv, IDrawStyle * style)
{
    return draw(g, pConv, style);
}

bool geomRay::getSegment(RtreeBound & bound, std::vector<Wm5::Segment2d> & segments)
{
    Wm5::Box2d bound_box;
    gu::CreateBoxFromBound(bound_box, bound);
    Wm5::IntrRay2Box2d intr(ray_, bound_box);
    if (intr.Find() == false)
        return false;

    auto intersection_type = intr.GetIntersectionType();

    if (Wm5::Intersector<double, Wm5::Vector2d>::IT_SEGMENT == intersection_type) {
        segments.push_back(Wm5::Segment2d(intr.GetPoint(0), intr.GetPoint(1)));
        return true;
    }

    if (Wm5::Intersector<double, Wm5::Vector2d>::IT_POINT == intersection_type) {
        segments.push_back(Wm5::Segment2d(ray_.Origin, intr.GetPoint(0)));
        return true;
    }
    return false;
}

bool geomRay::dump_info(CString & info)
{
    info.Format(L"RAY(%lf %lf, %lf %lf)", ray_.Origin.X(), ray_.Origin.Y(),
        ray_.Direction.X(),
        ray_.Direction.Y());
    return true;
}
