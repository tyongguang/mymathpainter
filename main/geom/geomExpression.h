#pragma once
#include "layer.h"
#include "Wm5Ray2.h"
class geomExpression : public DrawableBase
{
public:
    geomExpression(std::function<bool (double, double *)> fn,  LPCTSTR fn_str = nullptr);
    ~geomExpression();
    virtual vPoint nearestPoint(const vPoint & pt, double range);
    virtual vPoint nearestIntrPoint(vPoint & pt);

    virtual GeomType getType();
    virtual void draw(Gdiplus::Graphics &g, CoordSys * pConv, IDrawStyle * style);
    virtual void high_light(Gdiplus::Graphics &g, CoordSys * pConv, IDrawStyle * style);
    virtual bool hit_test(const RtreeBound & bound);
    virtual bool getBound(RtreeBound & bound);
    virtual bool getSegment(RtreeBound & bound, std::vector<Wm5::Segment2d> & segments);
    virtual bool dump_info(CString & info);
private:
    void draw_segment(Gdiplus::Graphics &g, CoordSys * pConv, IDrawStyle * style, std::vector<gp::PointF> & spts);
    void add_segment(std::vector<Wm5::Segment2d> & target_result, std::vector<vPoint> & vpts);
    std::function<bool(double, double *)> fn_;
    CString str_exp_;
};

