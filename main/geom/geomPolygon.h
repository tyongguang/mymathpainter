#pragma once
#include "layer.h"
#include "RTree.h"
#include "geom.h"


class geomPolygon : public DrawableBase
{
public:
    geomPolygon(const std::vector<vPoint> & pts);
    geomPolygon(std::vector<vPoint> & pts, bool swap);
    ~geomPolygon();
    virtual vPoint nearestPoint(const vPoint & pt, double range);
    virtual vPoint nearestIntrPoint(vPoint & pt);

    virtual GeomType getType();
    virtual void draw(Gdiplus::Graphics &g, CoordSys * pConv, IDrawStyle * style);
    virtual void high_light(Gdiplus::Graphics &g, CoordSys * pConv, IDrawStyle * style);
    virtual bool hit_test(const RtreeBound & bound);
    virtual bool getBound(RtreeBound & bound);
    virtual bool getSegment(RtreeBound & bound, std::vector<Wm5::Segment2d> & segments);
    virtual bool dump_info(CString & info);
private:
    std::vector<vPoint> pts_;
    RTree<int, double, 2> rtree_;
    gu::Bound2d bound_;
    void InitPoints();
};

