#pragma once
#include "layer.h"
#include "Wm5Ray2.h"
class geomRay : public DrawableBase
{
public:
    geomRay(const vPoint & pt, const vPoint & dir);
    ~geomRay();
    virtual vPoint nearestPoint(const vPoint & pt, double range);
    virtual vPoint nearestIntrPoint(vPoint & pt);

    virtual GeomType getType();
    virtual void draw(Gdiplus::Graphics &g, CoordSys * pConv, IDrawStyle * style);
    virtual void high_light(Gdiplus::Graphics &g, CoordSys * pConv, IDrawStyle * style);
    virtual bool hit_test(const RtreeBound & bound);
    virtual bool getBound(RtreeBound & bound);
    virtual bool getSegment(RtreeBound & bound, std::vector<Wm5::Segment2d> & segments);
    virtual bool dump_info(CString & info);
private:
    Wm5::Ray2d ray_;
};

