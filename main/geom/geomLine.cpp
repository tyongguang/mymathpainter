#include "stdafx.h"
#include "geomLine.h"
#include "geom.h"
#include "Wm5ContBox2.h"
#include "guimisc.h"
#include "Wm5DistPoint2Line2.h"
#include "Wm5IntrLine2Box2.h"

geomLine::geomLine(const vPoint & pt, const vPoint & dir) :line_(pt, dir)
{

}

geomLine::~geomLine()
{
}

void geomLine::draw(Gdiplus::Graphics &g, CoordSys * pConv, IDrawStyle * style)
{
    RtreeBound bound;
    bound.boundMin[0] = pConv->base_conv->vleft();
    bound.boundMin[1] = pConv->base_conv->vbottom();

    bound.boundMax[0] = pConv->base_conv->vright();
    bound.boundMax[1] = pConv->base_conv->vtop();

    Wm5::Box2d bound_box;
    gu::CreateBoxFromBound(bound_box, bound);
    Wm5::IntrLine2Box2d intr(line_, bound_box);
    if (intr.Find() == false)
        return;

    if (Wm5::Intersector<double, Wm5::Vector2d>::IT_SEGMENT != intr.GetIntersectionType()) {
        return;
    }
    g.SetInterpolationMode(gp::InterpolationMode::InterpolationModeHighQuality);
    g.SetCompositingQuality(gp::CompositingQuality::CompositingQualityHighQuality);
    g.SetSmoothingMode(gp::SmoothingMode::SmoothingModeAntiAlias);
    g.DrawLine(style->getPen(), pConv->conv(intr.GetPoint(0)), pConv->conv(intr.GetPoint(1)));

}

bool geomLine::hit_test(const RtreeBound & bound)
{
    Wm5::Box2d bound_box;
    gu::CreateBoxFromBound(bound_box, bound);
    return Wm5::IntrLine2Box2d(line_, bound_box).Test();
}

bool geomLine::getBound(RtreeBound & bound)
{
    //gu::CreateBound(bound, pt_);
    return false;
}

GeomType geomLine::getType()
{
    return GeomType::Line;
}

vPoint geomLine::nearestIntrPoint(vPoint & pt)
{
    return vPoint(DBL_MAX, DBL_MAX);
}

vPoint geomLine::nearestPoint(const vPoint & pt, double range)
{
    Wm5::DistPoint2Line2d dist(pt, line_);
    double distance = dist.Get();
    auto target_point = dist.GetPoint();
    return target_point;
}

void geomLine::high_light(Gdiplus::Graphics &g, CoordSys * pConv, IDrawStyle * style)
{
    return draw(g, pConv, style);
}

bool geomLine::getSegment(RtreeBound & bound, std::vector<Wm5::Segment2d> & segments)
{
    Wm5::Box2d bound_box;
    gu::CreateBoxFromBound(bound_box, bound);
    Wm5::IntrLine2Box2d intr(line_, bound_box);
    if (intr.Find() == false)
        return false;

    if (Wm5::Intersector<double, Wm5::Vector2d>::IT_SEGMENT != intr.GetIntersectionType()) {
        return false;
    }
    segments.push_back(Wm5::Segment2d(intr.GetPoint(0), intr.GetPoint(1)));
    return true;

}

bool geomLine::dump_info(CString & info)
{
    info.Format(L"RAY(%lf %lf, %lf %lf)", line_.Origin.X(), line_.Origin.Y(),
        line_.Direction.X(),
        line_.Direction.Y());
    return true;
}
