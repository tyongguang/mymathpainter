#include "stdafx.h"
#include "geomPoint.h"
#include "geom.h"
#include "Wm5ContBox2.h"
#include "guimisc.h"
#include "Wm5DistPoint2Segment2.h"

geomPoint::geomPoint(const vPoint & pt) :pt_(pt)
{

}

geomPoint::~geomPoint()
{
}

void geomPoint::draw(Gdiplus::Graphics &g, CoordSys * pConv, IDrawStyle * style)
{
    auto width = style->getPointRenderWidth();
    auto spt = pConv->conv(pt_);
    g.FillEllipse(style->getBrush(), gm::CenterPointSquare(spt, (float)width));
}

bool geomPoint::hit_test(const RtreeBound & bound)
{
    Wm5::Box2d bound_box;
    gu::CreateBoxFromBound(bound_box, bound);
    return Wm5::InBox(pt_, bound_box);
}

bool geomPoint::getBound(RtreeBound & bound)
{
    gu::CreateBound(bound, pt_);
    return true;
}

GeomType geomPoint::getType()
{
    return GeomType::Point;
}

vPoint geomPoint::nearestIntrPoint(vPoint & pt)
{
    return pt_;
}

vPoint geomPoint::nearestPoint(const vPoint & pt, double range)
{
    return pt_;
}

void geomPoint::high_light(Gdiplus::Graphics &g, CoordSys * pConv, IDrawStyle * style)
{
    return draw(g, pConv, style);
}

bool geomPoint::dump_info(CString & info)
{
    info.Format(L"POINT(%lf %lf)", pt_.X(), pt_.Y());
    return true;
}
