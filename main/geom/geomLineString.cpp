#include "stdafx.h"
#include <sstream>
#include "geomLineString.h"
#include "Wm5IntrSegment2Box2.h"
#include "guimisc.h"


geomLineString::geomLineString(const std::vector<vPoint> & pts) :pts_(pts)
{
    InitPoints();
}

geomLineString::geomLineString(std::vector<vPoint> & pts, bool swap)
{
    if (swap == true) {
        pts_.swap(pts);
    }
    else {
        pts_ = pts;
    }
    InitPoints();
}


void geomLineString::InitPoints()
{
    int size = pts_.size() - 1;
    for (int i = 0; i < size; ++i) {
        gu::Bound2d bb(pts_[i], pts_[i + 1]);
        rtree_.Insert(bb.boundMin, bb.boundMax, i);
        bound_.extend(pts_[i]);
    }
    bound_.extend(pts_[size]);
}


geomLineString::~geomLineString()
{
}

void geomLineString::draw_inner(Gdiplus::Graphics &g, CoordSys * pConv, IDrawStyle * style, bool is_draw_points /*= false*/)
{
    std::vector<gp::PointF> spts;
    size_t pt_count = pts_.size();
    spts.reserve(pt_count);
    gp::PointF pre_point;
    double avg_len = 0;
    for (size_t i = 0; i < pt_count; ++i) {
        auto spt = pConv->conv(pts_[i]);
        if (i != 0) {
            avg_len += max(abs(pre_point.X - spt.X), abs(pre_point.Y - spt.Y));
        }
        pre_point = spt;
        spts.push_back(spt);
    }
    avg_len /= pt_count;

    if (avg_len < 10) {
        //用低质量画
        g.SetInterpolationMode(gp::InterpolationMode::InterpolationModeInvalid);
        g.SetCompositingQuality(gp::CompositingQuality::CompositingQualityInvalid);
        g.SetSmoothingMode(gp::SmoothingMode::SmoothingModeInvalid);

        if (avg_len < 0.2) {
            spts[1] = spts.back();
            g.DrawLines(style->getPen(), &spts.front(), 2);
            return;
        }
        g.DrawLines(style->getPen(), &spts.front(), spts.size());
    }
    else {
        g.SetInterpolationMode(gp::InterpolationMode::InterpolationModeHighQuality);
        g.SetCompositingQuality(gp::CompositingQuality::CompositingQualityHighQuality);
        g.SetSmoothingMode(gp::SmoothingMode::SmoothingModeAntiAlias);

        g.DrawLines(style->getPen(), &spts.front(), spts.size());

        //只有在高质量的时候才考虑画点
        if (is_draw_points == true) {
            auto width = style->getPointRenderWidth();
            for (auto & spt : spts) {
                g.FillEllipse(style->getBrush(), gm::CenterPointSquare(spt, (float)width));
            }
        }
    }

}

void geomLineString::draw(Gdiplus::Graphics &g, CoordSys * pConv, IDrawStyle * style)
{
    draw_inner(g, pConv, style, false);
}

bool geomLineString::hit_test(const RtreeBound & bound)
{
    Wm5::Box2d bound_box;
    gu::CreateBoxFromBound(bound_box, bound);
    bool found = false;
    auto input_args = std::make_tuple(&bound, this, &bound_box, &found);
    using input_args_type = decltype(input_args);
    rtree_.Search(bound.boundMin, bound.boundMax, [](int index, void * args)
    {
        input_args_type * pInput = static_cast<input_args_type *>(args);
        auto & query_bound = *std::get<0>(*pInput);
        auto pThis = std::get<1>(*pInput);
        auto &bound_box = *std::get<2>(*pInput);
        auto &found = *std::get<3>(*pInput);

        Wm5::Segment2d seg(pThis->pts_[index], pThis->pts_[index + 1]);
        Wm5::IntrSegment2Box2d intr(seg, bound_box, false);
        if (intr.Test() == true)
        {
            found = true;
            return false;   //终止rtree的搜索
        }
        return true;
    }, &input_args);
    return found;
}

bool geomLineString::getBound(RtreeBound & bound)
{
    bound = bound_;
    return true;
}

GeomType geomLineString::getType()
{
    return GeomType::LineSting;
}

vPoint geomLineString::nearestIntrPoint(vPoint & pt)
{
    //暂未实现
    return vPoint(0, 0);
}

vPoint geomLineString::nearestPoint(const vPoint & pt, double range)
{
    gu::Bound2d bb(pt, range);
    std::set<int> indexs;
    rtree_.Search(bb.boundMin, bb.boundMax, [](int index, void * args)
    {
        std::set<int> * index_set = static_cast<std::set<int> *>(args);
        index_set->insert(index);
        index_set->insert(index + 1);
        return true;
    }, &indexs);

    if (indexs.size() == 0)
        return vPoint(DBL_MAX, DBL_MAX);

    auto & iter = std::min_element(indexs.begin(), indexs.end(), [=](int i, int j) {
        return (pts_[i] - pt).SquaredLength() < (pts_[j] - pt).SquaredLength();
    });
    return pts_[*iter];
}

void geomLineString::high_light(Gdiplus::Graphics &g, CoordSys * pConv, IDrawStyle * style)
{
    draw_inner(g, pConv, style, true);
}

bool geomLineString::getSegment(RtreeBound & bound, std::vector<Wm5::Segment2d> & segments)
{
    Wm5::Box2d bound_box;
    gu::CreateBoxFromBound(bound_box, bound);
    std::vector<Wm5::Segment2d> result;
    auto input_args = std::make_tuple(&bound, this, &bound_box, &result);
    using input_args_type = decltype(input_args);
    rtree_.Search(bound.boundMin, bound.boundMax, [](int index, void * args)
    {
        input_args_type * pInput = static_cast<input_args_type *>(args);
        auto & query_bound = *std::get<0>(*pInput);
        auto pThis = std::get<1>(*pInput);
        auto &bound_box = *std::get<2>(*pInput);
        std::vector<Wm5::Segment2d> & segs_result = *std::get<3>(*pInput);

        Wm5::Segment2d seg(pThis->pts_[index], pThis->pts_[index + 1]);
        Wm5::IntrSegment2Box2d intr(seg, bound_box, false);
        if (intr.Test() == true)
        {
            segs_result.push_back(seg);
            return true;
        }
        return true;
    }, &input_args);
    if (result.size() > 0) {
        segments.swap(result);
        return true;
    }

    return false;
}

bool geomLineString::dump_info(CString & info)
{
    std::wostringstream ss;
    ss.precision(12);
    ss << "LINESTRING(";
    for (auto & pt : pts_) {
        ss << pt.X() << " " << pt.Y() << " ,";
    }
    ss.seekp(-1, ss.cur);
    ss << ")";
    info = ss.str().c_str();
    return true;
}
