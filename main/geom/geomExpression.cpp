#include "stdafx.h"
#include "geomExpression.h"
#include "geom.h"
#include "Wm5ContBox2.h"
#include "guimisc.h"
#include "Wm5DistPoint2Ray2.h"
#include "Wm5IntrRay2Box2.h"

geomExpression::geomExpression(std::function<bool(double, double *)> fn, LPCTSTR fn_str) :fn_(fn)
{
    if (nullptr != fn_str)
        str_exp_ = fn_str;
}

geomExpression::~geomExpression()
{
}

void geomExpression::draw_segment(Gdiplus::Graphics &g, CoordSys * pConv, IDrawStyle * style, std::vector<gp::PointF> & spts) {
    if (spts.size() >= 2) {
        //std::vector<gp::PointF> render_line;
        //pConv->conv(vpts.begin(), vpts.end(), std::back_inserter(render_line));

        g.DrawLines(style->getPen(), &spts.front(), spts.size());
    }
}
void geomExpression::draw(Gdiplus::Graphics &g, CoordSys * pConv, IDrawStyle * style)
{
    RtreeBound bound;
    bound.boundMin[0] = pConv->base_conv->vleft();
    bound.boundMin[1] = pConv->base_conv->vbottom();

    bound.boundMax[0] = pConv->base_conv->vright();
    bound.boundMax[1] = pConv->base_conv->vtop();
 
    g.SetInterpolationMode(gp::InterpolationMode::InterpolationModeHighQuality);
    g.SetCompositingQuality(gp::CompositingQuality::CompositingQualityHighQuality);
    g.SetSmoothingMode(gp::SmoothingMode::SmoothingModeAntiAlias);

    std::vector<gp::PointF> spts;
    double step = (bound.boundMax[0] - bound.boundMin[0]) / 1000;
    for (double i = bound.boundMin[0]; i < bound.boundMax[0]; i += step) {
        double y = DBL_MAX;
        if (false == fn_(i, &y) ) {
            draw_segment(g, pConv, style, spts);
            spts.clear();
            continue;
        }
        auto spt = pConv->conv(vPoint(i, y));
        if (fabs(spt.Y)  > pConv->base_conv->sheight() * 2) {
            draw_segment(g, pConv, style, spts);
            spts.clear();
            continue;
        }
        spts.push_back(spt);
    }
    draw_segment(g, pConv, style, spts);
}

bool geomExpression::hit_test(const RtreeBound & bound)
{
    double step = (bound.boundMax[0] - bound.boundMin[0]) / 1000;
    for (double i = bound.boundMin[0]; i < bound.boundMax[0]; i += step) {
        double y = DBL_MAX;
        if (true == fn_(i, &y) && y > bound.boundMin[1] && y < bound.boundMax[1]) {
            return true;
        }
    }
    return false;
}

bool geomExpression::getBound(RtreeBound & bound)
{
    return false;
}

GeomType geomExpression::getType()
{
    return GeomType::Expression;
}

vPoint geomExpression::nearestIntrPoint(vPoint & pt)
{
    return vPoint(DBL_MAX, DBL_MAX);
}

vPoint geomExpression::nearestPoint(const vPoint & pt, double range)
{
    return vPoint(DBL_MAX, DBL_MAX);
//     Wm5::DistPoint2Ray2d dist(pt, ray_);
//     double distance = dist.Get();
//     auto target_point = dist.GetPoint();
//     return target_point;
}

void geomExpression::high_light(Gdiplus::Graphics &g, CoordSys * pConv, IDrawStyle * style)
{
    return draw(g, pConv, style);
}

bool geomExpression::getSegment(RtreeBound & bound, std::vector<Wm5::Segment2d> & segments)
{
    std::vector<Wm5::Segment2d> seg_result;
    std::vector<vPoint> vpts;

    double step = (bound.boundMax[0] - bound.boundMin[0]) / 100;
    for (double i = bound.boundMin[0]; i < bound.boundMax[0]; i += step) {
        double y = DBL_MAX;
        if (false == fn_(i, &y) || y < bound.boundMin[1] || y > bound.boundMax[1]) {
            add_segment(seg_result, vpts);
            vpts.clear();
            continue;
        }

        vpts.emplace_back(i, y);
    }
    add_segment(seg_result, vpts);
    if (seg_result.size() == 0)
        return false;

    seg_result.swap(segments);
    return true;
}

void geomExpression::add_segment(std::vector<Wm5::Segment2d> & target_result, std::vector<vPoint> & vpts)
{
    size_t size = vpts.size();
    if (size < 2)
        return;

    size -= 1;
    for (size_t i = 0; i < size; ++i) {
        target_result.push_back(Wm5::Segment2d(vpts[i], vpts[i + 1]));
    }
}

bool geomExpression::dump_info(CString & info)
{
    if (str_exp_.IsEmpty() == true)
        return false;
    info = L"����ʽ��" + str_exp_;
    return true;
}
