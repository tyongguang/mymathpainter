#pragma once

#include "layer.h"
#include "Wm5Box2.h"
#define _USE_MATH_DEFINES
#include <math.h>

//gu ==> geometry util
namespace gu { 
    inline double Radian2Angle(double radian) {
        return radian * 180 / M_PI;
    }

    inline double Angle2Radian(double angle) {
        return angle * M_PI / 180;
    }
    void CreateBound(RtreeBound & output, const vPoint & pt1, double buffer = 0);
    void CreateBound(RtreeBound & output, const vPoint & pt1, const vPoint & pt2);
    void CreateBound(RtreeBound & output, const Wm5::Box2d & box);
    void CreateBoxFromBound(Wm5::Box2d & box, const RtreeBound & input);
    class Bound2d : public RtreeBound
    {
    public:
        Bound2d();
        Bound2d(const vPoint & pt1, double buffer = 0);
        Bound2d(const vPoint & pt1, const vPoint & pt2);
        Bound2d(const Wm5::Box2d & box);

        void reset(const vPoint & pt1, double buffer = 0);
        void reset(const vPoint & pt1, const vPoint & pt2);
        void reset(const Wm5::Box2d & box);
        void extend(const vPoint & pt);
    };
    
}