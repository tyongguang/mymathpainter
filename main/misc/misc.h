#pragma once

namespace misc {
    CString format_double(double d);
    CString format_point(double x, double y);
    bool Copy(LPCTSTR szData, HWND hWnd);

    BOOL LoadPNGFromRes(HINSTANCE hResIntance, UINT nResourceID, Gdiplus::Bitmap ** ppBitmap, LPCTSTR szType = _T("PNG"));

    COLORREF PixelAlpha(COLORREF src, COLORREF dst, int percent);
}