#include "stdafx.h"
#include "geom.h"
#define  NOMINMAX
#include <algorithm>

namespace gu {
    Bound2d::Bound2d()
    {
        boundMin[0] = DBL_MAX;
        boundMin[1] = DBL_MAX;

        boundMax[0] = DBL_MIN;
        boundMax[1] = DBL_MIN;
    }

    void Bound2d::extend(const vPoint & pt)
    {
        if (pt.X() < boundMin[0]) {
            boundMin[0] = pt.X();
        }
        if (pt.X() > boundMax[0]) {
            boundMax[0] = pt.X();
        }

        if (pt.Y() < boundMin[1]) {
            boundMin[1] = pt.Y();
        }
        if (pt.Y() > boundMax[1]) {
            boundMax[1] = pt.Y();
        }
    }

    Bound2d::Bound2d(const vPoint & pt1, double buffer /*= 0*/)
    {
        CreateBound(*this, pt1, buffer);
    }

    Bound2d::Bound2d(const vPoint & pt1, const vPoint & pt2)
    {
        CreateBound(*this, pt1, pt2);

    }

    Bound2d::Bound2d(const Wm5::Box2d & box)
    {
        CreateBound(*this, box);
    }

    void Bound2d::reset(const vPoint & pt1, double buffer /*= 0*/)
    {
        CreateBound(*this, pt1, buffer);
    }

    void Bound2d::reset(const vPoint & pt1, const vPoint & pt2)
    {
        CreateBound(*this, pt1, pt2);
    }

    void Bound2d::reset(const Wm5::Box2d & box)
    {
        CreateBound(*this, box);
    }

    void CreateBound(RtreeBound & output, const vPoint & pt1, double buffer /*= 0*/)
    {
        output.boundMin[0] = pt1.X() - buffer;
        output.boundMin[1] = pt1.Y() - buffer;

        output.boundMax[0] = pt1.X() + buffer;
        output.boundMax[1] = pt1.Y() + buffer;
    }

    void CreateBound(RtreeBound & output, const vPoint & pt1, const vPoint & pt2)
    {
        output.boundMin[0] = min(pt1.X(), pt2.X());
        output.boundMin[1] = min(pt1.Y(), pt2.Y());

        output.boundMax[0] = max(pt1.X(), pt2.X());
        output.boundMax[1] = max(pt1.Y(), pt2.Y());
    }

    void CreateBound(RtreeBound & output, const Wm5::Box2d & box)
    {
        output.boundMin[0] = box.Center.X() - box.Extent[0];
        output.boundMin[1] = box.Center.Y() - box.Extent[1];
        
        output.boundMax[0] = box.Center.X() + box.Extent[0];
        output.boundMax[1] = box.Center.Y() + box.Extent[1];
    }

    void CreateBoxFromBound(Wm5::Box2d & box, const RtreeBound & input)
    {
        box.Center = vPoint(
            (input.boundMin[0] + input.boundMax[0]) /2,
            (input.boundMin[1] + input.boundMax[1]) / 2);
        box.Extent[0] = (input.boundMax[0] - input.boundMin[0]) / 2;
        box.Extent[1] = (input.boundMax[1] - input.boundMin[1]) / 2;
        box.Axis[0].X() = 1;
        box.Axis[0].Y() = 0;

        box.Axis[1].X() = 0;
        box.Axis[1].Y() = 1;
    }

}