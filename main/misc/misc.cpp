#include "stdafx.h"
#include "misc.h"

namespace misc {
    CString format_double(double d) {
        CString str;
        if (fabs(d) >= 1e7) {
            str.Format(L"%.1lf", d);
        }
        if (fabs(d) >= 1e6) {
            str.Format(L"%.2lf", d);
        }
        else if (fabs(d) >= 1e5) {
            str.Format(L"%.3lf", d);
        }
        else if (fabs(d) >= 1e4) {
            str.Format(L"%.4lf", d);
        }
        else if (fabs(d) >= 1e3) {
            str.Format(L"%.5lf", d);
        }
        else if (fabs(d) >= 1e2) {
            str.Format(L"%.6lf", d);
        }
        else {
            str.Format(L"%.7lf", d);
        }
        return str;
    }

    CString format_point(double x, double y)
    {
        CString str;
        str.Append(format_double(x));
        str.Append(L", ");
        str.Append(format_double(y));
        return str;
    }

    bool Copy(LPCTSTR szData, HWND hWnd)
    {
        wchar_t * hMem;
        CStringW strData;

        HGLOBAL globalMem = ::GlobalAlloc(GMEM_DDESHARE, 2 * (lstrlen(szData) + 1));
        hMem = (wchar_t *)::GlobalLock(globalMem);
        lstrcpy(hMem, szData);
        ::GlobalUnlock(hMem);
        if (!::OpenClipboard(hWnd))
            return false;
        ::EmptyClipboard();
        SetClipboardData(CF_UNICODETEXT, hMem);
        ::CloseClipboard();
        return true;
    }

    BOOL LoadPNGFromRes(HINSTANCE hResIntance, UINT nResourceID, Gdiplus::Bitmap ** ppBitmap, LPCTSTR szType /*= _T("PNG")*/)
    {
        if (NULL == hResIntance)
            return FALSE;

        using namespace Gdiplus;
        HRSRC hRsrc = ::FindResource(hResIntance, MAKEINTRESOURCE(nResourceID), szType);
        if (!hRsrc)
        {
            return FALSE;
        }

        DWORD dwLen = SizeofResource(hResIntance, hRsrc);
        BYTE* lpRsrc = (BYTE*)LoadResource(hResIntance, hRsrc);
        if (!lpRsrc)
        {
            return FALSE;
        }
        HGLOBAL hMem = GlobalAlloc(GMEM_FIXED, dwLen);
        BYTE* pmem = (BYTE*)GlobalLock(hMem);
        memcpy(pmem, lpRsrc, dwLen);
        IStream* pstm = NULL;
        HRESULT hr = CreateStreamOnHGlobal(hMem, TRUE, &pstm);
        ATLASSERT(hr == S_OK && pstm != NULL);
        if (hr != S_OK)
        {
            return FALSE;
        }

        *ppBitmap = Gdiplus::Bitmap::FromStream(pstm);
        //pBitMapEx->GetHBITMAP(Color(255, 255, 255, 255), pBitmap);

        GlobalUnlock(hMem);
        pstm->Release(); //It will free the memory which is allocated by GlobalAlloc
        FreeResource(lpRsrc);
        return TRUE;
    }

    COLORREF PixelAlpha(COLORREF src, COLORREF dst, int percent)
    {
        int ipercent = 100 - percent;
        return RGB(
            (GetRValue(src) * percent + GetRValue(dst) * ipercent) / 100,
            (GetGValue(src) * percent + GetGValue(dst) * ipercent) / 100,
            (GetBValue(src) * percent + GetBValue(dst) * ipercent) / 100);
    }

}