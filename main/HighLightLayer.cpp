#include "stdafx.h"
#include "layer.h"
#include "HighLightLayer.h"


void HighLightLayer::draw_elements(Gdiplus::Graphics &g, CoordSys * pConv)
{
    RtreeBound bb;
    pConv->getRtreeBound(bb);
    auto input_args = std::make_tuple(&g, pConv, this);
    using input_args_type = decltype(input_args);
    rtree_.Search(bb.boundMin, bb.boundMax, [](IDrawable * element, void * args)
    {
        input_args_type * pInput = static_cast<input_args_type *>(args);
        auto & g = *std::get<0>(*pInput);
        auto pConv = std::get<1>(*pInput);
        auto pThis = std::get<2>(*pInput);
        element->high_light(g, pConv, &pThis->default_style);

        return true;
    }, &input_args);

    for (auto & unbound : unBound_element_) {
        if (unbound->hit_test(bb) == true) {
            unbound->draw(g, pConv, &default_style);
        }
    }
}

void HighLightLayer::SelectMany(std::vector<IDrawable *> & elements, bool swap)
{
    rtree_.RemoveAll();
    unBound_element_.clear();
    if (swap) {
        m_selected_elements.swap(elements);
    }
    else{
        m_selected_elements.clear();
        std::copy(elements.begin(), elements.end(),
            std::back_inserter(m_selected_elements));
    }

    for (auto & ele : m_selected_elements) {
        AddRTree(ele);
    }
}

void HighLightLayer::Select(IDrawable * element)
{
    rtree_.RemoveAll();
    m_selected_elements.clear();
    unBound_element_.clear();
    if (element != nullptr) {
        m_selected_elements.push_back(element);
        AddRTree(element);
    }
}

void HighLightLayer::UnSelect()
{
    m_selected_elements.clear();
    rtree_.RemoveAll();
    unBound_element_.clear();
}

void HighLightLayer::GetSelect(std::vector<IDrawable *> & elements)
{
    std::copy(m_selected_elements.begin(), m_selected_elements.end(),
        std::back_inserter(elements));
}

void HighLightLayer::AddRTree(IDrawable * element)
{
    RtreeBound rb;
    if (true == element->getBound(rb)) {
        rtree_.Insert(rb.boundMin, rb.boundMax, element);
    }
    else {
        unBound_element_.push_back(element);
    }

}

bool HighLightLayer::needRedraw(int reason)
{
    return (
        reason > draw_reson::draw_reson_Force_begin ||
        reason & draw_reson::draw_reson_highlight
        );
}

void HighLightLayer::SpecialHeightLight(IDrawable * element, CoordSys * cs, CDCHandle dc)
{
    gp::Graphics g(dc);
    for (int i = 0; i < 6; ++i)
    {
        element->draw(g, cs, i % 2 ? &inverse_style: &default_style);
        Sleep(100);
    }
}
