#pragma once
#include "map.h"
#include <memory>
#include <vector>
#include <list>
#include <map>
#include <UIRibbon.h>
#include "object_factory.h"

#define  COMMAND_FACTORY_REGISTE(derive_class) OBJECT_FACTORY_REGISTE(ICommand, derive_class)
//#define  COMMAND_FACTORY_REGISTE(derive_class) OBJECT_FACTORY_REGISTE(std::shared_ptr<ICommand>, derive_class)

#define BASE_RADIO_GROUP 100
#define INVALID_RADIO_GROUP -1

enum class Command_exe_type {
    LikeButton,
    LikeCheckBox,
    LikeRadioButton
};

enum class Command_value_type {
      button      //null
    , check_box   //bool
    , list_box    //int value
    //�Ժ��ٲ���
};

class CommandRoot;
class ICommand {
public:
    virtual ~ICommand(){}
    virtual unsigned int getID() = 0;
    virtual bool isEnable() = 0;
    virtual Command_exe_type getCommandType() = 0;

    virtual int getGroupID() {
        return INVALID_RADIO_GROUP;
    }
    virtual void OnConnected() {
    }
    virtual void OnDisConnected() {
    }

    virtual void OnExecute(Command_value_type type, long value) = 0;

    IMap * map_;
    CommandRoot * root_;
};

class CommandRoot {
public:
    void Register(std::shared_ptr<ICommand> & cmd, IMap * map);
    void UnRegister(ICommand * cmd);
    void UnRegisterAll();
    void OnExecute(unsigned int cmdID, IMap * map, bool isForce);
    void UnCheckGroup(IMap * map, int group_id, unsigned int sender_id);
    void SetUI(IUIFramework* ui);
    template <typename V>
    bool SetProperty(WORD wID, REFPROPERTYKEY key, V val)
    {
        PROPVARIANT var;
        if (SUCCEEDED(RibbonUI::SetPropertyVal(key, val, &var)))
        {
            if (FAILED(ui_->SetUICommandProperty(wID, key, var)))
                return false;
        }
        return true;
    }
    bool UIEnable(WORD wID, bool isEnable);
    bool UICheck(WORD wID, bool isEnable);
    bool UIGetCheck(WORD wID);
    bool UIIsEnable(WORD wID);

    CComVariant GetProperty( WORD wID, REFPROPERTYKEY key);
private:
    void add_group_command(std::shared_ptr<ICommand> s_cmd);

    std::map<unsigned int, std::shared_ptr<ICommand> > cmds;
    std::map<unsigned int, std::shared_ptr<std::vector<std::shared_ptr<ICommand> >> > radio_cmds;
    IUIFramework* ui_;
};



class CheckBoxCommand : public ICommand
{
public:
    virtual bool getDefaultCheckedValue() {
        return false;
    }
    virtual void OnConnected() {
        bool isChecked = getDefaultCheckedValue();
        root_->SetProperty(getID(), UI_PKEY_BooleanValue, isChecked);
        if (isChecked == true) {
            OnChecked();
        }
        else {
            OnUnChecked();
        }
    }
    virtual void OnChecked() = 0;
    virtual void OnUnChecked() = 0;
    virtual void OnExecute(Command_value_type type, long value)
    {
        if (type != Command_value_type::check_box)
            return;

        if (value)
            OnChecked();
        else
            OnUnChecked();
    }
};

class RadionCommand : public CheckBoxCommand
{
public:

    virtual void OnExecute(Command_value_type type, long value)
    {
        if (type != Command_value_type::check_box)
            return;

        if (value) {
            root_->UnCheckGroup(map_, getGroupID(), getID());
            OnChecked();
        }
        else {
            OnUnChecked();
        }
    }
};
