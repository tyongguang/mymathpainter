// aboutdlg.h : interface of the CreateGeomDlg class
//
/////////////////////////////////////////////////////////////////////////////

#pragma once
#include "resource.h"
class CreateGeomDlg : public CDialogImpl<CreateGeomDlg>
{
public:
    CreateGeomDlg();
    enum { IDD = IDD_DIALOG_INPUT_GEOM };

	BEGIN_MSG_MAP(CreateGeomDlg)
		MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
		COMMAND_ID_HANDLER(IDOK, OnCloseCmd)
		COMMAND_ID_HANDLER(IDCANCEL, OnCloseCmd)
        COMMAND_HANDLER(IDC_LIST_GEOM_LIST, LBN_SELCHANGE, OnListBoxSelChanged)
    ALT_MSG_MAP(1)
        MESSAGE_HANDLER(WM_GETDLGCODE, OnGetDlgCode)
	END_MSG_MAP()

    CListBox m_listBox;
    static BOOL m_isAutoZoom;
    static int m_curSelectIndex;

    LRESULT OnListBoxSelChanged(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);

	LRESULT OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT OnCloseCmd(WORD /*wNotifyCode*/, WORD wID, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
    LRESULT OnGetDlgCode(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);

    CString m_strCreatedText;

    void UpdateCurSel();
private:
    std::list<std::pair<std::wstring, std::wstring> > m_geomInfo;
    ATL::CContainedWindowT<CEdit> m_edInput;
};
