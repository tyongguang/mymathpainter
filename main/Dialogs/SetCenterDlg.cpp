// aboutdlg.cpp : implementation of the SetCenterDlg class
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "resource.h"

#include "SetCenterDlg.h"

double SetCenterDlg::s_defX = 0.0;
double SetCenterDlg::s_defY = 0.0;
LRESULT SetCenterDlg::OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
    CString str;
    str.Format(L"%lf", s_defX);
    SetDlgItemText(IDC_EDIT_X, str);

    str.Format(L"%lf", s_defY);
    SetDlgItemText(IDC_EDIT_Y, str);

	CenterWindow(GetParent());
	return TRUE;
}

LRESULT SetCenterDlg::OnCloseCmd(WORD /*wNotifyCode*/, WORD wID, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
    if (wID == IDOK) {
        CString str;
        GetDlgItemText(IDC_EDIT_X, str);
        s_defX = m_fX = _wtof(str);

        GetDlgItemText(IDC_EDIT_Y, str);
        s_defY = m_fY = _wtof(str);

    }
	EndDialog(wID);
	return 0;
}
