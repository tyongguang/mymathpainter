// aboutdlg.h : interface of the CreateExpressionDlg class
//
/////////////////////////////////////////////////////////////////////////////

#pragma once
#include "resource.h"
class CreateExpressionDlg : public CDialogImpl<CreateExpressionDlg>
{
public:
    enum { IDD = IDD_DIALOG_INPUT_EXPRESSION };
    CreateExpressionDlg();
	BEGIN_MSG_MAP(CreateExpressionDlg)
		MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
		COMMAND_ID_HANDLER(IDOK, OnCloseCmd)
        COMMAND_ID_HANDLER(IDCANCEL, OnCloseCmd)
    ALT_MSG_MAP(1)
        MESSAGE_HANDLER(WM_GETDLGCODE, OnGetDlgCode)
	END_MSG_MAP()


	LRESULT OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT OnCloseCmd(WORD /*wNotifyCode*/, WORD wID, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
    LRESULT OnGetDlgCode(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
    static CString m_strCreatedText;
    CFont m_editFont;
private:
    ATL::CContainedWindowT<CEdit> m_edInput;

};
