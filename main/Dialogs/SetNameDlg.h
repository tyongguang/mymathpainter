// aboutdlg.h : interface of the SetNameDlg class
//
/////////////////////////////////////////////////////////////////////////////

#pragma once
#include "resource.h"
class SetNameDlg : public CDialogImpl<SetNameDlg>
{
public:
    enum { IDD = IDD_DIALOG_SETNAME };

	BEGIN_MSG_MAP(SetNameDlg)
		MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
		COMMAND_ID_HANDLER(IDOK, OnCloseCmd)
		COMMAND_ID_HANDLER(IDCANCEL, OnCloseCmd)
	END_MSG_MAP()

    CString m_strName;
	LRESULT OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT OnCloseCmd(WORD /*wNotifyCode*/, WORD wID, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
};
