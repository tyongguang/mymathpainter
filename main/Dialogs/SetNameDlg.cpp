// aboutdlg.cpp : implementation of the SetNameDlg class
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "resource.h"

#include "SetNameDlg.h"

LRESULT SetNameDlg::OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
    SetDlgItemText(IDC_EDIT_NAME, m_strName);
	CenterWindow(GetParent());
	return TRUE;
}

LRESULT SetNameDlg::OnCloseCmd(WORD /*wNotifyCode*/, WORD wID, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
    if (wID == IDOK) {
        GetDlgItemText(IDC_EDIT_NAME, m_strName);
    }
	EndDialog(wID);
	return 0;
}
