// aboutdlg.cpp : implementation of the CreateExpressionDlg class
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "resource.h"

#include "CreateExpressionDlg.h"
CString CreateExpressionDlg::m_strCreatedText;
CreateExpressionDlg::CreateExpressionDlg() : m_edInput(this, 1)
{

}


LRESULT CreateExpressionDlg::OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{

    WTL::CFontHandle guifont = AtlGetDefaultGuiFont();

    LOGFONT lf;
    guifont.GetLogFont(&lf);
    lf.lfHeight = 32;
    lstrcpy(lf.lfFaceName, L"Microsoft Sans Serif");
    m_editFont.CreateFontIndirect(&lf);


    CenterWindow(GetParent());
    m_edInput.SubclassWindow(GetDlgItem(IDC_EDIT_INPUT));
    m_edInput.SetWindowText(m_strCreatedText);
    m_edInput.SetFont(m_editFont);

    SetDlgItemText(IDC_EDIT_README, L"函数:sin cos tan ctg asin acos atan sqrt log ln exp abs pow sigmoid tanh, 数值符号:pi e");
	return TRUE;
}

LRESULT CreateExpressionDlg::OnCloseCmd(WORD /*wNotifyCode*/, WORD wID, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
    if (wID == IDOK) {
        GetDlgItemText(IDC_EDIT_INPUT, m_strCreatedText);
        if (m_strCreatedText.GetLength() == 0) {
            MessageBox(L"表达式为空？");
            return 0;
        }
    }
	EndDialog(wID);
	return 0;
}

LRESULT CreateExpressionDlg::OnGetDlgCode(UINT /*uMsg*/, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
    LPMSG lpmsg = (LPMSG)lParam;
    bHandled = false;
    if (lpmsg && lpmsg->message == WM_KEYDOWN &&
        lpmsg->wParam == VK_RETURN &&
        m_edInput.IsWindow() &&
        m_edInput.m_hWnd == lpmsg->hwnd 
        )
    {
        short vk = GetKeyState(VK_CONTROL);
        if ((vk & 0x8000) == 0)
            return 0;

        PostMessage(WM_COMMAND, MAKEWPARAM(IDOK, 0), 0);
        return 0;
    }
    return 0;
}
