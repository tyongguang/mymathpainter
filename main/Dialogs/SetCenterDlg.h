// aboutdlg.h : interface of the SetCenterDlg class
//
/////////////////////////////////////////////////////////////////////////////

#pragma once
#include "resource.h"
class SetCenterDlg : public CDialogImpl<SetCenterDlg>
{
public:
    enum { IDD = IDD_DIALOG_SETCENTER };

	BEGIN_MSG_MAP(SetCenterDlg)
		MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
		COMMAND_ID_HANDLER(IDOK, OnCloseCmd)
		COMMAND_ID_HANDLER(IDCANCEL, OnCloseCmd)
	END_MSG_MAP()

    double m_fX;
    double m_fY;
	LRESULT OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT OnCloseCmd(WORD /*wNotifyCode*/, WORD wID, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
private:
    static double s_defX;
    static double s_defY;

};
