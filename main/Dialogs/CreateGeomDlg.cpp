// aboutdlg.cpp : implementation of the CreateGeomDlg class
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "resource.h"

#include "CreateGeomDlg.h"

BOOL CreateGeomDlg::m_isAutoZoom = true;
int CreateGeomDlg::m_curSelectIndex = -1;


CreateGeomDlg::CreateGeomDlg()
    :m_edInput(this, 1)
{
    m_geomInfo.push_back(std::make_pair(std::wstring(L"POINT"),
        std::wstring(L"创建一个点.格式：POINT(X Y)")));

    m_geomInfo.push_back(std::make_pair(std::wstring(L"LINESTRING"),
        std::wstring(L"创建线段.格式：LINESTRING(X1 Y1, X2 Y2, X3 Y3)")));
    m_geomInfo.push_back(std::make_pair(std::wstring(L"POLYGON"),
        std::wstring(L"创建多边形.格式：POLYGON(X1 Y1, X2 Y2, X3 Y3)")));
    m_geomInfo.push_back(std::make_pair(std::wstring(L"RAY"),
        std::wstring(L"创建射线.格式：RAY(起点X 起点Y, 方向X 方向Y)  起点是个硬坐标，而方向则单位向量")));

    m_geomInfo.push_back(std::make_pair(std::wstring(L"RAY2"),
        std::wstring(L"创建射线.格式：RAY2(经过点1X 经过点1Y, 经过点2X 经过点2Y)")));

    m_geomInfo.push_back(std::make_pair(std::wstring(L"LINE"),
        std::wstring(L"创建直线.格式：LINE(经过点X 经过点Y, 方向X 方向Y)  经过点是个硬坐标，而方向则单位向量")));

    m_geomInfo.push_back(std::make_pair(std::wstring(L"LINE2"),
        std::wstring(L"创建直线.格式：LINE2(经过点1X 经过点1Y, 经过点2X 经过点2Y) ")));
}

LRESULT CreateGeomDlg::OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	CenterWindow(GetParent());
    CEdit edit = GetDlgItem(IDC_EDIT_INPUT);
    edit.SetLimitText(1024 * 1024 * 8);//支持8M

    m_listBox = GetDlgItem(IDC_LIST_GEOM_LIST);
    for (auto & item : m_geomInfo) {
        int index = m_listBox.AddString(item.first.c_str());
        m_listBox.SetItemData(index, (DWORD_PTR)&item.second);

    }


    if (CreateGeomDlg::m_curSelectIndex >= 0) {
        m_listBox.SetCurSel(CreateGeomDlg::m_curSelectIndex);
        UpdateCurSel();
    }
    CButton(GetDlgItem(IDC_CHECK_AUTO_ZOOM)).SetCheck(m_isAutoZoom);
    m_edInput.SubclassWindow(GetDlgItem(IDC_EDIT_INPUT));
	return FALSE;
}

LRESULT CreateGeomDlg::OnCloseCmd(WORD /*wNotifyCode*/, WORD wID, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
    if (wID == IDOK) {
        m_curSelectIndex = m_listBox.GetCurSel();
        GetDlgItemText(IDC_EDIT_INPUT, m_strCreatedText);
        if (m_strCreatedText.GetLength() == 0) {
            MessageBox(L"geom描述为空？");
            return 0;
        }
        m_isAutoZoom = CButton(GetDlgItem(IDC_CHECK_AUTO_ZOOM)).GetCheck();
    }
	EndDialog(wID);
	return 0;
}

LRESULT CreateGeomDlg::OnListBoxSelChanged(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
{
    UpdateCurSel();
    return 0;
}

void CreateGeomDlg::UpdateCurSel()
{
    CString strText;
    int index = m_listBox.GetCurSel();
    m_listBox.GetText(index, strText);
    if (L"POLYGON" == strText) {
        strText += L"((\r\n \r\n \r\n \r\n \r\n))";
    }
    else {
        strText += L"(\r\n \r\n \r\n \r\n \r\n)";
    }


    SetDlgItemText(IDC_EDIT_INPUT, strText);
    GetDlgItem(IDC_EDIT_INPUT).SetFocus();
    CEdit(GetDlgItem(IDC_EDIT_INPUT)).SetSel(0, strText.GetLength() - 9);
    CEdit(GetDlgItem(IDC_EDIT_INPUT)).SetSelNone();
    std::wstring * pString = (std::wstring * )m_listBox.GetItemDataPtr(index);
    SetDlgItemText(IDC_STATIC_TIPS, pString->c_str());

}

LRESULT CreateGeomDlg::OnGetDlgCode(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM lParam, BOOL& bHandled)
{
    LPMSG lpmsg = (LPMSG)lParam;
    bHandled = false;
    if (lpmsg && lpmsg->message == WM_KEYDOWN &&
        lpmsg->wParam == VK_RETURN &&
        m_edInput.IsWindow() &&
        m_edInput.m_hWnd == lpmsg->hwnd
        )
    {
        short vk = GetKeyState(VK_CONTROL);
        if ((vk & 0x8000) == 0)
            return 0;

        PostMessage(WM_COMMAND, MAKEWPARAM(IDOK, 0), 0);
        return 0;
    }
    return 0;
}
