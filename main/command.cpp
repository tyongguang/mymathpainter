#include "stdafx.h"
#include "command.h"
#include <algorithm>


void CommandRoot::Register(std::shared_ptr<ICommand> & cmd, IMap * map)
{
    std::shared_ptr<ICommand> s_cmd(cmd);
    s_cmd->map_ = map;
    s_cmd->root_ = this;
    cmds.insert(std::make_pair(cmd->getID(), s_cmd));
    if (cmd->getCommandType() == Command_exe_type::LikeRadioButton) {
        add_group_command(s_cmd);
    }
    s_cmd->OnConnected();
}

void CommandRoot::UnCheckGroup(IMap * map, int group_id, unsigned int sender_id)
{
    auto iter = radio_cmds.find(group_id);
    if (iter == radio_cmds.end())
        return;
    for (auto & cmd : *iter->second.get()) {
        if (cmd->getID() != sender_id) {
            cmd->OnExecute(Command_value_type::check_box, false);
            SetProperty(cmd->getID(), UI_PKEY_BooleanValue, false);
        }
    }
}

void CommandRoot::add_group_command(std::shared_ptr<ICommand> s_cmd)
{
    auto iter = radio_cmds.find(s_cmd->getGroupID());
    if (iter == radio_cmds.end()) {
        auto vec = std::make_shared<std::vector<std::shared_ptr<ICommand> > >();
        radio_cmds.insert(std::make_pair(s_cmd->getGroupID(), vec));
        vec->push_back(s_cmd);
        return;
    }
    
    iter->second->push_back(s_cmd);
}

void CommandRoot::OnExecute(unsigned int cmdID, IMap * map, bool isForce)
{
    auto cmd_iter = cmds.find(cmdID);
    if (cmd_iter == cmds.end())
        return;
    auto cmd_type = cmd_iter->second->getCommandType();
    if (UIIsEnable(cmdID) == false)
        return;

    if (cmd_type == Command_exe_type::LikeRadioButton) {
        CComVariant var = GetProperty(cmdID, UI_PKEY_BooleanValue);
        if (var.boolVal == VARIANT_FALSE) {
            SetProperty(cmdID, UI_PKEY_BooleanValue, true);
            if (isForce == false)
                return;

            //by force
            var.boolVal = VARIANT_TRUE;
        }
        cmd_iter->second->OnExecute(Command_value_type::check_box, var.boolVal == VARIANT_TRUE);
    }
    else if (cmd_type == Command_exe_type::LikeCheckBox) {
        CComVariant var = GetProperty(cmdID, UI_PKEY_BooleanValue);
        cmd_iter->second->OnExecute(Command_value_type::check_box, var.boolVal == VARIANT_TRUE);

    }if (cmd_type == Command_exe_type::LikeButton) {
        cmd_iter->second->OnExecute(Command_value_type::button, 0);
    }
}

void CommandRoot::SetUI(IUIFramework* ui)
{
    ui_ = ui;
}

ATL::CComVariant CommandRoot::GetProperty(WORD wID, REFPROPERTYKEY key)
{
    PROPVARIANT var;
    if (FAILED(ui_->GetUICommandProperty(wID, key, &var)))
        return false;


    return CComVariant(*(VARIANT *)&var);
}

void CommandRoot::UnRegister(ICommand * cmd)
{
    auto iter_for_remove = cmds.find(cmd->getID());
    if (iter_for_remove != cmds.end()) {
        cmd->OnDisConnected();
        cmds.erase(iter_for_remove);
    }
    if (cmd->getCommandType() == Command_exe_type::LikeRadioButton) {
        auto iter_for_remove_radio = radio_cmds.find(cmd->getGroupID());
        if (iter_for_remove_radio != radio_cmds.end())  {
            auto & vec = *iter_for_remove_radio->second;
            auto iter_vector = std::find_if(vec.begin(), vec.end(),
                [&](std::shared_ptr<ICommand> & vec_cmd)
            {
                if (vec_cmd->getID() == cmd->getID())
                    return true;
                return false;
            });
            if (iter_vector != vec.end())
                vec.erase(iter_vector);

        } //end[ if (iter_for_remove_radio != ...]
    }//end [if (cmd->getCommandType()  ....]
    
}

void CommandRoot::UnRegisterAll()
{
    for (auto & cmd : cmds) {
        cmd.second->OnDisConnected();
    }
    radio_cmds.clear();
    cmds.clear();
}

bool CommandRoot::UIEnable(WORD wID, bool isEnable)
{
    return SetProperty(wID, UI_PKEY_Enabled, isEnable);
}

bool CommandRoot::UICheck(WORD wID, bool isChecked)
{
    return SetProperty(wID, UI_PKEY_BooleanValue, isChecked);
}

bool CommandRoot::UIGetCheck(WORD wID)
{
    CComVariant isChecked = GetProperty(wID, UI_PKEY_BooleanValue);
    return  isChecked.boolVal == VARIANT_TRUE;
}

bool CommandRoot::UIIsEnable(WORD wID)
{
    CComVariant isEnable = GetProperty(wID, UI_PKEY_Enabled);
    return  isEnable.boolVal == VARIANT_TRUE;
}
