#pragma once

#include "gdiplus.h"
#include "view_point_config.h"
#include <memory>
#include <algorithm>
#include "Wm5Box2.h"
#include "Wm5Segment2.h"

#ifdef min
#define  MIN min
#else
#define  MIN std::min
#endif

#ifdef max
#define  MAX max
#else
#define  MAX std::max
#endif

class RtreeBound
{
public:
    double boundMin[2];
    double boundMax[2];
};

class ICoordConv {
public:
    virtual vPoint Screen2View(const Gdiplus::PointF & pt) = 0;
    virtual Gdiplus::PointF View2Screen(const vPoint & spt) = 0;
    virtual double ViewSize2ScreenSize(double vSize) = 0;
    virtual double ScreenSize2ViewSize(double sSize) = 0;

    //view point coordinate
    virtual double vleft() = 0;
    virtual double vtop() = 0;
    virtual double vright() = 0;
    virtual double vbottom() = 0;

    //一个box由center和extent组成
    virtual vPoint center() = 0;
    virtual vPoint extent() = 0;

    //screen point coordinate
    virtual float swidth() = 0;
    virtual float sheight() = 0;
};



class CoordSys {
public:
    CoordSys(ICoordConv * p) :base_conv(p) {
    }
    inline vPoint conv(const Gdiplus::PointF & pt) {
        return base_conv->Screen2View(pt);
    }

    inline Gdiplus::PointF conv(const vPoint & spt) {
        return base_conv->View2Screen(spt);
    }
    void getBox(Wm5::Box2d & box);
    void getRtreeBound(RtreeBound & bound);
    template<class _InIt, class _OutIt> 
    inline _OutIt conv(_InIt _First, _InIt _Last, _OutIt _Dest) {
        for (; _First != _Last; ++_Dest, ++_First)
            *_Dest = conv(*_First); 
        return (_Dest);
    }
  
    ICoordConv * base_conv;
};


class IDrawStyle
{
public:
    virtual const Gdiplus::Pen * getPen() = 0;
    virtual const Gdiplus::Brush * getBrush() = 0;
    virtual CPenHandle getPenHandle() = 0;
    virtual CBrushHandle getBrushHandle() = 0;
    virtual float getPointRenderWidth() = 0;
};

class MGuid : public GUID
{
public:
    inline explicit MGuid()
    {
        memset(this, 0, sizeof(GUID));
    }
    inline explicit MGuid(const BYTE nValue)
    {
        memset(this, nValue, sizeof(GUID));
    }
    inline MGuid(const GUID & src)
    {
        memcpy(this, (const void *)&src, sizeof(GUID));
    }
    inline bool operator == (const GUID & rGuid) const
    {
        return memcmp(this, (const void *)&rGuid, sizeof(GUID)) == 0;
    }
    inline bool operator < (const GUID & rGuid) const
    {
        return memcmp(this, (const void *)&rGuid, sizeof(GUID)) < 0;
    }
    inline bool operator > (const GUID & rGuid) const
    {
        return memcmp(this, (const void *)&rGuid, sizeof(GUID)) > 0;
    }
    inline bool IsEmpty() const
    {
        return Data1 == 0 &&
            Data2 == 0 && Data3 == 0 &&
            (*(__int64 *)Data4) == 0;
    }
    inline void NewID()  {
        CoCreateGuid(this);
    }
};

//顺序不能变
enum class GeomType : int {
    Point = 0,
    LineSting,
    Polygon,
    Line,
    Ray,
    Expression,
    Composite
};
class ILayer;
class IDrawable {
public:
    virtual ~IDrawable() {};
    virtual const MGuid & getID() = 0;
    virtual vPoint nearestPoint(const vPoint & pt, double range) = 0;   //物体上原有最近点
    virtual vPoint nearestIntrPoint(vPoint & pt) = 0;   //物体上理论上最近点, 也就是切线相交垂直相交点
    virtual GeomType getType() = 0;
    virtual void draw(Gdiplus::Graphics &g, CoordSys * pConv, IDrawStyle * style) = 0;
    virtual void high_light(Gdiplus::Graphics &g, CoordSys * pConv, IDrawStyle * style) = 0;

    virtual bool hit_test(const RtreeBound & bound) = 0;
    virtual bool getBound(RtreeBound & bound) = 0;
    virtual void Delete() = 0;
    virtual void SetLayer(ILayer * layer) = 0;
    virtual bool IntersectWith(IDrawable * other, RtreeBound & bound, vPoint * pt) = 0;
    virtual bool IntersectWithAxis(RtreeBound & bound, vPoint * pt) = 0;
    virtual bool getSegment(RtreeBound & bound, std::vector<Wm5::Segment2d> & segments) {
        return false;
    }
    virtual bool dump_info(CString & info){
        return false;
    }
};

enum draw_reson {
    draw_reson_Other = 1 << 0,
    draw_reson_highlight = 1 << 1,


    draw_reson_Force_begin = 1 << 10,
    draw_reson_Zoom = 1 << 11,
    draw_reson_Roam = 1 << 12,
    draw_reson_Size = 1 << 13,
    draw_reson_Delete = 1 << 14,
    draw_reson_Force = 1 << 15
};


class ILayer {
public:
    ILayer() 
        :selectable(true), visiable(true) {
    }
    virtual ~ILayer() {};
    void draw(Gdiplus::Graphics &g, CoordSys * pConv, int reason) {
        if (false == visiable)
            return;
        DoDraw(g, pConv, reason);
    }
    virtual void Query(const RtreeBound & bound, std::vector<IDrawable * > &output) {
    }
    virtual void setSize(int width, int height) {
    }
    virtual void AddElement(std::shared_ptr<IDrawable> & ele) {
    }
    virtual bool getBound(RtreeBound & bound) {
        return false;
    }
    virtual void Delete(IDrawable * element) {
    }
    virtual void RemoveAll() {
    }
    CString name;

    bool selectable;
    bool visiable;
protected:
    virtual void DoDraw(Gdiplus::Graphics &g, CoordSys * pConv, int reason) {
    }
};

class DrawableBase : public IDrawable {
public:
    DrawableBase() {
        guid.NewID();
    }
    virtual const MGuid & getID() {
        return guid;
    }
    virtual void Delete() {
        parent->Delete(this);
    }
    virtual void SetLayer(ILayer * layer) {
        parent = layer;
    }
    virtual bool IntersectWithAxis(RtreeBound & bound, vPoint * pt);
    virtual bool IntersectWith(IDrawable * other, RtreeBound & bound, vPoint * pt);
    ILayer * parent;
private:
    MGuid guid;
    
};

class CacheDrawlayer : public ILayer
{
public:
    virtual void DoDraw(Gdiplus::Graphics &g, CoordSys * pConv, int reason);
    virtual void setSize(int width, int height);
    virtual bool needRedraw(int reason);
    virtual void draw_elements(Gdiplus::Graphics &g, CoordSys * pConv) = 0;
protected:
    std::unique_ptr<gp::Bitmap> cache_bmp_;
private:
    void ClearBitmap(gp::Bitmap &bmp);
};

class IAxisControl
{
public:
    bool rw_showGrid_;
    bool rw_showGridText_;
    bool rw_showMeasureScale_;
    bool rw_visiable_;
    bool r_scale_valid_;
    double r_scale_;
};
